/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "serialization.hpp"

#include "Payload.hpp"
#include "ErrorType.hpp"
#include "IPayload.hpp"

isms::kn::ErrorType isms::kn::Serialize(
    const std::shared_ptr<isms::kn::IPayload> &ipayload,
    std::string &stream_str) {
  std::stringstream stream;
  /* Add the output serialization buff */
  boost::archive::binary_oarchive ar(stream);

  auto payload_casted = isms::kn::IPayload::CastPayload<Payload>(ipayload);

  if (payload_casted) {
    /* Add the serialized data into the ipc object*/
    ar << *payload_casted;
    stream_str = stream.str();
    if (stream_str.size() == 0) return isms::kn::ErrorType::kSerializationError;
    return isms::kn::ErrorType::kSuccess;
  } else {
    std::cout << "Payload didn't cast properly for the serialization"
              << std::endl;
    return isms::kn::ErrorType::kCannotCast;
  }
}

isms::kn::ErrorType isms::kn::Deserialize(
    const std::string &stream_str, const std::shared_ptr<IPayload> &ipayload) {
  std::stringstream stream;

  stream.str(stream_str);

  /* Add the input serialization buff */
  boost::archive::binary_iarchive bin_input_file(stream);

  auto payload = isms::kn::IPayload::CastPayload<Payload>(ipayload);

  if (payload) {
    /* Add the serialized data into the ipc object*/
    bin_input_file >> *payload;
    return isms::kn::ErrorType::kSuccess;
  } else {
    std::cout << "Payload didn't cast properly for the serialization"
              << std::endl;
    return isms::kn::ErrorType::kCannotCast;
  }
}