/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/binary_object.hpp>
#include <boost/serialization/serialization.hpp>
#include <iostream>
#include <memory>
#include <sstream>

#include "ErrorType.hpp"

namespace isms {
namespace kn {
class IPayload;
/**
 * Serialize
 * @brief Serialize receives an IPayload shared pointer which will be serialized
 * into a binary format, it returns a string that contains the serialized data.
 * @param payload IPayload shared pointer that will be serialized
 * @param stream_str std::string reference that will store the serialized Payload
 * information
 * @return ErrorType, kSuccess if the process was ok, any other means a
 * problem
 */
ErrorType Serialize(const std::shared_ptr<IPayload> &payload,
                    std::string &stream_str);

/**
 * Deserialize
 * @brief Deserialize receives a string and an IPayload shared pointer, to
 * insert the data into the payload to retrieve the serialized information
 * the serialized data.
 * @param stream_str string, which contains the serialized data
 * @param payload IPayload shared pointer that will store the data
 * @return ErrorType, kSuccess if the process was ok, any other means a
 * problem
 */
ErrorType Deserialize(const std::string &stream_str,
                      const std::shared_ptr<IPayload> &ipayload);

}  // namespace kn
}  // namespace isms
