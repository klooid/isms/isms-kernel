/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <memory>

#include <RequestType.hpp>

/**
 *
 * IPayload
 * @brief IPayload is an interface used to handle the minimum common attributes
 * for all the payloads in the system. A Payload is used to transfer packets
 * between services. IPayload is inherited by the specific Payloads.
 *
 */
namespace isms {
namespace kn {
class IPayload {
 public:
  /**
   * Create
   * @brief Create, retrieves the IPayload Instance acting as a Factory
   * for the Payloads which inherited it.
   */
  static std::shared_ptr<IPayload> Create();
  IPayload() : pid_{-1}, subprocess_{false} {}
  virtual ~IPayload(){};

  /* Getters and Setters */

  /**
   * get_origin_sid
   * @brief Provides the origin Service ID that requested the payload
   * @return int, sid
   */
  const int get_origin_sid() const noexcept;

  /**
   * set_origin_sid
   * @brief Sets the origin Service ID
   * @param sid integer with the Service ID
   */
  void set_origin_sid(const int sid);

  /**
   * get_dest_sid
   * @brief Provides the destination Service ID
   * @return Int, destination service ID
   */
  const int get_dest_sid() const noexcept;

  /**
   * set_dest_id
   * @brief Sets the destination Service ID
   * @param sid int, Service ID
   */
  void set_dest_sid(const int sid);

  /**
   * get_pid
   * @brief Provides the Process owner ID for the payload
   * @return int, Process ID
   */
  const int get_pid() const noexcept;

  /**
   * set_pid
   * @brief Sets the Process owner ID for the payload
   * @param pid int, Process ID
   */
  void set_pid(const int);

  /**
   * get_user
   * @brief Provides the user name that requested the process
   * @return string, the user name
   */
  const std::string &get_user() const noexcept;

  /**
   * set_user
   * @brief Sets the user name who request the process
   * @param user string, the user name
   */
  void set_user(const std::string &user);

  /**
   * get_password
   * @brief Provides the user password to verify the access
   * @return string, user password
   */
  const std::string &get_password() const noexcept;

  /**
   * set_password
   * @brief Sets the user password
   * @param password string, password
   */
  void set_password(const std::string &password);

  /**
   * get_project_id
   * @brief Provides the Project ID for the request
   * @return int, Project ID
   */
  const int get_project_id() const noexcept;

  /**
   * set_project_id
   * @brief Sets the Project ID for the request
   * @param project_id int, Project ID
   */
  void set_project_id(const int project_id);

  /**
   * IsSubprocess
   * @brief Indicates if the request is for a subprocess
   * @return bool, sub_process flag
   */
  const bool IsSubprocess() const noexcept;

  /**
   * set_subprocess
   * @brief Sets the subprocess flag
   * @param subprocess, bool, sub_process flag
   */
  void set_subprocess(bool subprocess);

  /**
   * get_request_type
   * @brief Provides the request type which is used to identify the service
   * destination, such as:
   *    - kCon : connect
   *    - kDcon : disconnect
   *    - kComm: communication
   *    - kProc: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @return string, request type
   */
  const RequestType get_request_type() const noexcept;

  /**
   * set_request_type
   * @brief Sets the request type which is used to identify the service
   * destination, such as:
   * @param request_type string, request type, such as
   *    - kCon : connect
   *    - kDcon : disconnect
   *    - kComm: communication
   *    - kProc: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   */
  void set_request_type(const RequestType request_type);

  /**
   * CastPayload
   * @brief Return a payload casted into a selected inherited class
   * @param payload IPayload object that will be casted
   * @return shared_ptr of the new casted object
   */
  template <class T>
  static const std::shared_ptr<T> CastPayload(
      const std::shared_ptr<IPayload> &payload);

 protected:
  int origin_sid_;
  int dest_sid_;
  int pid_;
  std::string user_;
  std::string password_;
  int project_id_;
  bool subprocess_;
  RequestType request_type_;

  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &origin_sid_;
    archive &dest_sid_;
    archive &pid_;
    archive &user_;
    archive &password_;
    archive &project_id_;
    archive &request_type_;
    archive &subprocess_;
  }
};

template <class T>
const std::shared_ptr<T> IPayload::CastPayload(
    const std::shared_ptr<IPayload> &payload) {
  auto casted_payload = std::dynamic_pointer_cast<T>(payload);
  if (casted_payload) return casted_payload;
  return nullptr;
}

inline const std::string &IPayload::get_user() const noexcept {
  return this->user_;
}

inline void IPayload::set_user(const std::string &user) { this->user_ = user; }

inline const std::string &IPayload::get_password() const noexcept {
  return this->password_;
}

inline void IPayload::set_password(const std::string &password) {
  this->password_ = password;
}

inline void IPayload::set_subprocess(bool subprocess) {
  this->subprocess_ = subprocess;
}

inline const bool IPayload::IsSubprocess() const noexcept {
  return this->subprocess_;
}

inline const int IPayload::get_project_id() const noexcept {
  return this->project_id_;
}

inline void IPayload::set_project_id(int project_id) {
  this->project_id_ = project_id;
}

inline const RequestType IPayload::get_request_type() const noexcept {
  return this->request_type_;
}

inline void IPayload::set_request_type(const RequestType request_type) {
  this->request_type_ = request_type;
}

inline const int IPayload::get_origin_sid() const noexcept {
  return this->origin_sid_;
}

inline void IPayload::set_origin_sid(const int sid) { this->origin_sid_ = sid; }

inline const int IPayload::get_dest_sid() const noexcept {
  return this->dest_sid_;
}

inline void IPayload::set_dest_sid(int sid) { this->dest_sid_ = sid; }

inline void IPayload::set_pid(int pid) { this->pid_ = pid; }

inline const int IPayload::get_pid() const noexcept { return this->pid_; }

}  // namespace kn
}  // namespace isms
