/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include "CommPayload.hpp"
#include "ConnectionPayload.hpp"
#include "DBPayload.hpp"
#include "DoerPayload.hpp"
#include "ProcessInfoPayload.hpp"
#include "ResPayload.hpp"
#include "ServiceInfoPayload.hpp"

namespace isms {
namespace kn {
/**
 * Payload
 * @brief this is the payload class used to handle all the data that will be
 * transfered. It follows the Diamond Class Inheritance to provide a dynamic
 * object which could be manipulated in order to provide the most accurate
 * encapsulated information packet for each service and request.
 */
class Payload : public ProcessInfoPayload,
                public ServiceInfoPayload,
                public CommPayload,
                public DbPayload,
                public DoerPayload,
                public ResPayload,
                public ConnectionPayload {
 public:
  Payload() {}
  virtual ~Payload() {}

 private:
  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &boost::serialization::base_object<ProcessInfoPayload>(*this);
    archive &boost::serialization::base_object<ServiceInfoPayload>(*this);
    archive &boost::serialization::base_object<CommPayload>(*this);
    archive &boost::serialization::base_object<DbPayload>(*this);
    archive &boost::serialization::base_object<DoerPayload>(*this);
    archive &boost::serialization::base_object<ResPayload>(*this);
    archive &boost::serialization::base_object<ConnectionPayload>(*this);
  }
};
}  // namespace kn
}  // namespace isms
