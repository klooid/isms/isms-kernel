/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <string>

#include "IPayload.hpp"
#include "ProcessState.hpp"

namespace isms {
namespace kn {
/**
 * ProcessInfoPayload
 * @brief this is the payload used to handle the Processes Information, it is
 * used to retrive the current process information to services, due to a
 * request, Control Gate
 */
class ProcessInfoPayload : public virtual IPayload {
 public:
  ProcessInfoPayload() {}
  virtual ~ProcessInfoPayload() {}

  /* Getters and Setters */

  /**
   * get_process_id
   * @brief Provides the Process ID
   * @return int, Process ID
   */
  const int get_process_id() const noexcept;

  /**
   * set_process_id
   * @brief Sets the Process ID
   * @param process_id int, Process ID
   */
  void set_process_id(const int process_id);

  /**
   * get_process_name
   * @brief Provides the process name
   * @return string, Process name
   */
  const std::string &get_process_name() const noexcept;

  /**
   * set_process_name
   * @brief Sets the process name (it is a combination of parameters)
   * @param process_name string, process name
   */
  void set_process_name(const std::string &process_name);

  /**
   * get_origin_service
   * @brief Provides the origin service name
   * (it is a combination of parameters)
   * @return string
   */
  const std::string &get_origin_service() const noexcept;

  /**
   * set_origin_service
   * @brief Sets the origin service name
   * @param origin_service string, Origin service name
   */
  void set_origin_service(const std::string &origin_service);

  /**
   * get_current_service
   * @brief Provides the current service name where the process is running
   * (it is a combination of parameters)
   * @return string, Current service name
   */
  const std::string &get_current_service() const noexcept;

  /**
   * set_current_service
   * @brief Sets the current service name where the process is running
   * (it is a combination of parameters)
   * @param current_service string, Current Service
   */
  void set_current_service(const std::string &current_service);

  /**
   * get_process_type
   * @brief Provides the process type
   * @return string, the process type
   */
  const std::string &get_process_type() const noexcept;

  /**
   * set_process_type
   * @brief Sets the process type
   * @param process_type string, process type
   */
  void set_process_type(const std::string &process_type);

  /**
   * get_state
   * @brief Provides the current state of the service
   * @return ServiceState, the possible state:
   *    - stop, stopped
   *    - run, running
   *    - blocked, blocked
   */
  const ProcessState get_state() const noexcept;

  /**
   * set_state
   * @brief Sets the process state based on five possibilities:
   *    - stopped
   *    - running
   *    - done
   *    - blocked
   *    - ready
   * @param state ProcessState, Process State
   */
  void set_state(ProcessState state);

  /**
   * get_start_time
   * @brief Provides the time when the process was created
   * @return time_t, start time
   */
  const std::string &get_start_time() const noexcept;

  /**
   * set_start_time
   * @brief Sets the time when the process was created
   * @param start_time time_t, process start time
   */
  void set_start_time(const std::string &start_time);

  /**
   * get_current_time
   * @brief Provides the amount of time that the process has consumed in seconds
   * @return int, the current time in seconds
   */
  int get_current_time() noexcept;

  /**
   * set_current_time
   * @brief Sets the current time that the process has consumed in seconds
   * @param current_time int, the current time in secods
   */
  void set_current_time(int current_time);

 protected:
  int process_id_;
  std::string process_name_;
  std::string origin_service_;
  std::string current_service_;
  std::string process_type_;
  std::string start_time_;
  ProcessState state_;
  int current_time_;

  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &boost::serialization::base_object<IPayload>(*this);
    archive &process_id_;
    archive &process_name_;
    archive &origin_service_;
    archive &current_service_;
    archive &process_type_;
    archive &start_time_;
    archive &state_;
    archive &current_time_;
  }
};
inline const int ProcessInfoPayload::get_process_id() const noexcept {
  return this->process_id_;
}

inline void ProcessInfoPayload::set_process_id(int process_id) {
  this->process_id_ = process_id;
}

inline const std::string &ProcessInfoPayload::get_process_name()
    const noexcept {
  return this->process_name_;
}

inline void ProcessInfoPayload::set_process_name(
    const std::string &process_name) {
  this->process_name_ = process_name;
}

inline const std::string &ProcessInfoPayload::get_origin_service()
    const noexcept {
  return this->origin_service_;
}

inline void ProcessInfoPayload::set_origin_service(
    const std::string &origin_service) {
  this->origin_service_ = origin_service;
}

inline const std::string &ProcessInfoPayload::get_current_service()
    const noexcept {
  return this->current_service_;
}

inline void ProcessInfoPayload::set_current_service(
    const std::string &current_service) {
  this->current_service_ = current_service;
}

inline const std::string &ProcessInfoPayload::get_process_type()
    const noexcept {
  return this->process_type_;
}

inline void ProcessInfoPayload::set_process_type(
    const std::string &process_type) {
  this->process_type_ = process_type;
}

inline const ProcessState ProcessInfoPayload::get_state() const noexcept {
  return this->state_;
}

inline void ProcessInfoPayload::set_state(ProcessState state) {
  this->state_ = state;
}

inline const std::string &ProcessInfoPayload::get_start_time() const noexcept {
  return this->start_time_;
}

inline void ProcessInfoPayload::set_start_time(const std::string &start_time) {
  this->start_time_ = start_time;
}

inline int ProcessInfoPayload::get_current_time() noexcept {
  return this->current_time_;
}

inline void ProcessInfoPayload::set_current_time(int current_time) {
  this->current_time_ = current_time;
}
}  // namespace kn
}  // namespace isms
