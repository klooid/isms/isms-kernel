/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */
#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include "IPayload.hpp"

namespace isms {
namespace kn {
/**
 *
 * DBPayload
 * @brief DBPayload is the payload class used to transfer the information for
 * DBriver requests. This class contains the most relevant data that Dbriver
 * needs to handle the requested database operations.
 *
 */
class DbPayload : public virtual IPayload {
 public:
  DbPayload() : custom_{false} {}
  virtual ~DbPayload() {}

  /* Getters and Setters */
  /**
   * get_query
   * @brief Provides the query which will be requested, it is a serialized JSON
   * @return string with the serialized JSON
   */
  const std::string &get_query() const noexcept;

  /**
   * set_query
   * @brief Sets the serilized JSON in string type
   * @param query serilized JSON string
   */
  void set_query(const std::string &query);

  /**
   * get_function_name
   * @brief Provides the DBriver function name to be executed
   * @return string with the function name
   */
  const std::string &get_function_name() const noexcept;

  /**
   * set_function_name
   * @brief Sets the DBriver function name to be executed
   * @param function_name string with the function name
   */
  void set_function_name(const std::string &function_name);

  /**
   * get_inputs
   * @brief Provides the inputs for the requested query
   * @return string which contains the query inputs
   */
  const std::string &get_inputs() const noexcept;

  /**
   * set_inputs
   * @brief Sets the inputs for the requested query
   * @param inputs string which contains the query inputs
   */
  void set_inputs(const std::string &inputs);

  /**
   * get_outputs
   * @brief Provides the outputs for the query result
   * @return string with the outputs
   */
  const std::string &get_outputs() const noexcept;

  /**
   * set_outputs
   * @brief
   * @param
   */
  void set_outputs(const std::string &outputs);

  /**
   * IsCustom
   * @brief Provides the custom flag which indicates if the query is a custom
   * query
   * @return boolean, true / false
   */
  const bool IsCustom() const noexcept;

  /**
   * set_custom
   * @brief Sets the flag custom which indicates that the query is a custom
   * query.
   * @param custom boolean true / false
   */
  void set_custom(bool custom);

 protected:
  std::string query_;
  std::string function_name_;
  std::string inputs_;
  std::string outputs_;
  bool custom_;

  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &boost::serialization::base_object<IPayload>(*this);
    archive &query_;
    archive &function_name_;
    archive &inputs_;
    archive &outputs_;
    archive &custom_;
  }
};
inline const std::string &DbPayload::get_query() const noexcept {
  return this->query_;
}

inline void DbPayload::set_query(const std::string &query) {
  this->query_ = query;
}

inline const std::string &DbPayload::get_function_name() const noexcept {
  return this->function_name_;
}

inline void DbPayload::set_function_name(const std::string &function_name) {
  this->function_name_ = function_name;
}

inline const std::string &DbPayload::get_inputs() const noexcept {
  return this->inputs_;
}

inline void DbPayload::set_inputs(const std::string &inputs) {
  this->inputs_ = inputs;
}

inline const std::string &DbPayload::get_outputs() const noexcept {
  return this->outputs_;
}

inline void DbPayload::set_outputs(const std::string &outputs) {
  this->outputs_ = outputs;
}

inline const bool DbPayload::IsCustom() const noexcept { return this->custom_; }

inline void DbPayload::set_custom(bool custom) { this->custom_ = custom; }
}  // namespace kn
}  // namespace isms
