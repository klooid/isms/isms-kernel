/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <string>

#include "IPayload.hpp"
#include "ServiceState.hpp"

namespace isms {
namespace kn {

/**
 * ServicesInfoPayload
 * @brief this is the payload used to handle the Services Information, it is
 * used to retrieve the current services information to other services, due to a
 * request, such as Control Gate
 */
class ServiceInfoPayload : public virtual IPayload {
 public:
  ServiceInfoPayload() {}
  virtual ~ServiceInfoPayload() {}

  /* Getters and Setters */

  /**
   * get_service_id
   * @brief Provides the Service ID
   * @return int, Service ID
   */
  const int get_service_id() const noexcept;

  /**
   * set_service_id
   * @brief Sets the Service ID
   * @param service_id int, Service ID
   */
  void set_service_id(const int service_id);

  /**
   * get_service_name
   * @brief Provides the service name, it is a combination of parameters
   * @return string, service name
   */
  const std::string &get_service_name() const noexcept;

  /**
   * set_service_name
   * @brief Sets the service name, it is a combination of parameters
   * @param service_name string, service name
   */
  void set_service_name(const std::string &service_name);

  /**
   * get_service_type
   * @brief Provides the service type, such as:
   *    - kComm: communication
   *    - exe: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @return string, service type
   */
  const std::string &get_service_type() const noexcept;

  /**
   * set_service_type
   * @brief Sets the service type, such as:
   *    - kComm: communication
   *    - exe: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @param service_type string, service type
   */
  void set_service_type(const std::string &service_type);

  /**
   * get_state
   * @brief Provides the current service state, such as:
   *    - e, running
   *    - s, stopped
   *    - d, dead
   * @return char, state
   */
  const ServiceState get_state() const noexcept;

  /**
   * set_state
   * @brief Sets the current state of the service, such as:
   *    - e, running
   *    - s, stopped
   *    - d, dead
   * @param state char, state
   */
  void set_state(ServiceState state);

  /**
   * get_start_time
   * @brief Provides the time when the service started its execution
   * @return string, start time
   */
  const std::string &get_start_time() const noexcept;

  /**
   * set_start_time
   * @brief Sets the time when the service started its execution
   * @param start_time string, start time
   */
  void set_start_time(const std::string &start_time_);

  /**
   * get_current_time
   * @brief Provides the amount of time that the service has been running
   * @return string, current time
   */
  const std::string &get_current_time() const noexcept;

  /**
   * set_current_time
   * @brief Sets the amount of time that the service has been running
   * @param current_time string, current time
   */
  void set_current_time(const std::string &current_time_);

 protected:
  int service_id_;
  std::string service_name_;
  std::string service_type_;
  ServiceState state_;
  std::string start_time_;
  std::string current_time_;

  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &boost::serialization::base_object<IPayload>(*this);
    archive &service_id_;
    archive &service_name_;
    archive &service_type_;
    archive &state_;
    archive &start_time_;
    archive &current_time_;
  }
};

inline const int ServiceInfoPayload::get_service_id() const noexcept {
  return this->service_id_;
}

inline void ServiceInfoPayload::set_service_id(const int service_id) {
  this->service_id_ = service_id;
}

inline const std::string &ServiceInfoPayload::get_service_name()
    const noexcept {
  return this->service_name_;
}

inline void ServiceInfoPayload::set_service_name(
    const std::string &service_name) {
  this->service_name_ = service_name;
}

inline const std::string &ServiceInfoPayload::get_service_type()
    const noexcept {
  return this->service_type_;
}

inline void ServiceInfoPayload::set_service_type(
    const std::string &service_type) {
  this->service_type_ = service_type;
}

inline const ServiceState ServiceInfoPayload::get_state() const noexcept {
  return this->state_;
}

inline void ServiceInfoPayload::set_state(ServiceState state) { this->state_ = state; }

inline const std::string &ServiceInfoPayload::get_start_time() const noexcept {
  return this->start_time_;
}

inline void ServiceInfoPayload::set_start_time(const std::string &start_time) {
  this->start_time_ = start_time;
}

inline const std::string &ServiceInfoPayload::get_current_time()
    const noexcept {
  return this->current_time_;
}

inline void ServiceInfoPayload::set_current_time(
    const std::string &current_time) {
  this->current_time_ = current_time;
}
}  // namespace kn
}  // namespace isms
