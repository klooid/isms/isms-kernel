/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <CommServerProtocol.hpp>
#include <EndpointType.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include "IPayload.hpp"

namespace isms {
namespace kn {
/**
 *
 * CommPayload
 * @brief CommPayload is the payload class used to transfer the information for
 * CommServer requests. This class contains the most relevant data that
 * CommServer needs to handle the requested operations, such as an MQTT
 * Publication.
 *
 */
class CommPayload : public virtual IPayload {
 public:
  CommPayload() {}
  virtual ~CommPayload() {}

  /* Getters and Setters */

  /**
   * get_protocol
   * @brief Provides the communication protocol of the CommServer destination
   * @return kn::CommServerProtocol
   */
  const kn::CommServerProtocol get_protocol() const noexcept;

  /**
   * set_protocol
   * @brief Sets the communication protocol for the CommServer destination
   * @param protocol, kn::CommServerProtocol the protocol type
   */
  void set_protocol(const kn::CommServerProtocol protocol);

  /**
   * get_data
   * @brief Provides the data wich contains the request information
   * @return String with the data serialized
   */
  const std::string &get_data() const noexcept;

  /**
   * set_data
   * @brief Sets the data wich contains the request information
   * @param data serialized string data
   */
  void set_data(const std::string &data);

  /**
   * get_path_to_send
   * @brief Provides the path where data will be sent
   * @return string with the path information
   */
  const std::string &get_path_to_send() const noexcept;

  /**
   * set_path_to_send
   * @brief Sets the path where data will be sent
   * @param path_to_send string with the path information such as mqtt topic
   */
  void set_path_to_send(const std::string &path_to_send);

  /**
   * get_function_type
   * @brief Provides the function type for the endpoint
   * @return EndpointType with the function type
   */
  const EndpointType get_function_type() const noexcept;

  /**
   * set_function_type
   * @brief Sets the function type wich will be executed in the system for an
   * specific endpoint
   * @param function_type EndpointType with the function type
   */
  void set_function_type(const EndpointType function_type);

  /**
   * get_function_name
   * @brief Provides the requested function name
   * @return string with the function name requested
   */
  const std::string &get_function_name() const noexcept;

  /**
   * set_function_name
   * @brief Sets the function name wich will be executed in the system
   * @param function_name string with the function name
   */
  void set_function_name(const std::string &function_type);

  /**
   * get_function_name
   * @brief Set the function name that is requested
   * @return string with the function name requested
   */
  const std::string &get_result_data() const noexcept;

  /**
   * set_result_data
   * @brief Sets the serilized result data
   * @param data_to_send string with the serialized result data
   */
  void set_result_data(const std::string &function_type);

  /**
   * @brief Get the domain object
   *
   * @return const std::string&
   */
  const std::string &get_domain() const noexcept;

  /**
   * @brief Set the domain object
   *
   * @param domain const std::string &
   */
  void set_domain(const std::string &domain);

  /**
   * get_function_id
   * @brief Provides the function ID which shall be executed
   * @return string with the function name requested
   */
  const int get_function_id() const noexcept;

  /**
   * set_function_id
   * @brief Sets the function id wich will be executed in the Doer,
   * it is the Doee ID
   * @param function_id int of the function ID
   */
  void set_function_id(const int function_id);

  /**
   * @brief Get the port object
   *
   * @return const unsigned int
   */
  const unsigned int get_port() const noexcept;

  /**
   * @brief Set the port object
   *
   * @param port
   */
  void set_port(const unsigned int port);

 protected:
  kn::CommServerProtocol protocol_;
  std::string requested_data_;
  std::string path_to_send_;
  EndpointType function_type_;
  std::string function_name_;
  int function_id_;
  std::string result_data_;
  unsigned int port_;
  std::string domain_;

  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &boost::serialization::base_object<IPayload>(*this);
    archive &protocol_;
    archive &requested_data_;
    archive &path_to_send_;
    archive &function_type_;
    archive &function_name_;
    archive &function_id_;
    archive &result_data_;
    archive &port_;
    archive &domain_;
  }
};

inline const kn::CommServerProtocol CommPayload::get_protocol() const noexcept {
  return this->protocol_;
}

inline void CommPayload::set_protocol(const kn::CommServerProtocol protocol) {
  this->protocol_ = protocol;
}

inline const std::string &CommPayload::get_data() const noexcept {
  return this->requested_data_;
}

inline void CommPayload::set_data(const std::string &data) {
  this->requested_data_ = data;
}

inline const std::string &CommPayload::get_path_to_send() const noexcept {
  return this->path_to_send_;
}

inline void CommPayload::set_path_to_send(const std::string &path_to_send) {
  this->path_to_send_ = path_to_send;
}

inline const EndpointType CommPayload::get_function_type() const noexcept {
  return this->function_type_;
}

inline void CommPayload::set_function_type(const EndpointType function_type) {
  this->function_type_ = function_type;
}

inline const std::string &CommPayload::get_function_name() const noexcept {
  return this->function_name_;
}

inline void CommPayload::set_function_name(const std::string &function_name) {
  this->function_name_ = function_name;
}

inline const std::string &CommPayload::get_result_data() const noexcept {
  return this->result_data_;
}

inline void CommPayload::set_result_data(const std::string &result_data) {
  this->result_data_ = result_data;
}

inline const int CommPayload::get_function_id() const noexcept {
  return this->function_id_;
}

inline void CommPayload::set_function_id(const int function_id) {
  this->function_id_ = function_id;
}

inline const unsigned int CommPayload::get_port() const noexcept {
  return this->port_;
}

inline void CommPayload::set_port(const unsigned int port) {
  this->port_ = port;
}

inline const std::string &CommPayload::get_domain() const noexcept {
  return this->domain_;
}

inline void CommPayload::set_domain(const std::string &domain) {
  this->domain_ = domain;
}

}  // namespace kn
}  // namespace isms
