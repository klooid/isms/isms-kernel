/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <string>

#include "IPayload.hpp"

namespace isms {
namespace kn {
/**
 *
 * DoerPayload
 * @brief DoerPayload is the payload class used to transfer the information for
 * Doer requests. This class contains the most relevant data that Doer needs to
 * handle the requested operations. such as Doee execution.
 *
 */
class DoerPayload : public virtual IPayload {
 public:
  DoerPayload() {}
  virtual ~DoerPayload() {}

  /* Gettes and Setter */

  /**
   * get_doee_name
   * @brief Provides the Doee name which contains the src for processing the
   * request
   * @return string with the Doee name
   */
  const std::string &get_doee_name() const noexcept;

  /**
   * set_doee_name
   * @brief Sets the Doee name which contains the src for processing the
   * request
   * @param doee_name string
   */
  void set_doee_name(const std::string &doee_name);

  /**
   * get_volume_path
   * @brief Provide the directory path of the project that contains the Doee
   * srcs, it will be used to set the volume path into the containers
   * @return string with the volume path
   */
  const std::string &get_volume_path() const noexcept;

  /**
   * set_volume_path
   * @brief Sets the derectory which contains the resorces of a projects
   * @param volume_path string with the volume path
   */
  void set_volume_path(const std::string &volume_path);

  /**
   * get_inputs
   * @brief Provides the Doee inputs for Doee execution
   * @return string with the inputs
   */
  const std::string &get_inputs() const noexcept;

  /**
   * set_inputs
   * @brief Sets the Doee inputs
   * @param inputs String with the inputs
   */
  void set_inputs(const std::string &inputs);

  /**
   * get_doee_id
   * @brief Provides the Doee ID that is requested
   * @return int with the Doee ID
   */
  const int get_doee_id() const noexcept;

  /**
   * set_doee_id
   * @brief Sets the Doee ID
   * @param doee_id int with the DoeeID
   */
  void set_doee_id(const int doee_id);

  /**
   * get_result
   * @brief Provides the result of the Doee execution
   * @return string with the Doee result
   */
  const std::string &get_result() const noexcept;

  /**
   * set_result
   * @brief Sets the result of the Doee execution
   * @param result string with the Doee result
   */
  void set_result(const std::string &result);

  /**
   * @brief Get the doee language object
   *
   * @return const std::string&
   */
  const std::string &get_doee_language() const noexcept;

  /**
   * @brief Set the doee language object
   *
   * @param lang const std::string &
   */
  void set_doee_language(const std::string &lang);

  /**
   * @brief Get the doee src object
   *
   * @return const std::string&
   */
  const std::string &get_doee_src() const noexcept;

  /**
   * @brief Set the doee src object
   *
   * @param src const std::string &
   */
  void set_doee_src(const std::string &src);

 protected:
  int doee_id_;
  std::string doee_name_;
  std::string volume_path_;
  std::string inputs_;
  std::string result_;
  std::string doee_language_;
  std::string src_file_;

  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &boost::serialization::base_object<IPayload>(*this);
    archive &doee_id_;
    archive &doee_name_;
    archive &volume_path_;
    archive &inputs_;
    archive &result_;
    archive &doee_language_;
    archive &src_file_;
  }
};

inline const std::string &DoerPayload::get_doee_name() const noexcept {
  return this->doee_name_;
}

inline void DoerPayload::set_doee_name(const std::string &doee_name) {
  this->doee_name_ = doee_name;
}

inline const std::string &DoerPayload::get_volume_path() const noexcept {
  return this->volume_path_;
}

inline void DoerPayload::set_volume_path(const std::string &volume_path) {
  this->volume_path_ = volume_path;
}

inline const std::string &DoerPayload::get_inputs() const noexcept {
  return this->inputs_;
}

inline void DoerPayload::set_inputs(const std::string &inputs) {
  this->inputs_ = inputs;
}

inline const std::string &DoerPayload::get_result() const noexcept {
  return this->result_;
}

inline void DoerPayload::set_result(const std::string &result) {
  this->result_ = result;
}

inline const int DoerPayload::get_doee_id() const noexcept {
  return this->doee_id_;
}

inline void DoerPayload::set_doee_id(const int doee_id) {
  this->doee_id_ = doee_id;
}

inline const std::string &DoerPayload::get_doee_language() const noexcept {
  return this->doee_language_;
}

inline void DoerPayload::set_doee_language(const std::string &lang) {
  this->doee_language_ = lang;
}

inline const std::string &DoerPayload::get_doee_src() const noexcept {
  return this->src_file_;
}

inline void DoerPayload::set_doee_src(const std::string &src) {
  this->src_file_ = src;
}

}  // namespace kn
}  // namespace isms
