/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include "IPayload.hpp"

namespace isms {
namespace kn {

/**
 *
 * ResPayload
 * @brief ResPayload is the payload class used to transfer the information for
 * Resadmin requests. This object contains the relevant information that needs
 * the Resadmin to interpretate the requested operations from other services.
 *
 */
class ResPayload : public virtual IPayload {
 public:
  ResPayload() {}
  virtual ~ResPayload() {}

  /* Getters and Setters */

  /**
   * get_resource_type
   * @brief Provide the type of resource that is requested
   * @return string, the resource type
   */
  const std::string &get_resource_type() const noexcept;

  /**
   * set_resource_type
   * @brief Sets the resource type that is requested
   * @param resource_type string, resource type
   */
  void set_resource_type(const std::string &resource_type);

  /**
   * get_doee_id
   * @brief Provides the Doee ID
   * @return int, Doee ID
   */
  const int get_doee_id() const noexcept;

  /**
   * set_doee_id
   * @brief Sets the Doee ID
   * @param doee_id int, Doee ID
   */
  void set_doee_id(int doee_id);

  /**
   * get_comm_server_id
   * @brief Provides the CommServer ID
   * @return int, CommServer ID
   */
  const int get_comm_server_id() const noexcept;

  /**
   * set_comm_server_id
   * @brief Sets the CommServer ID
   * @param comm_server_id int, CommServer ID
   */
  void set_comm_server_id(int comm_server_id);

  /**
   * get_ip_to_check
   * @brief Provides the IP that is needed to verify new connection
   * @return string, IP
   */
  const std::string &get_ip_to_check() const noexcept;

  /**
   * set_ip_to_check
   * @brief Sets the IP that is needed to verify new connection
   * @param ip_to_check string, IP
   */
  void set_ip_to_check(const std::string &ip_to_check);

  /**
   * get_device_id
   * @brief Provides the Device ID, such as an IoT Device
   * @return int, device ID
   */
  const int get_device_id() const noexcept;

  /**
   * set_device_id
   * @brief Sets the Device ID, such as an IoT Device
   * @param device_id int, Device ID
   */
  void set_device_id(int device_id);

  /**
   * @brief Set the project name object
   *
   * @param project_name
   */
  void set_project_name(const std::string &project_name);

  /**
   * @brief Get the project name object
   *
   * @return const std::string&
   */
  const std::string &get_project_name() const noexcept;

  /**
   * @brief Set the new user name object
   *
   * @param name, const std::string &, the user name of the new user to add
   */
  void set_new_user(const std::string &name);

  /**
   * @brief Get the new user object
   *
   * @return const std::string&
   */
  const std::string &get_new_user() const noexcept;

  /**
   * @brief Set the group name object
   *
   * @param name
   */
  void set_group_name(const std::string &name);

  /**
   * @brief Set the group name object
   *
   * @return const std::string&
   */
  const std::string &get_group_name() const noexcept;

 protected:
  std::string resource_type_;
  int doee_id_;
  int comm_server_id_;
  std::string ip_to_check_;
  int device_id_;
  std::string project_name_;
  std::string new_user_;
  std::string group_name_;

  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &boost::serialization::base_object<IPayload>(*this);
    archive &resource_type_;
    archive &doee_id_;
    archive &comm_server_id_;
    archive &ip_to_check_;
    archive &device_id_;
    archive &project_name_;
    archive &new_user_;
    archive &group_name_;
  }
};

inline const std::string &ResPayload::get_resource_type() const noexcept {
  return this->resource_type_;
}

inline void ResPayload::set_resource_type(const std::string &resource_type) {
  this->resource_type_ = resource_type;
}

inline const int ResPayload::get_doee_id() const noexcept {
  return this->doee_id_;
}

inline void ResPayload::set_doee_id(const int doee_id) {
  this->doee_id_ = doee_id;
}

inline const int ResPayload::get_comm_server_id() const noexcept {
  return this->comm_server_id_;
}

inline void ResPayload::set_comm_server_id(const int comm_server_id) {
  this->comm_server_id_ = comm_server_id;
}

inline const std::string &ResPayload::get_ip_to_check() const noexcept {
  return this->ip_to_check_;
}

inline void ResPayload::set_ip_to_check(const std::string &ip_to_check) {
  this->ip_to_check_ = ip_to_check;
}

inline const int ResPayload::get_device_id() const noexcept {
  return this->device_id_;
}

inline void ResPayload::set_device_id(int device_id) {
  this->device_id_ = device_id;
}

inline void ResPayload::set_project_name(const std::string &project_name) {
  this->project_name_ = project_name;
}

inline const std::string &ResPayload::get_project_name() const noexcept {
  return this->project_name_;
}

inline void ResPayload::set_new_user(const std::string &name) {
  this->new_user_ = name;
}

inline const std::string &ResPayload::get_new_user() const noexcept {
  return this->new_user_;
}

inline void ResPayload::set_group_name(const std::string &name) {
  this->group_name_ = name;
}

inline const std::string &ResPayload::get_group_name() const noexcept {
  return this->group_name_;
}

}  // namespace kn
}  // namespace isms
