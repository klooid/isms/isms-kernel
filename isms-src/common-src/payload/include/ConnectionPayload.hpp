/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <string>

#include "IPayload.hpp"

namespace isms {
namespace kn {
/**
 *
 * ConnectionPayload
 * @brief ConnectionPayload is the payload class used to transfer the
 * information for new connections requests. This class contains the most
 * relevant data that Kernel needs to handle the requested operations.
 *
 */
class ConnectionPayload : public virtual IPayload {
 public:
  ConnectionPayload() : availability_{true} {}
  virtual ~ConnectionPayload() {}

  /* Getters and Setters */

  /**
   * get_endpoint
   * @brief Provides the endpoint that will be used to handle the communication
   * between the service and the kernel
   * @param type_of_service string, service type
   * @return
   */
  const std::string &get_endpoint() const noexcept;

  /**
   * set_endpoint
   * @brief Sets endpoint that will be used to handle the communication
   * between the service and the kernel
   * @param endpoint string, endpoint
   */
  void set_endpoint(const std::string &endpoint);

  /**
   * get_type_of_service
   * @brief Provides the service type, such as:
   *    - kCon : connect
   *    - kDcon : disconnect
   *    - kComm: communication
   *    - kProc: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @return string, type of service
   */
  const RequestType get_type_of_service() const noexcept;

  /**
   * set_type_of_service
   * @brief Sets the service type, such as:
   *    - kCon : connect
   *    - kDcon : disconnect
   *    - kComm: communication
   *    - kProc: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @param type_of_service string, service type
   */
  void set_type_of_service(const RequestType type_of_service);

  /**
   * get_availability
   * @brief Provides the service availability for message reception
   * @return bool, service availability
   */
  const bool get_availability() const noexcept;

  /**
   * set_availability
   * @brief Sets the service availability for message reception
   * @param availability bool, service availability
   */
  void set_availability(const bool availability);

 protected:
  std::string endpoint_;
  RequestType type_of_service_;
  bool availability_;

  /**
   *  Serialization overrided function
   * @brief It is used to serialize the Payload data for communication purposes
   */
  friend class boost::serialization::access;
  template <class Archive>
  void serialize(Archive &archive, const unsigned int /*version*/) {
    archive &boost::serialization::base_object<IPayload>(*this);
    archive &endpoint_;
    archive &type_of_service_;
    archive &availability_;
  }
};

inline const bool ConnectionPayload::get_availability() const noexcept {
  return this->availability_;
}

inline void ConnectionPayload::set_availability(const bool availability) {
  this->availability_ = availability;
}

inline const std::string &ConnectionPayload::get_endpoint() const noexcept {
  return this->endpoint_;
}

inline void ConnectionPayload::set_endpoint(const std::string &endpoint) {
  this->endpoint_ = endpoint;
}

inline const RequestType ConnectionPayload::get_type_of_service()
    const noexcept {
  return this->type_of_service_;
}

inline void ConnectionPayload::set_type_of_service(
    const RequestType type_of_service) {
  this->type_of_service_ = type_of_service;
}

}  // namespace kn
}  // namespace isms
