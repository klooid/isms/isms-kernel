/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ExecutionResult.hpp>
#include <string>

namespace isms {
namespace cmd {

/**
 * @brief Run command and return the output
 *
 * @param command const std::string, the command to be executed
 * @return ExecutionResult
 */
ExecutionResult Exec(const std::string &command);

}  // namespace cmd
}  // namespace isms
