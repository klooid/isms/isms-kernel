/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "execommand.hpp"

#include <array>
#include <cstdio>
#include <iostream>
#include <string>

using namespace isms;

const static int kBufferSize{128};

cmd::ExecutionResult cmd::Exec(const std::string& command) {
  /* Defining the variables */
  cmd::ExecutionResult result_obj;
  std::string result_message{""};
  std::array<char, kBufferSize> buffer;

  /* Execute the cmd */
  auto pipe = popen(command.c_str(), "r");

  if (!pipe) {
    result_obj.set_error_message("popen() failed!");
    result_obj.set_error_code(kn::ErrorType::kCannotOpen);
    return result_obj;
  }

  while (!feof(pipe)) {
    if (fgets(buffer.data(), kBufferSize, pipe) != nullptr)
      result_message += buffer.data();
  }
  result_obj.set_result(result_message);

  auto rc = pclose(pipe);

  if (rc == EXIT_SUCCESS) {
    result_obj.set_error_message("pclose() success!");
    result_obj.set_error_code(kn::ErrorType::kSuccess);
  } else {
    result_obj.set_error_message("pclose() failed!");
    result_obj.set_error_code(kn::ErrorType::kCannotClose);
  }

  return result_obj;
}
