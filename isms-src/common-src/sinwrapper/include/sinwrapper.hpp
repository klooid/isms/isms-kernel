/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ErrorType.hpp>
#include <string>

namespace isms {
namespace sinw {

/**
 * @brief Start a singularity container as a persistent instance
 *
 * @param instance_name std::string container instance
 * @return kn::ErrorType successful result returns kn::ErrorType::kSuccess, any
 * other means an error
 */
kn::ErrorType StartInstance(const std::string &instance_name);

/**
 * @brief Stop a singularity container instance
 *
 * @param instance_name std::string container instance
 * @return kn::ErrorType successful result returns kn::ErrorType::kSuccess, any
 * other means an error
 */
kn::ErrorType StopInstance(const std::string &instance_name);

/**
 * @brief Execute command into a container
 *
 * @param instance_name std::string container instance
 * @param command std::string command
 * @return kn::ErrorType successful result returns kn::ErrorType::kSuccess, any
 * other means an error
 */
kn::ErrorType ExecCommand(const std::string &instance_name,
                          const std::string &command);

/**
 * @brief Copy src element from host to a destination path in a container
 * instance
 *
 * @param instance_name std::string container instance
 * @param src_path std::string src path element
 * @param dest_path std::string destination path
 * @return kn::ErrorType successful result returns kn::ErrorType::kSuccess, any
 * other means an error
 */
kn::ErrorType SendToInstance(const std::string &instance_name,
                             const std::string &src_path,
                             const std::string &dest_path);

}  // namespace sinw
}  // namespace isms
