/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "sinwrapper.hpp"

#include <execommand.hpp>
#include <iostream>
#include <string>

using namespace isms;

const std::string kBindFlag{"--bind"};
const std::string kContainerSIFPath{
    "isms-src/doer/worker-handler/config/worker.sif"};
const std::string kCreationFlags{"--containall"};
const std::string kExecCmd{"exec"};
const std::string kInstanceCmd{"instance"};
const std::string kInstanceList{"list"};
const std::string kProgCmd{"singularity"};
const std::string kSeparator{" "};
const std::string kStartInstance{"start"};
const std::string kStopInstance{"stop"};

kn::ErrorType sinw::StartInstance(const std::string &instance_name) {
  if (instance_name.size() <= 0) {
    std::cout << "Instance name must be specified" << std::endl;
    return kn::ErrorType::kCannotCreate;
  }
  auto cmd = kProgCmd + kSeparator + kInstanceCmd + kSeparator +
             kStartInstance + kSeparator + kCreationFlags + kSeparator +
             kContainerSIFPath + kSeparator + instance_name;

  auto cmd_result = cmd::Exec(cmd);

  if (cmd_result.get_error_code() != kn::ErrorType::kSuccess) {
    std::cout << "Error running cmd: " << cmd_result.get_error_message() << std::endl;
    return cmd_result.get_error_code();
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType sinw::StopInstance(const std::string &instance_name) {
  if (instance_name.size() <= 0) {
    std::cout << "Instance name must be specified" << std::endl;
    return kn::ErrorType::kCannotClose;
  }

  auto cmd = kProgCmd + kSeparator + kInstanceCmd + kSeparator +
             kStopInstance + kSeparator + instance_name;

  auto cmd_result = cmd::Exec(cmd);

  if (cmd_result.get_error_code() != kn::ErrorType::kSuccess) {
    std::cout << "Error running cmd: " << cmd_result.get_error_message() << std::endl;
    return cmd_result.get_error_code();
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType sinw::ExecCommand(const std::string &instance_name,
                                const std::string &command) {
  if (instance_name.size() <= 0 && command.size() <= 0) {
    std::cout << "Parameters must be specified" << std::endl;
    return kn::ErrorType::kCannotSend;
  }

  auto cmd = kProgCmd + kSeparator + kExecCmd + kSeparator + kInstanceCmd +
             "://" + instance_name + kSeparator + command;
  auto cmd_result = cmd::Exec(cmd);

  if (cmd_result.get_error_code() != kn::ErrorType::kSuccess) {
    std::cout << "Error running cmd: " << cmd_result.get_error_message() << std::endl;
    return cmd_result.get_error_code();
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType sinw::SendToInstance(const std::string &instance_name,
                                   const std::string &src_path,
                                   const std::string &dest_path) {
  // This will be defined later
  return kn::ErrorType::kSuccess;
}
