/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

namespace isms {
namespace log {
/**
 * @brief LogType provides the type of Loggers that can be created,
 * each of them are related to the modules of the system
 *
 */
enum class LogType {
  kControlGateLog,
  kCommServerLog,
  kDoerLog,
  kKernelLog,
  kResadminLog,
};
}  // namespace log
}  // namespace isms
