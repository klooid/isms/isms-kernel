/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once
#include <memory>
#include <string>

#include "LogType.hpp"

namespace isms {
namespace log {
class ILogger {
 public:
  ILogger() {}
  virtual ~ILogger() {}

  /**
   * @brief Records an Information log
   *
   * @param message const std::string &, message to add to the logger
   */
  virtual void InfoLog(const std::string &message) = 0;

  /**
   * @brief Records a Warning log
   *
   * @param message const std::string &, message to add to the logger
   */
  virtual void WarnLog(const std::string &message) = 0;

  /**
   * @brief Records an Error log
   *
   * @param message const std::string &, message to add to the logger
   */
  virtual void ErrorLog(const std::string &message) = 0;

  /**
   * @brief Records a Debug log
   *
   * @param message const std::string &, message to add to the logger
   */
  virtual void DebugLog(const std::string &message) = 0;

  /**
   * @brief Records a Trace log
   *
   * @param message const std::string &, message to add to the logger
   */
  virtual void TraceLog(const std::string &message) = 0;

  /**
   * @brief Records a Fatal log
   *
   * @param message const std::string &, message to add to the logger
   */
  virtual void FatalLog(const std::string &message) = 0;

  /**
   * @brief Initialize the logger
   *
   * @param module_name const std::string &, It is the module name for the
   * logger
   */
  virtual void Init(const std::string &module_name) = 0;

  /**
   * @brief Set the main module object
   *
   * @param main_module_name const std::string &, main module name
   */
  virtual void set_parent_module_name(const std::string &main_module_name) = 0;

  /**
   * @brief Builds a logger depending on the given LogType
   *
   * @param type const LogType, it is the type of the logger such as:
   *    - kControlGateLog
   *    - kCommServerLog
   *    - kDoerLog
   *    - kKernelLog
   *    - kResadminLog
   * @return std::shared_ptr<ILogger>
   */
  static std::shared_ptr<ILogger> Build(const LogType type);
};

}  // namespace log
}  // namespace isms
