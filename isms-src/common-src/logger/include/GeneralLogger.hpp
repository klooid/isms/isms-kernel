/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <spdlog/spdlog.h>

#include "ILogger.hpp"

namespace isms {
namespace log {
class GeneralLogger : public ILogger {
 public:
  GeneralLogger() {}
  virtual ~GeneralLogger() {}

  /**
   * @brief Records an Information log
   *
   * @param message const std::string &, message to add to the logger
   */
  void InfoLog(const std::string &message) override;

  /**
   * @brief Records a Warning log
   *
   * @param message const std::string &, message to add to the logger
   */
  void WarnLog(const std::string &message) override;

  /**
   * @brief Records an Error log
   *
   * @param message const std::string &, message to add to the logger
   */
  void ErrorLog(const std::string &message) override;

  /**
   * @brief Records a Debug log
   *
   * @param message const std::string &, message to add to the logger
   */
  void DebugLog(const std::string &message) override;

  /**
   * @brief Records a Trace log
   *
   * @param message const std::string &, message to add to the logger
   */
  void TraceLog(const std::string &message) override;

  /**
   * @brief Records a Fatal log
   *
   * @param message const std::string &, message to add to the logger
   */
  void FatalLog(const std::string &message) override;

  /**
   * @brief Initialize the logger
   *
   * @param module_name const std::string &, It is the module name for the
   * logger
   */
  void Init(const std::string &module_name) override;

  /**
   * @brief Set the parent module name object
   *
   * @param main_module_name const std::string &, main module name
   */
  void set_parent_module_name(const std::string &parent_name) override;

 private:
  std::shared_ptr<spdlog::logger> logger_;
  std::string parent_module_name_;
};

inline void GeneralLogger::set_parent_module_name(
    const std::string &parent_name) {
  this->parent_module_name_ = parent_name;
}

}  // namespace log
}  // namespace isms
