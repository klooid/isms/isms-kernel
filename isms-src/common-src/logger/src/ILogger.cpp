/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "ILogger.hpp"

#include "GeneralLogger.hpp"

using namespace isms::log;

const std::string kCommServerLoggerName{"CommServer"};
const std::string kControlGateLoggerName{"ControlGate"};
const std::string kDoerLoggerName{"Doer"};
const std::string kKernelLoggerName{"Kernel"};
const std::string kResadminLoggerName{"Resadmin"};

std::shared_ptr<ILogger> ILogger::Build(const LogType type) {
  std::shared_ptr<ILogger> logger;
  switch (type) {
    case LogType::kKernelLog:
      logger = std::make_shared<GeneralLogger>();
      logger->set_parent_module_name(kKernelLoggerName);
      break;

    case LogType::kDoerLog:
      logger = std::make_shared<GeneralLogger>();
      logger->set_parent_module_name(kDoerLoggerName);
      break;

    case LogType::kResadminLog:
      logger = std::make_shared<GeneralLogger>();
      logger->set_parent_module_name(kResadminLoggerName);
      break;

    case LogType::kCommServerLog:
      logger = std::make_shared<GeneralLogger>();
      logger->set_parent_module_name(kCommServerLoggerName);
      break;

    case LogType::kControlGateLog:
      logger = std::make_shared<GeneralLogger>();
      logger->set_parent_module_name(kControlGateLoggerName);
      break;

    default:
      logger = nullptr;
      break;
  }
  return logger;
}
