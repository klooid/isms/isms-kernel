/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "GeneralLogger.hpp"

#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

using namespace isms;

const int kBackTraceSize{64};

void log::GeneralLogger::InfoLog(const std::string &message) {
  this->logger_->info("[INFO] {} ", message);
}

void log::GeneralLogger::WarnLog(const std::string &message) {
  this->logger_->warn("[WARNING] {}", message);
}

void log::GeneralLogger::ErrorLog(const std::string &message) {
  this->logger_->error("[ERROR] {}", message);
  this->logger_->dump_backtrace();
}

void log::GeneralLogger::DebugLog(const std::string &message) {
  this->logger_->debug("[DEBUG] {}", message);
}

void log::GeneralLogger::TraceLog(const std::string &message) {
  this->logger_->trace("[TRACE] {}", message);
}

void log::GeneralLogger::FatalLog(const std::string &message) {
  this->logger_->critical("[CRITICAL] {}", message);
  this->logger_->dump_backtrace();
}

void log::GeneralLogger::Init(const std::string &module_name) {
  auto logger_name = this->parent_module_name_ + "-" + module_name;
  auto file_size = 1024 * 1024 * 10;
  auto max_files = 3;

  /* Set the console logger */
  auto console_logger = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
  console_logger->set_pattern("[%H:%M:%S.%e] [%n]: %v%$");
  console_logger->set_level(spdlog::level::debug);

  /* Set the file logger */
  auto file_logger = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(
      "logs/" + parent_module_name_ + "/" + logger_name + ".log", file_size,
      max_files);
  file_logger->set_pattern("[%Y-%m-%d | %H:%M:%S.%e]: %v%$");
  file_logger->set_level(spdlog::level::trace);

  /* Set the loggers list */
  spdlog::sinks_init_list sink_list = {file_logger, console_logger};

  /* Set the general logger */
  this->logger_ = std::make_shared<spdlog::logger>(
      logger_name, sink_list.begin(), sink_list.end());
  this->logger_->set_level(spdlog::level::debug);
  this->logger_->enable_backtrace(kBackTraceSize);
  this->logger_->flush_on(spdlog::level::err);
}
