/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include "ILibraryHandler.hpp"
namespace isms {
namespace lib {

class LibraryHandler : public ILibraryHandler {
 public:
  /**
   * @brief Construct a new Library Handler object
   *
   */
  LibraryHandler() {}

  /**
   * @brief Destroy the Library Handler object
   *
   */
  virtual ~LibraryHandler() {}

  /**
   * CreateLibrary
   * @brief Compile and create a shared library
   * @param file_path std::string src file path
   * @param name std::string doee name
   * @return ErrorType kSuccess if the execution was well, any other means an
   * error
   */
  kn::ErrorType CreateLibrary(const std::string &file_path,
                              const std::string &name) override;

  /**
   * LoadLibrary
   * @brief Load a shared library from an specific path
   * @param file_path std::string doee shared library file path
   * @param shared_lib void *& to provide the loaded shared library
   * @return ErrorType kSuccess if the execution was well, any other means an
   * error
   */
  kn::ErrorType LoadLibrary(const std::string &file_path,
                            void *&shared_lib) override;

  /**
   * @brief Set the dependencies object with the structure:
   *    -> "-llib_1 -llib_2 -lib_3 ... -llib_n"
   *    Where "lib_k" is the lib dependency
   *
   * @param dependencies std::string lib dependencies
   */
  void set_dependencies(const std::string &dependencies) override;

  /**
   * @brief Set the lib folder path object with the structure:
   *    -> "path/to/folder/"
   *
   * @param path std::string & path to folder
   */
  void set_lib_path(const std::vector<std::string> &path) override;

  /**
   * @brief Set the include folder path object with the structure:
   *    -> "path/to/folder/"
   *
   * @param path std::string & path to folder
   */
  void set_include_path(const std::vector<std::string> &path) override;

  /**
   * @brief Get the dependencies object with the structure:
   *    -> "-llib_1 -llib_2 -lib_3 ... -llib_n"
   *    Where "lib_k" is the lib dependency
   *
   * @return const std::string& lib dependencies
   */
  const std::string &get_dependencies() const noexcept override;

  /**
   * @brief Get the lib folder path object with the structure:
   *    -> "path/to/folder/"
   *
   * @param path std::string & path to folder
   */
  const std::vector<std::string> &get_lib_path() const noexcept override;

  /**
   * @brief Get the include folder path object with the structure:
   *    -> "path/to/folder/"
   *
   * @param path std::string & path to folder
   */
  const std::vector<std::string> &get_include_path() const noexcept override;

 private:
  std::string dependencies_;
  std::vector<std::string> lib_paths_;
  std::vector<std::string> include_paths_;
};

inline void LibraryHandler::set_dependencies(const std::string &dependencies) {
  this->dependencies_ = dependencies;
}

inline const std::string &LibraryHandler::get_dependencies() const noexcept {
  return this->dependencies_;
}

inline void LibraryHandler::set_lib_path(const std::vector<std::string> &path) {
  this->lib_paths_ = path;
}

inline const std::vector<std::string> &LibraryHandler::get_lib_path() const noexcept {
  return this->lib_paths_;
}

inline void LibraryHandler::set_include_path(const std::vector<std::string> &path) {
  this->include_paths_ = path;
}

inline const std::vector<std::string> &LibraryHandler::get_include_path() const noexcept {
  return this->include_paths_;
}

}  // namespace lib
}  // namespace isms
