/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ErrorType.hpp>
#include <memory>
#include <string>
#include <vector>

namespace isms {
namespace lib {

class ILibraryHandler {
 public:
  ILibraryHandler() {}

  virtual ~ILibraryHandler() {}

  /**
   * CreateLibrary
   * @brief Compile and create a shared library
   * @param file_path std::string src file path
   * @param name std::string doee name
   * @return ErrorType kSuccess if the execution was well, any other means an
   * error
   */
  virtual kn::ErrorType CreateLibrary(const std::string &file_path,
                                      const std::string &name) = 0;

  /**
   * LoadLibrary
   * @brief Load a shared library from an specific path
   * @param file_path std::string doee shared library file path
   * @param shared_lib void *& to provide the loaded shared library
   * @return ErrorType kSuccess if the execution was well, any other means an
   * error
   */
  virtual kn::ErrorType LoadLibrary(const std::string &file_path,
                                    void *&shared_lib) = 0;

  /**
   * @brief Set the dependencies object with the structure:
   *    -> "-llib_1 -llib_2 -lib_3 ... -llib_n"
   *    Where "lib_k" is the lib dependency
   *
   * @param dependencies std::string lib dependencies
   */
  virtual void set_dependencies(const std::string &dependencies) = 0;

  /**
   * @brief Set the lib folder path object with the structure:
   *    -> "path/to/folder/"
   *
   * @param path std::string & path to folder
   */
  virtual void set_lib_path(const std::vector<std::string> &path) = 0;

  /**
   * @brief Get the dependencies object
   *
   * @return const std::string&
   */
  virtual const std::string &get_dependencies() const noexcept = 0;

  /**
   * @brief Get the lib folder path object with the structure:
   *    -> "path/to/folder/"
   *
   * @param path std::string & path to folder
   */
  virtual const std::vector<std::string> &get_lib_path() const noexcept = 0;

  /**
   * @brief Set the include folder path object with the structure:
   *    -> "path/to/folder/"
   *
   * @param path std::string & path to folder
   */
  virtual void set_include_path(const std::vector<std::string> &path) = 0;

  /**
   * @brief Get the include folder path object with the structure:
   *    -> "path/to/folder/"
   *
   * @param path std::string & path to folder
   */
  virtual const std::vector<std::string> &get_include_path() const noexcept = 0;

  static std::shared_ptr<ILibraryHandler> Build();
};

}  // namespace lib
}  // namespace isms
