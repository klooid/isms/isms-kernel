/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "LibraryHandler.hpp"

#include <dlfcn.h>

#include <cstdio>
#include <execommand.hpp>
#include <iostream>

using namespace isms;

const std::string kComFlag{"-c"};
const std::string kCompiler{"g++"};
const std::string kDefExports{"-D%s_EXPORTS"};
const std::string kFlag{"-fPIC"};
const std::string kInclude{"-I"};
const std::string kLib{"-L"};
const std::string kLibFlags{"-fPIC -shared -Wl,-soname,lib%s.so"};
const std::string kLibDefinition{"%slib%s.so"};
const std::string kObjectDefinition{"%s%s.o"};
const std::string kObjFlag{"-o"};
const std::string kSeparator{" "};
const std::string kSrcDefinition{"%s%s.cpp"};

std::string FormatString(const std::string &format_str,
                         const std::string &arg0 = "",
                         const std::string &arg1 = "") {
  /* Get the size of the string*/
  auto size_s = std::snprintf(nullptr, 0, format_str.c_str(), arg0.c_str(),
                              arg1.c_str()) +
                1;
  if (size_s <= 0) {
    return "";
  }

  auto size = static_cast<size_t>(size_s);
  auto buf = std::make_unique<char[]>(size);
  /* Format the string */
  std::snprintf(buf.get(), size, format_str.c_str(), arg0.c_str(),
                arg1.c_str());

  return std::string(buf.get(), buf.get() + size - 1);
}

kn::ErrorType lib::LibraryHandler::CreateLibrary(const std::string &file_path,
                                                 const std::string &name) {
  auto lib_flags = FormatString(kLibFlags, name);
  auto lib_file = FormatString(kLibDefinition, file_path, name);
  auto obj_file = FormatString(kObjectDefinition, file_path, name);
  auto src_file = FormatString(kSrcDefinition, file_path, name);
  auto def_export = FormatString(kDefExports, name);

  auto cmd_compile = kCompiler + kSeparator + def_export + kSeparator + kFlag +
                     kSeparator + kObjFlag + kSeparator + obj_file +
                     kSeparator + kComFlag + kSeparator + src_file +
                     kSeparator + this->dependencies_ + kSeparator;

  for (auto &path : include_paths_) {
    cmd_compile += kInclude + path + kSeparator;
  }

  for (auto &path : lib_paths_) {
    cmd_compile += kLib + path + kSeparator;
  }

  auto lib_cmd_compile = kCompiler + kSeparator + lib_flags + kSeparator +
                         kObjFlag + kSeparator + lib_file + kSeparator +
                         obj_file + kSeparator + this->dependencies_ +
                         kSeparator;

  for (auto &path : include_paths_) {
    lib_cmd_compile += kInclude + path + kSeparator;
  }

  for (auto &path : lib_paths_) {
    lib_cmd_compile += kLib + path + kSeparator;
  }

  std::cout << "## COMMANDS ##" << std::endl;
  std::cout << cmd_compile << std::endl;
  std::cout << lib_cmd_compile << std::endl;

  auto cmd_result = cmd::Exec(cmd_compile);
  ;
  auto cmd_result_2 = cmd::Exec(lib_cmd_compile);
  ;

  /* This will be changed by the logger */
  std::cout << "Result1: " << cmd_result.get_result() << std::endl;
  std::cout << "Result2: " << cmd_result_2.get_result() << std::endl;

  return kn::ErrorType::kSuccess;
}

kn::ErrorType lib::LibraryHandler::LoadLibrary(const std::string &file_path,
                                               void *&shared_lib) {
  shared_lib = dlopen(file_path.c_str(), RTLD_NOW);

  if (!shared_lib) {
    std::cerr << "Cannot load library: " << dlerror() << std::endl;
    return kn::ErrorType::kCannotCreate;
  }
  dlerror();

  return kn::ErrorType::kSuccess;
}
