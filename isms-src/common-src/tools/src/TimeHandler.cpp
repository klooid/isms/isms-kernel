/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "TimeHandler.hpp"

#include <chrono>
#include <thread>

using namespace isms::kn;

void TimeHandler::Sleep() {
  std::this_thread::sleep_for(
      std::chrono::microseconds(this->thread_sleep_duration_));
}
