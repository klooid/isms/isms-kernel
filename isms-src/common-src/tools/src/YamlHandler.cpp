/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "YamlHandler.hpp"
#include <fstream>

YAML::Node isms::kn::ReadYaml(const std::string &file_path) {
  auto yaml_file = YAML::LoadFile(file_path);

  return yaml_file;
}

isms::kn::ErrorType isms::kn::WriteYaml(const YAML::Node yaml_node,
                                        const std::string &file_path) {
  std::ofstream yaml_ofile(file_path);
  yaml_ofile << yaml_node;
  yaml_ofile.close();
  return isms::kn::ErrorType::kSuccess;
}
