/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

namespace isms {
namespace kn {

/**
 * @brief Holds type of the endpoint to identify how to use it
 *
 */
enum class EndpointType {

  kSub = 0,
  kPub
};
}  // namespace kn
}  // namespace isms
