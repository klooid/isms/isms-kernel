/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

namespace isms {
namespace kn {

/**
 * CommServerProtocol and services type
 * @brief Used to identify the protocol type of the CommServer
 */
enum class CommServerProtocol {

  kMqtt = 0
};
}  // namespace kn
}  // namespace isms
