/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

namespace isms {
namespace kn {

/**
 * ErrorType and services type
 * @brief It is used to track the results generated in a process or a function
 * It provides a better fit for error handling and identification
 */
enum class ErrorType {

  kSuccess = 0,
  kCannotBind,
  kCannotUnbind,
  kCannotCreate,
  kCannotDelete,
  kCannotReceive,
  kCannotSend,
  kCannotCast,
  kDeserializationError,
  kSerializationError,
  kSocketNotFound,
  kServiceNotFound,
  kInvalidPointer,
  kNotFound,
  kCannotClose,
  kCannotOpen,
  kAlreadyExists,
  kCannotUpdate
};
}  // namespace kn
}  // namespace isms
