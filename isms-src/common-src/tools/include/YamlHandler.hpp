/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include <yaml-cpp/yaml.h>

#include "ErrorType.hpp"

namespace isms {
namespace kn {

/**
 * ReadYaml
 * @brief This function provide a YAML::Node to be used, by getting it from an
 * specific file
 * @param file_path std::string, file path to be read
 * @return YAML::Node
 */
YAML::Node ReadYaml(const std::string &file_path);

/**
 * WriteYaml
 * @brief This function writes a YAML::Node into a specific file
 *
 * @param yaml_node YAML::Node information to be stored
 * @param file_path std::string file path
 * @return ErrorType 
 */
ErrorType WriteYaml(const YAML::Node yaml_node, const std::string &file_path);

}  // namespace kn
}  // namespace isms
