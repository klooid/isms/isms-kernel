/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

namespace isms {
namespace kn {

/**
 * Request and services type
 * @brief This enum class is used to define constants related with the type of
 * requests and the type of services. When a request is created and arrives to
 * the kernel, the messages includes one of types described below. When a
 * service is created it shall include the type of service is related with the
 * type of requests that it handle. It is used to associate the request and to
 * prioritize them.
 */
enum class RequestType {

  /* It indicates the change availability payload request */
  kChangeAvailability,

  /* It indicates that the message is a reesponse for the request */
  kResponse,

  /* It indicates the priority for control gate requests */
  kControlGate = 6,

  /* It indicates the priority for connection requests */
  kConnect = 8,

  /* Disconnect request type */
  kDisconnect,

  /* Communication request and service type, it also indicates the priority for
     comm server requests */
  kComm = 10,

  /* Storage and permissions request and service type, it also indicates the
     priority for Resadmin Requests */
  kStoragePermission = 12,

  /* Databases request and service type, it also indicates the priority for
     Database Requests */
  kDb = 14,

  /* Process execution request and service type, also indicates the priority for
     Doer Requests */
  kProc = 16

};
}  // namespace kn
}  // namespace isms
