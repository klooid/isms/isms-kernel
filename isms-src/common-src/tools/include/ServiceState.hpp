/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

namespace isms {
namespace kn {

/**
 * ServiceState type
 * @brief This enum class is used to define constants related to the state of
 * services they indicates the current state of a service
 */
enum class ServiceState {

  /* Service state which indicates when a service is stopped */
  kStop = 0,

  /* Service type which indicates when a service is running */
  kRun,

  /* Service type which indicates when a service is not running anymore */
  kDead,

};
}  // namespace kn
}  // namespace isms
