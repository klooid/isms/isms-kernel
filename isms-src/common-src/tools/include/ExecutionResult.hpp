/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ErrorType.hpp>
#include <string>

namespace isms {
namespace cmd {

/**
 * @brief Holds the result after running a command
 *
 */
class ExecutionResult {
 public:
  ExecutionResult() {}
  virtual ~ExecutionResult() {}

  /**
   * @brief Set the result object
   *
   * @param result const std::string &
   */
  void set_result(const std::string &result);

  /**
   * @brief Set the error code object
   *
   * @param error_code const kn::ErrorType
   */
  void set_error_code(const kn::ErrorType error_code);

  /**
   * @brief Set the error message object
   *
   * @param error_message const std::string &
   */
  void set_error_message(const std::string &error_message);

  /**
   * @brief Get the result object
   *
   * @return const std::string &
   */
  const std::string &get_result() const noexcept;

  /**
   * @brief Get the error code object
   *
   * @return const kn::ErrorType
   */
  const kn::ErrorType get_error_code() const noexcept;

  /**
   * @brief Get the error message object
   *
   * @return const std::string &
   */
  const std::string &get_error_message() const noexcept;

 private:
  std::string result_;
  kn::ErrorType error_code_;
  std::string error_message_;
};

inline void ExecutionResult::set_result(const std::string &result) {
  this->result_ = result;
}

inline void ExecutionResult::set_error_code(const kn::ErrorType error_code) {
  this->error_code_ = error_code;
}

inline void ExecutionResult::set_error_message(const std::string &error_message) {
  this->error_message_ = error_message;
}

inline const std::string &ExecutionResult::get_result() const noexcept {
  return this->result_;
}

inline const kn::ErrorType ExecutionResult::get_error_code() const noexcept {
  return this->error_code_;
}

inline const std::string &ExecutionResult::get_error_message() const noexcept {
  return this->error_message_;
}

}  // namespace cmd
}  // namespace isms
