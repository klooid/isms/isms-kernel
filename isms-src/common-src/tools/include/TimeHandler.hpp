/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

namespace isms {
namespace kn {

/**
 * TimeHandler type
 * @brief This handles some related to time and threads, it is intended to be
 * inherited for other classes
 */
class TimeHandler {
 public:
  TimeHandler() : thread_sleep_duration_{1}, recv_timeout_{-1} {};
  virtual ~TimeHandler(){};
  /**
   * get_thread_sleep_duration
   * @brief Provides the sleep duration time for threads in microseconds
   * @return unsigned int, the sleep duration time in microseconds
   */
  const unsigned int get_thread_sleep_duration() const noexcept;

  /**
   * set_thread_sleep_duration
   * @brief Sets the sleep duration time for threads in microseconds
   * @param thread_sleep_duration unsigned int, sleep duration time which is set
   * by default as 1us
   */
  void set_thread_sleep_duration(const unsigned int thread_sleep_duration);

  /**
   * get_thread_sleep_duration
   * @brief Provides the recv timeout which indicates the requests waiting time
   * for sockets recv in miliseconds
   * @return unsigned int, the recv duration
   */
  const int get_recv_timeout() const noexcept;

  /**
   * set_recv_timeout
   * @brief Sets the recv timeout which indicates the requests waiting time
   * for sockets recv in miliseconds
   * @param recv_timeout int, recv timeout for sockets,it is configured by
   * default as -1 which indicates an infinite timeout.
   */
  void set_recv_timeout(const int recv_timeout);

 protected:
  unsigned int thread_sleep_duration_;
  int recv_timeout_;

  /**
   * Sleep
   * @brief Sends the current thread to sleep for the amount of time
   * specified in the parameter thread_sleep_duration_ in microseconds, it is
   * helpful when indefined loops are runing to avoid wasting processing
   * resources in the same task. It is configured by default as 1 us.
   */
  void Sleep();
};

inline const int TimeHandler::get_recv_timeout() const noexcept {
  return this->recv_timeout_;
}

inline void TimeHandler::set_recv_timeout(const int recv_timeout) {
  this->recv_timeout_ = recv_timeout;
}

inline const unsigned int TimeHandler::get_thread_sleep_duration()
    const noexcept {
  return this->thread_sleep_duration_;
}

inline void TimeHandler::set_thread_sleep_duration(
    const unsigned int thread_sleep_duration) {
  this->thread_sleep_duration_ = thread_sleep_duration;
}

}  // namespace kn
}  // namespace isms
