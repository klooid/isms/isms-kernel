/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

namespace isms {
namespace kn {

/**
 * ProcessState
 * @brief This enum class is used to define constants related to the Processes
 * State each state is used to know the current condition
 */
enum class ProcessState {

  /* Indicates that the process is a new process and shall be created */
  kNewProcess = 0,

  /* Indicates when a process is ready to be executeds */
  kReady,

  /* Indicates when a process is blocked, waiting for an event to continue its
     operation */
  kBlocked,

  /* Indicates when a process is executing */
  kRunning,

  /* Indicates when a process is no longer running */
  kFinished
};
}  // namespace kn
}  // namespace ism
