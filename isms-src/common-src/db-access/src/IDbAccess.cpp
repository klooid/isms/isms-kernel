/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "IDbAccess.hpp"

#include "DbAccess.hpp"

using namespace isms::db;

std::shared_ptr<IDbAccess> IDbAccess::Build() {
  return std::make_shared<DbAccess>();
}
