/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "DbAccess.hpp"

#include <INIReader.h>
#include <fmt/core.h>

#include <nlohmann/json.hpp>
#include <vector>

using namespace isms;
using json = nlohmann::json;

const std::string kCondQuery{"{} WHERE {}"};
const std::string kCredentialsFile{""};
const std::string kInsertQuery{"INSERT INTO {} ({}) VALUES ({})"};
const std::string kSelectQuery{"SELECT {} FROM {}"};
const std::string kUpdateQuery{"UPDATE {} SET {}"};
const std::string kDeleteQuery{"DELETE FROM {} WHERE {}"};

/* Credentiales consts */

const std::string kConnSection{"db_connection"};
const std::string kDefaultValue{"null"};
const std::string kGeneralSection{"db_general"};

db::QueryResult db::DbAccess::Select(const std::string &table,
                                     const std::string &colums,
                                     const std::string &condition /*= ""*/) {
  db::QueryResult result;
  MYSQL_ROW row;
  MYSQL_FIELD *field;
  std::string column_temp;
  std::vector<std::string> columns;
  auto json_array = json::array();

  auto query = fmt::format(kSelectQuery, colums, table);
  if (!condition.empty()) query = fmt::format(kCondQuery, query, condition);

  auto exe_result = mysql_query(this->db_conn_, query.c_str());

  if (exe_result != 0) {
    result.set_error_type(kn::ErrorType::kNotFound);
    auto error_message =
        "SELECT failed by: " + std::string(mysql_error(this->db_conn_));
    result.set_result(json_array);
    result.set_error_message(error_message);
    return result;
  }

  auto query_result = mysql_store_result(this->db_conn_);

  /* Retrieve the data */
  int num_fields = mysql_num_fields(query_result);

  /* Extract the rows information */

  while (field = mysql_fetch_field(query_result)) {
    columns.push_back(field->name);
  }

  while ((row = mysql_fetch_row(query_result))) {
    auto temp_json = json::object();
    for (int i = 0; i < num_fields; i++) {
      auto key = columns[i];
      temp_json[key] = row[i] ? row[i] : "NULL";
    }
    json_array.push_back(temp_json);
  }

  result.set_result(json_array);
  result.set_error_type(kn::ErrorType::kSuccess);

  mysql_free_result(query_result);

  return result;
}

db::QueryResult db::DbAccess::Insert(const std::string &table,
                                     const std::string &colums,
                                     const std::string &values) {
  QueryResult result;
  auto json_result = json::object();

  if (table.empty() || colums.empty() || values.empty()) {
    result.set_error_message("Table, Columns and Values must be specified");
    result.set_error_type(kn::ErrorType::kCannotBind);
    return result;
  }

  auto query = fmt::format(kInsertQuery, table, colums, values);

  auto exe_result = mysql_query(this->db_conn_, query.c_str());

  if (exe_result != 0) {
    result.set_error_type(kn::ErrorType::kAlreadyExists);
    auto error_message =
        "INSERT failed by: " + std::string(mysql_error(this->db_conn_));
    result.set_error_message(error_message);

    return result;
  }

  auto last_id = mysql_insert_id(this->db_conn_);

  json_result["id"] == last_id;

  result.set_error_type(kn::ErrorType::kSuccess);
  result.set_result(json_result);

  return result;
}

db::QueryResult db::DbAccess::Update(const std::string &table,
                                     const std::string &colums_values,
                                     const std::string &condition /*= ""*/) {
  QueryResult result;
  auto json_result = json::object();

  if (table.empty() || colums_values.empty()) {
    result.set_error_message("Table, Columns and Values must be specified");
    result.set_error_type(kn::ErrorType::kCannotBind);
    return result;
  }

  auto query = fmt::format(kUpdateQuery, table, colums_values);
  if (!condition.empty()) query = fmt::format(kCondQuery, query, condition);

  auto exe_result = mysql_query(this->db_conn_, query.c_str());

  if (exe_result != 0) {
    result.set_error_type(kn::ErrorType::kCannotUpdate);
    auto error_message =
        "UPDATE failed by: " + std::string(mysql_error(this->db_conn_));
    result.set_error_message(error_message);
    return result;
  }

  auto affected_rows = mysql_affected_rows(this->db_conn_);
  json_result["affected-rows"] == affected_rows;

  if (affected_rows == 0) {
    result.set_error_type(kn::ErrorType::kCannotUpdate);
    auto error_message =
        "UPDATE affected rows: " + std::to_string(affected_rows);
    result.set_error_message(error_message);
    return result;
  }

  auto last_id = mysql_insert_id(this->db_conn_);

  json_result["id"] == last_id;

  result.set_error_type(kn::ErrorType::kSuccess);
  result.set_result(json_result);

  return result;
}

db::QueryResult db::DbAccess::Delete(const std::string &table,
                                     const std::string &condition) {
  QueryResult result;
  auto json_result = json::object();

  if (table.empty() || condition.empty()) {
    result.set_error_message("Table and Condition must be specified");
    result.set_error_type(kn::ErrorType::kCannotBind);
    return result;
  }

  auto query = fmt::format(kDeleteQuery, table, condition);

  auto exe_result = mysql_query(this->db_conn_, query.c_str());

  if (exe_result != 0) {
    result.set_error_type(kn::ErrorType::kCannotCreate);
    auto error_message =
        "DELETE failed by: " + std::string(mysql_error(this->db_conn_));
    result.set_error_message(error_message);
    return result;
  }

  auto affected_rows = mysql_affected_rows(this->db_conn_);
  json_result["affected-rows"] == affected_rows;

  if (affected_rows == 0) {
    result.set_error_type(kn::ErrorType::kCannotUpdate);
    auto error_message =
        "DELETE affected rows: " + std::to_string(affected_rows);
    result.set_error_message(error_message);
    return result;
  }

  auto last_id = mysql_insert_id(this->db_conn_);

  json_result["id"] == last_id;

  result.set_error_type(kn::ErrorType::kSuccess);
  result.set_result(json_result);

  return result;
}

db::DbCredentials db::DbAccess::ReadCredentials() {
  db::DbCredentials credentials;

  INIReader reader{this->ini_file_};

  if (reader.ParseError() < 0) {
    credentials.set_error(kn::ErrorType::kNotFound);
    return credentials;
  }

  credentials.set_database_ip(
      reader.GetString(kConnSection, "host", kDefaultValue));

  credentials.set_database_port(reader.GetInteger(kConnSection, "port", 0));

  credentials.set_socket_file_path(
      reader.GetString(kConnSection, "sock_file", kDefaultValue));

  credentials.set_database_name(
      reader.GetString(kConnSection, "dbname", kDefaultValue));

  credentials.set_password(
      reader.GetString(kGeneralSection, "password", kDefaultValue));

  credentials.set_username(
      reader.GetString(kGeneralSection, "user", kDefaultValue));

  credentials.set_error(kn::ErrorType::kSuccess);

  return credentials;
}

kn::ErrorType db::DbAccess::Init() {
  MYSQL *connection_done;

  /* Check the database name */
  this->db_conn_ = mysql_init(NULL);

  if (NULL == this->db_conn_) return kn::ErrorType::kInvalidPointer;
  /* Get the credentials to connect */
  auto credentials = ReadCredentials();

  if (credentials.get_error() != kn::ErrorType::kSuccess)
    return credentials.get_error();

  if (this->database_name_.empty())
    this->set_database_name(credentials.get_database_name());

  auto user = credentials.get_username().c_str();
  auto password = credentials.get_password().c_str();

  /* Establish the connection to the server */
  if (this->connect_to_server_) { /* Connection through the host */
    auto host = credentials.get_database_ip().c_str();
    auto port = credentials.get_database_port();
    connection_done =
        mysql_real_connect(this->db_conn_, host, user, password,
                           this->database_name_.c_str(), port, NULL, 0);
  } else { /* Connection through the socket */
    auto socket_file = credentials.get_socket_file_path().c_str();
    connection_done =
        mysql_real_connect(this->db_conn_, NULL, user, password,
                           this->database_name_.c_str(), NULL, socket_file, 0);
  }

  if (NULL == connection_done) {
    mysql_close(this->db_conn_);
    return kn::ErrorType::kCannotCreate;
  }

  return kn::ErrorType::kSuccess;
}
