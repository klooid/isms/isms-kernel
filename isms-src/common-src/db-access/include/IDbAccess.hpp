/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <ErrorType.hpp>
#include <memory>
#include <string>

#include "QueryResult.hpp"

namespace isms {
namespace db {

class IDbAccess {
 public:
  /**
   * @brief Construct a new IDbAccess object
   *
   */
  IDbAccess() {}

  /**
   * @brief Destroy the IDbAccess object
   *
   */
  virtual ~IDbAccess() {}

  /**
   * @brief Makes a simple SELECT query the database
   *
   * @param table const std::string &, table to be affected
   * @param colums const std::string &, columns to be affected
   * @param condition const std::string &, condition to evaluate
   * @return QueryResult, an object with the result of the query
   */
  virtual QueryResult Select(const std::string &table,
                             const std::string &colums,
                             const std::string &condition = "") = 0;

  /**
   * @brief Makes a simple INSERT query in the database
   *
   * @param table const std::string &, table to be affected
   * @param colums const std::string &, columns to be affected
   * @param values const std::string &, values to insert
   * @return QueryResult, an object with the result of the query
   */
  virtual QueryResult Insert(const std::string &table,
                             const std::string &colums,
                             const std::string &values) = 0;

  /**
   * @brief Makes a simple UPDATE in a record of the database
   *
   * @param table const std::string &, table to be affected
   * @param colums_values const std::string &, columns and values to be affected
   * with the sintax: [column = value, ...]
   * @param condition const std::string &, condition to evaluate
   * @return QueryResult, an object with the result of the query
   */
  virtual QueryResult Update(const std::string &table,
                             const std::string &colums_values,
                             const std::string &condition = "") = 0;

  /**
   * @brief Deletes a record from a table in the database
   *
   * @param table const std::string &, table to use
   * @param condition const std::string &, condition wich indicates the value to
   * compared
   * @return QueryResult, an object with the result of the query
   */
  virtual QueryResult Delete(const std::string &table,
                             const std::string &condition) = 0;

  /**
   * @brief Set the database name object
   *
   * @param db_name const std::string &, name of the database to be used
   */
  virtual void set_database_name(const std::string &db_name) = 0;

  /**
   * @brief Get the database name object
   *
   * @return const std::string& database name
   */
  virtual const std::string &get_database_name() const noexcept = 0;

  /**
   * @brief Set the Ini config file path object
   *
   * @param ini_file const std::string &, database Ini config
   */
  virtual void set_ini_file_path(const std::string &ini_file) = 0;

  /**
   * @brief Get the Ini config file path object
   *
   * @return const std::string&, database Ini config file
   */
  virtual const std::string &get_ini_file_path() const noexcept = 0;

  /**
   * @brief Set the connect to server object
   *
   * @param connect, bool
   */
  virtual void set_connect_to_server(const bool connect) = 0;

  /**
   * @brief Get the connect to server object
   *
   * @return true
   * @return false
   */
  virtual const bool get_connect_to_server() const noexcept = 0;

  /**
   * @brief Connects to the database
   *
   * @return kn::ErrorType
   */
  virtual kn::ErrorType Init() = 0;

  /**
   * @brief Creates an IDbAccess object
   *
   * @return std::shared_ptr<IDbAccess>
   */
  static std::shared_ptr<IDbAccess> Build();
};

}  // namespace db
}  // namespace isms
