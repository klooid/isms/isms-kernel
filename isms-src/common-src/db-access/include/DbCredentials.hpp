/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <ErrorType.hpp>
#include <ExecutionResult.hpp>
#include <IPayload.hpp>
#include <memory>
#include <string>

namespace isms {
namespace db {

class DbCredentials {
 public:
  /**
   * @brief Construct a new DbCredentials object
   *
   */
  DbCredentials() {}

  /**
   * @brief Destroy the DbCredentials object
   *
   */
  virtual ~DbCredentials() {}

  /**
   * @brief Set the database name object
   *
   * @param db_name const std::string &, name of the database to be used
   */
  void set_database_name(const std::string &db_name);

  /**
   * @brief Get the database name object
   *
   * @return const std::string& database name
   */
  const std::string &get_database_name() const noexcept;

  /**
   * @brief Set the Socket file path object
   *
   * @param socket_file const std::string &, database socket file
   */
  void set_socket_file_path(const std::string &socket_file);

  /**
   * @brief Get the Socket file path object
   *
   * @return const std::string&, database socket file
   */
  const std::string &get_socket_file_path() const noexcept;

  /**
   * @brief Set the Database IP object
   *
   * @param ip const std::string &, the database ip
   */
  void set_database_ip(const std::string &ip);

  /**
   * @brief Get the Database IP object
   *
   * @return const std::string& database ip
   */
  const std::string &get_database_ip() const noexcept;

  /**
   * @brief Set the username object
   *
   * @param username const std::string &, the username
   */
  void set_username(const std::string &username);

  /**
   * @brief Get the username object
   *
   * @return const std::string& username
   */
  const std::string &get_username() const noexcept;

  /**
   * @brief Set the password object
   *
   * @param password const std::string &, the password
   */
  void set_password(const std::string &password);

  /**
   * @brief Get the password object
   *
   * @return const std::string& password
   */
  const std::string &get_password() const noexcept;

  /**
   * @brief Set the Database Port object
   *
   * @param port const unsigned int, database port
   */
  void set_database_port(const unsigned int port);

  /**
   * @brief Get the Database Port object
   *
   * @return const unsigned int, database port
   */
  const unsigned int get_database_port() const noexcept;

  /**
   * @brief Set the error object
   *
   * @param error
   */
  void set_error(const kn::ErrorType error);

  /**
   * @brief Get the error object
   *
   * @return const kn::ErrorType
   */
  const kn::ErrorType get_error() const noexcept;

 private:
  std::string ip_;
  std::string database_name_;
  std::string username_;
  std::string password_;
  std::string socket_file_;
  kn::ErrorType error_type_;
  unsigned int port_;
};

inline void DbCredentials::set_database_name(const std::string &db_name) {
  this->database_name_ = db_name;
}

inline const std::string &DbCredentials::get_database_name() const noexcept {
  return this->database_name_;
}

inline void DbCredentials::set_socket_file_path(
    const std::string &socket_file) {
  this->socket_file_ = socket_file;
}

inline const std::string &DbCredentials::get_socket_file_path() const noexcept {
  return this->socket_file_;
}

inline void DbCredentials::set_database_ip(const std::string &ip) {
  this->ip_ = ip;
}

inline const std::string &DbCredentials::get_database_ip() const noexcept {
  return this->ip_;
}

inline void DbCredentials::set_username(const std::string &username) {
  this->username_ = username;
}

inline const std::string &DbCredentials::get_username() const noexcept {
  return this->username_;
}
inline void DbCredentials::set_password(const std::string &password) {
  this->password_ = password;
}

inline const std::string &DbCredentials::get_password() const noexcept {
  return this->password_;
}

inline void DbCredentials::set_database_port(const unsigned int port) {
  this->port_ = port;
}

inline const unsigned int DbCredentials::get_database_port() const noexcept {
  return this->port_;
}

inline void DbCredentials::set_error(const kn::ErrorType error) {
  this->error_type_ = error;
}
inline const kn::ErrorType DbCredentials::get_error() const noexcept {
  return this->error_type_;
}

}  // namespace db
}  // namespace isms
