/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <ErrorType.hpp>
#include <ExecutionResult.hpp>
#include <IPayload.hpp>
#include <memory>
#include <nlohmann/json.hpp>
#include <string>

namespace isms {
namespace db {

class QueryResult {
 public:
  /**
   * @brief Construct a new QueryResult object
   *
   */
  QueryResult() {}

  /**
   * @brief Destroy the QueryResult object
   *
   */
  virtual ~QueryResult() {}

  /**
   * @brief Set the error type object
   *
   * @param error
   */
  void set_error_type(const kn::ErrorType error);

  /**
   * @brief Get the error type object
   *
   * @return const kn::ErrorType
   */
  const kn::ErrorType get_error_type() const noexcept;

  /**
   * @brief Set the result JSON  object
   *
   * @param result
   */
  void set_result(const nlohmann::json &result);

  /**
   * @brief Get the result JSON object
   *
   * @return const nlohmann::json&, JSON  with the rows
   */
  const nlohmann::json &get_result() const noexcept;

  /**
   * @brief Set the result JSON  object
   *
   * @param error_message, std::string
   */
  void set_error_message(const std::string &error_message);

  /**
   * @brief Get the error message  object
   *
   * @return const std::string&, error_message
   */
  const std::string &get_error_message() const noexcept;

 private:
  kn::ErrorType error_type_;
  std::string error_message_;
  nlohmann::json result_;
};

inline void QueryResult::set_error_type(const kn::ErrorType error) {
  this->error_type_ = error;
}

inline const kn::ErrorType QueryResult::get_error_type() const noexcept {
  return this->error_type_;
}

inline void QueryResult::set_result(const nlohmann::json &result) {
  this->result_ = result;
}

inline const nlohmann::json &QueryResult::get_result() const noexcept {
  return this->result_;
}

inline void QueryResult::set_error_message(const std::string &error_message) {
  this->error_message_ = error_message;
}

inline const std::string &QueryResult::get_error_message() const noexcept {
  return this->error_message_;
}

}  // namespace db
}  // namespace isms
