/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <mariadb/mysql.h>

#include "DbCredentials.hpp"
#include "IDbAccess.hpp"

namespace isms {
namespace db {

class DbAccess : public IDbAccess {
 public:
  /**
   * @brief Construct a new DbAccess object
   *
   */
  DbAccess() : connect_to_server_{false} {}

  /**
   * @brief Destroy the DbAccess object
   *
   */
  virtual ~DbAccess() {}

  /**
   * @brief Makes a simple SELECT query the database
   *
   * @param table const std::string &, table to be affected
   * @param colums const std::string &, columns to be affected
   * @param condition const std::string &, condition to evaluate
   * @return QueryResult, an object with the result of the query
   */
  QueryResult Select(const std::string &table, const std::string &colums,
                     const std::string &condition = "") override;

  /**
   * @brief Makes a simple INSERT query in the database
   *
   * @param table const std::string &, table to be affected
   * @param colums const std::string &, columns to be affected
   * @param values const std::string &, values to insert
   * @return QueryResult, an object with the result of the query
   */
  QueryResult Insert(const std::string &table, const std::string &colums,
                     const std::string &values) override;

  /**
   * @brief Makes a simple UPDATE in a record of the database
   *
   * @param table const std::string &, table to be affected
   * @param colums_values const std::string &, columns and values to be affected
   * with the sintax: [column = value, ...]
   * @param condition const std::string &, condition to evaluate
   * @return QueryResult, an object with the result of the query
   */
  QueryResult Update(const std::string &table, const std::string &colums_values,
                     const std::string &condition = "") override;

  /**
   * @brief Deletes a record from a table in the database
   *
   * @param table const std::string &, table to use
   * @param condition const std::string &, condition wich indicates the value to
   * compared
   * @return QueryResult, an object with the result of the query
   */
  QueryResult Delete(const std::string &table,
                     const std::string &condition) override;

  /**
   * @brief Set the database name object
   *
   * @param db_name const std::string &, name of the database to be used
   */
  void set_database_name(const std::string &db_name) override;

  /**
   * @brief Get the database name object
   *
   * @return const std::string& database name
   */
  const std::string &get_database_name() const noexcept override;

  /**
   * @brief Set the Socket file path object
   *
   * @param ini_file const std::string &, database socket file
   */
  void set_ini_file_path(const std::string &ini_file) override;

  /**
   * @brief Get the Socket file path object
   *
   * @return const std::string&, database socket file
   */
  const std::string &get_ini_file_path() const noexcept override;

  /**
   * @brief Set the connect to server object
   *
   * @param connect, bool
   */
  void set_connect_to_server(const bool connect) override;

  /**
   * @brief Get the connect to server object
   *
   * @return true
   * @return false
   */
  const bool get_connect_to_server() const noexcept override;

  /**
   * @brief Connects to the database
   *
   * @return kn::ErrorType
   */
  kn::ErrorType Init() override;

 private:
  std::string database_name_;
  std::string ini_file_;
  MYSQL *db_conn_;
  bool connect_to_server_;

  /**
   * @brief Retrieves the database connection credentials from the specified ini
   * configuration file
   *
   * @return db::DbCredentials, object that contains the credentials
   */
  DbCredentials ReadCredentials();
};

inline void DbAccess::set_database_name(const std::string &db_name) {
  this->database_name_ = db_name;
}

inline const std::string &DbAccess::get_database_name() const noexcept {
  return this->database_name_;
}

inline void DbAccess::set_ini_file_path(const std::string &ini_file) {
  this->ini_file_ = ini_file;
}

inline const std::string &DbAccess::get_ini_file_path() const noexcept {
  return this->ini_file_;
}

inline void DbAccess::set_connect_to_server(const bool connect) {
  this->connect_to_server_ = connect;
}

inline const bool DbAccess::get_connect_to_server() const noexcept {
  return this->connect_to_server_;
}

}  // namespace db
}  // namespace isms
