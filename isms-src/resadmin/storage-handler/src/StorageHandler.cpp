/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "StorageHandler.hpp"

#include <filesystem>
#include <fstream>

using namespace isms;

namespace fs = std::filesystem;
const std::string kProjectDir{"projects"};
const std::string kDoeesDir{"doees"};
const std::string kDoeesConfFile{"doess_conf.yml"};
const std::string kCommServerDir{"comm-server"};
const std::string kCommServerConfFile{"comm_server_conf.yml"};
const std::string kProjectConfigFile{"proyect.yml"};
const std::string kSeparator{"/"};

kn::ErrorType resadmin::StorageHandler::CreateFolder(
    const std::string &directory) {
  auto output_directory = this->base_directory_ + kSeparator + directory;

  if (fs::exists(output_directory)) return kn::ErrorType::kAlreadyExists;

  auto create_result = fs::create_directories(output_directory);

  if (!create_result) return kn::ErrorType::kCannotCreate;

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::StorageHandler::RemoveAll(const std::string &path) {
  auto output_directory = this->base_directory_ + kSeparator + path;

  if (!fs::exists(output_directory)) return kn::ErrorType::kNotFound;

  auto rm_result = fs::remove_all(output_directory);

  if (!rm_result) return kn::ErrorType::kSuccess;

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::StorageHandler::AddFile(const std::string &file) {
  auto path = this->base_directory_ + kSeparator + file;
  std::fstream new_file;

  if (fs::exists(path)) return kn::ErrorType::kAlreadyExists;

  new_file.open(path, std::ios_base::app);

  if (!new_file.is_open()) return kn::ErrorType::kCannotCreate;

  new_file.close();

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::StorageHandler::RemoveSingle(const std::string &path) {
  auto rm_path = this->base_directory_ + kSeparator + path;

  if (!fs::exists(rm_path)) return kn::ErrorType::kNotFound;

  auto rm_result = fs::remove(rm_path);

  if (!rm_result) return kn::ErrorType::kCannotDelete;

  return kn::ErrorType::kSuccess;
}

cmd::ExecutionResult resadmin::StorageHandler::CreateProjectDirectories(
    const std::string &project) {
  cmd::ExecutionResult result;
  auto doees_dir = project + kSeparator + kDoeesDir;
  auto comm_server_dir = project + kSeparator + kCommServerDir;
  auto doees_conf_path = doees_dir + kSeparator + kDoeesConfFile;
  auto comm_server_conf_path =
      comm_server_dir + kSeparator + kCommServerConfFile;
  auto project_conf_path = project + kSeparator + kProjectConfigFile;

  if (fs::exists(this->base_directory_ + kSeparator + project)) {
    result.set_error_code(kn::ErrorType::kCannotCreate);
    result.set_error_message(
        "Cannot create project base directory, directory already exists: " +
        project);
    return result;
  }

  /* Crete the project base dir */
  auto creation_result = this->CreateFolder(project);

  if (creation_result != kn::ErrorType::kSuccess) {
    result.set_error_code(creation_result);
    result.set_error_message("Cannot create project base directory");
    return result;
  }
  this->AddFile(project_conf_path);

  /* Crete the Doees base dir */
  creation_result = this->CreateFolder(doees_dir);

  if (creation_result != kn::ErrorType::kSuccess) {
    result.set_error_code(creation_result);
    result.set_error_message("Cannot create Doees base directory");
    return result;
  }
  this->AddFile(doees_conf_path);

  /* Crete the CommServer base dir */
  creation_result = this->CreateFolder(comm_server_dir);

  if (creation_result != kn::ErrorType::kSuccess) {
    result.set_error_code(creation_result);
    result.set_error_message("Cannot create CommServer base directory");
    return result;
  }
  this->AddFile(comm_server_conf_path);

  result.set_error_code(kn::ErrorType::kSuccess);
  result.set_result(project);
  return result;
}

cmd::ExecutionResult resadmin::StorageHandler::RemoveProjectDirectories(
    const std::string &project) {
  cmd::ExecutionResult result;

  if (!fs::exists(this->base_directory_ + kSeparator + project)) {
    result.set_error_code(kn::ErrorType::kNotFound);
    result.set_error_message(
        "Cannot remove project base directory, directory does not exists: " +
        project);
    return result;
  }

  auto rm_result = this->RemoveAll(project);

  if (rm_result != kn::ErrorType::kSuccess) {
    result.set_error_code(rm_result);
    result.set_error_message("Cannot remove all the project tree: " +
                             project);
    return result;
  }

  result.set_error_code(kn::ErrorType::kSuccess);
  result.set_result(project);
  return result;
}
