/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "IStorageHandler.hpp"

#include "StorageHandler.hpp"

using namespace isms::resadmin;

std::shared_ptr<IStorageHandler> IStorageHandler::Build() {
  return std::make_shared<StorageHandler>();
}
