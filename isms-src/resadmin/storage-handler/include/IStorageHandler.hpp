/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <memory>
#include <string>

/* Internal */
#include <ErrorType.hpp>
#include <ExecutionResult.hpp>
#include <IPayload.hpp>

namespace isms {
namespace resadmin {

class IStorageHandler {
 public:
  /**
   * @brief Construct a new IStorageHandler object
   *
   */
  IStorageHandler() {}

  /**
   * @brief Destroy the IStorageHandler object
   *
   */
  virtual ~IStorageHandler() {}

  /**
   * @brief Create a Folder in an specific directory for an specific project
   *
   * @param payload const std::string &file
   * @return kn::ErrorType
   */
  virtual kn::ErrorType CreateFolder(const std::string &directory) = 0;

  /**
   * @brief Removes all the sub directories from the given path for an specific
   * project
   *
   * @param payload const std::string &path
   * @return kn::ErrorType
   */
  virtual kn::ErrorType RemoveAll(const std::string &path) = 0;

  /**
   * @brief Add a file in a specific
   *
   * @param payload const std::string &file
   * @return kn::ErrorType
   */
  virtual kn::ErrorType AddFile(const std::string &file) = 0;

  /**
   * @brief Removes a single file from the given path of a project
   *
   * @param payload const std::string &path
   * @return kn::ErrorType
   */
  virtual kn::ErrorType RemoveSingle(const std::string &path) = 0;

  /**
   * @brief Creates a Project Directories tree
   *
   * @param project const std::string &, name of the project
   * @return cmd::ExecutionResult, if the error code is ErrorType::kSuccess, the
   * result is the project base directory path
   */
  virtual cmd::ExecutionResult CreateProjectDirectories(
      const std::string &project) = 0;

  /**
   * @brief Removes a Project Directories tree
   *
   * @param project const std::string &, name of the project
   * @return cmd::ExecutionResult, if the error code is ErrorType::kSuccess, the
   * result is the project base directory path
   */
  virtual cmd::ExecutionResult RemoveProjectDirectories(
      const std::string &project) = 0;

  /**
   * @brief Set the base directory path
   *
   * @param directory const std::string &, path name
   */
  virtual void set_base_directory(const std::string &directory) = 0;

  /**
   * @brief Creates an IStorageHandler object
   *
   * @return std::shared_ptr<IStorageHandler>
   */
  static std::shared_ptr<IStorageHandler> Build();
};

}  // namespace resadmin
}  // namespace isms
