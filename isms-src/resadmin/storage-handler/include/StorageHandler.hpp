/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <memory>

#include "IStorageHandler.hpp"

namespace isms {
namespace resadmin {

class StorageHandler : public IStorageHandler {
 public:
  /**
   * @brief Construct a new StorageHandler object
   *
   */
  StorageHandler() {}

  /**
   * @brief Destroy the StorageHandler object
   *
   */
  virtual ~StorageHandler() {}

  /**
   * @brief Create a Folder in an specific directory for an specific project
   *
   * @param payload const std::string &file
   * @return kn::ErrorType
   */
  kn::ErrorType CreateFolder(const std::string &directory) override;

  /**
   * @brief Removes all the sub directories from the given path for an specific
   * project
   *
   * @param payload const std::string &path
   * @return kn::ErrorType
   */
  kn::ErrorType RemoveAll(const std::string &path) override;

  /**
   * @brief Add a file in a specific
   *
   * @param payload const std::string &file
   * @return kn::ErrorType
   */
  kn::ErrorType AddFile(const std::string &file) override;

  /**
   * @brief Removes a single file from the given path of a project
   *
   * @param payload const std::string &path
   * @return kn::ErrorType
   */
  kn::ErrorType RemoveSingle(const std::string &path) override;

  /**
   * @brief Creates a Project Directories tree
   *
   * @param project const std::string &, name of the project
   * @return cmd::ExecutionResult, if the error code is ErrorType::kSuccess, the
   * result is the project base directory path
   */
  cmd::ExecutionResult CreateProjectDirectories(
      const std::string &project) override;

  /**
   * @brief Removes a Project Directories tree
   *
   * @param project const std::string &, name of the project
   * @return cmd::ExecutionResult, if the error code is ErrorType::kSuccess, the
   * result is the project base directory path
   */
  cmd::ExecutionResult RemoveProjectDirectories(
      const std::string &project) override;

  /**
   * @brief Set the base directory path
   *
   * @param directory const std::string &, path name
   */
  void set_base_directory(const std::string &directory) override;

 private:
  std::string base_directory_;
};

inline void StorageHandler::set_base_directory(const std::string &directory) {
  this->base_directory_ = directory;
}

}  // namespace resadmin
}  // namespace isms
