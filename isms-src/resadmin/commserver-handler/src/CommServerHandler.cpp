/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "CommServerHandler.hpp"

#include <fmt/core.h>

#include <CommPayload.hpp>
#include <YamlHandler.hpp>
#include <iostream>

using namespace isms;

const std::string kCommServerDirFormat{"{}/comm-server/{}"};
const std::string kBaseCommServerDirFormat{"{}/{}"};
const std::string kCommServerFileName{"comm-server-conf.yml"};
const std::string kCommServerMqttFile{"mqtt-comm-server.yml"};

/**
 * @brief Reads the configuration file that contains the general comm servers
 * information
 *
 * @param base_path const std::string &, the base path where the project is
 * located
 * @param project_path const std::string &, the project path to use
 * @return YAML::Node
 */
YAML::Node ReadConfFile(const std::string &base_path,
                        const std::string &project_path) {
  auto cs_conf_file =
      fmt::format(kCommServerDirFormat, project_path, kCommServerFileName);
  auto cs_conf_file_path =
      fmt::format(kBaseCommServerDirFormat, base_path, cs_conf_file);
  std::cout << "COMMIT CONF . " << cs_conf_file_path << std::endl;
  auto comm_servers_yaml = kn::ReadYaml(cs_conf_file_path);
  std::cout << "COMMIT CONF . " << cs_conf_file_path << std::endl;

  return comm_servers_yaml;
}

/**
 * @brief Writes the configuration file that contains the general comm servers
 * information
 *
 * @param base_path const std::string &, the base path where the project is
 * located
 * @param project_path const std::string &, the project path to use
 * @param comm_servers_yaml const YAML::Node &, the YAML node to write
 * @return kn::ErrorType
 */
kn::ErrorType WriteConfFile(const std::string &base_path,
                            const std::string &project_path,
                            const YAML::Node &comm_servers_yaml) {
  auto cs_conf_file =
      fmt::format(kCommServerDirFormat, project_path, kCommServerFileName);
  auto cs_conf_file_path =
      fmt::format(kBaseCommServerDirFormat, base_path, cs_conf_file);

  auto write_result = kn::WriteYaml(comm_servers_yaml, cs_conf_file_path);

  return write_result;
}

kn::ErrorType resadmin::CommServerHandler::AddCommServerEntity(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  kn::ErrorType result;

  auto res_payload = kn::IPayload::CastPayload<kn::CommPayload>(payload);
  auto protocol = res_payload->get_protocol();

  switch (protocol) {
    case kn::CommServerProtocol::kMqtt:
      result = DefineMqttEntity(payload, project_path);
      break;

    default:
      result = kn::ErrorType::kNotFound;
      this->logger_->ErrorLog(
          "Cannot add CommServer Entity, protocolo not found: " +
          std::to_string(static_cast<int>(protocol)));
      break;
  }

  return result;
}

kn::ErrorType resadmin::CommServerHandler::Init(
    const std::string &project_path) {
  auto node_file = ReadConfFile(this->base_path_, project_path);

  if (!node_file.IsNull()) {
    this->logger_->ErrorLog(
        "Configuration file already initialized for project: " + project_path);
    return kn::ErrorType::kAlreadyExists;
  }

  auto configuration_file = YAML::Node();
  configuration_file["description"] =
      "This file holds the information related to the CommServer entities "
      "configured for this project";
  configuration_file["comm-servers"] = YAML::Node();

  auto write_result =
      WriteConfFile(this->base_path_, project_path, configuration_file);
  if (write_result != kn::ErrorType::kSuccess) {
    this->logger_->FatalLog(
        "Cannot configure the CommServer configuration file for the project: " +
        project_path);
    return write_result;
  }

  this->logger_->InfoLog(
      "CommServer configuration file configured for the project: " +
      project_path);
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::CommServerHandler::DefineMqttEntity(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  auto res_payload = kn::IPayload::CastPayload<kn::CommPayload>(payload);
  auto comm_servers_yaml = ReadConfFile(this->base_path_, project_path);
  auto comm_servers_list = comm_servers_yaml["comm-servers"];
  auto comm_server_to_add = comm_servers_list["mqtt"];

  if (comm_server_to_add) {
    this->logger_->ErrorLog(
        "Cannot add MQTT CommServer entity, already exists for project: " +
        project_path);
    return kn::ErrorType::kAlreadyExists;
  }
  auto mqtt_file =
      fmt::format(kCommServerDirFormat, project_path, kCommServerMqttFile);
  auto mqtt_file_path =
      fmt::format(kBaseCommServerDirFormat, this->base_path_, mqtt_file);

  /* Add the MQTT config file */
  auto mqtt_yaml_node = YAML::Node();
  mqtt_yaml_node["description"] =
      "This is the configuration file for the MQTT entity";
  mqtt_yaml_node["path"] = mqtt_file;
  mqtt_yaml_node["suscriptions"] = YAML::Node();
  mqtt_yaml_node["publications"] = YAML::Node();

  auto mqtt_file_written = kn::WriteYaml(mqtt_yaml_node, mqtt_file_path);

  if (mqtt_file_written != kn::ErrorType::kSuccess) {
    this->logger_->FatalLog("Cannot create MQTT configuration file for: " +
                            project_path);
    return mqtt_file_written;
  }

  /* Add the MQTT entity definition */
  comm_server_to_add = YAML::Node();
  comm_server_to_add["file-path"] = mqtt_file;
  comm_server_to_add["active"] = true;
  comm_server_to_add["port"] = res_payload->get_port();
  comm_server_to_add["domain"] = res_payload->get_domain();

  comm_servers_list["mqtt"] = comm_server_to_add;
  comm_servers_yaml["comm-servers"] = comm_servers_list;

  auto write_result =
      WriteConfFile(this->base_path_, project_path, comm_servers_yaml);

  if (write_result != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(
        "Write failed when adding MQTT entity for project: " + project_path);
    return write_result;
  }

  this->logger_->InfoLog("MQTT added for project: " + project_path);
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::CommServerHandler::AddEndpointToEntity(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  auto res_payload = kn::IPayload::CastPayload<kn::CommPayload>(payload);
  auto protocol = res_payload->get_protocol();
  kn::ErrorType result;

  switch (protocol) {
    case kn::CommServerProtocol::kMqtt:
      result = AddMqttEndpoint(payload, project_path);
      break;

    default:
      this->logger_->ErrorLog("Protocol not found when adding new endpoint: " +
                              res_payload->get_path_to_send());
      result = kn::ErrorType::kNotFound;
      break;
  }

  return result;
}

kn::ErrorType resadmin::CommServerHandler::AddMqttEndpoint(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  YAML::Node endpoint_to_change;
  auto res_payload = kn::IPayload::CastPayload<kn::CommPayload>(payload);
  auto config_file = ReadConfFile(this->base_path_, project_path);

  auto config_mqtt_path =
      config_file["comm-servers"]["mqtt"]["file-path"].as<std::string>();
  auto conf_file_path =
      fmt::format(kBaseCommServerDirFormat, this->base_path_, config_mqtt_path);

  auto mqtt_yaml = kn::ReadYaml(conf_file_path);

  auto endpoint = res_payload->get_path_to_send();
  auto doee_name = res_payload->get_function_name();
  auto type = res_payload->get_function_type();

  switch (type) {
    case kn::EndpointType::kSub:
      endpoint_to_change = mqtt_yaml["subscriptions"][endpoint];
      if (endpoint_to_change) {
        this->logger_->ErrorLog("MQTT subscription topic already exists: " +
                                endpoint);
        return kn::ErrorType::kAlreadyExists;
      }
      endpoint_to_change = YAML::Node();
      endpoint_to_change["doee"] = doee_name;
      endpoint_to_change["topic"] = endpoint;
      mqtt_yaml["subscriptions"][endpoint] = endpoint_to_change;
      break;

    case kn::EndpointType::kPub:
      endpoint_to_change = mqtt_yaml["publications"][endpoint];
      if (endpoint_to_change) {
        this->logger_->ErrorLog("MQTT publication topic already exists: " +
                                endpoint);
        return kn::ErrorType::kAlreadyExists;
      }
      endpoint_to_change = YAML::Node();
      endpoint_to_change["topic"] = endpoint;
      mqtt_yaml["publications"][endpoint] = endpoint_to_change;
      break;
    default:
      this->logger_->ErrorLog("MQTT endpoint type not supported");
      return kn::ErrorType::kCannotCreate;
      break;
  }

  auto write_result = kn::WriteYaml(mqtt_yaml, conf_file_path);
  if (write_result != kn::ErrorType::kSuccess) {
    this->logger_->FatalLog(
        "Cannot update MQTT configuration file when adding endpoint: " +
        endpoint);
    return write_result;
  }
  this->logger_->InfoLog("MQTT endpoint added: " + endpoint);
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::CommServerHandler::RemoveMqttEndpoint(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  YAML::Node endpoint_to_change;
  auto res_payload = kn::IPayload::CastPayload<kn::CommPayload>(payload);
  auto config_file = ReadConfFile(this->base_path_, project_path);

  auto config_mqtt_path =
      config_file["comm-servers"]["mqtt"]["file-path"].as<std::string>();

  auto mqtt_conf_file =
      fmt::format(kCommServerDirFormat, project_path, kCommServerMqttFile);
  auto conf_file_path =
      fmt::format(kBaseCommServerDirFormat, this->base_path_, config_mqtt_path);

  auto mqtt_yaml = kn::ReadYaml(conf_file_path);

  auto endpoint = res_payload->get_path_to_send();
  auto doee_name = res_payload->get_function_name();
  auto type = res_payload->get_function_type();

  switch (type) {
    case kn::EndpointType::kSub:
      endpoint_to_change = mqtt_yaml["subscriptions"][endpoint];
      if (!endpoint_to_change) {
        this->logger_->ErrorLog("MQTT subscription topic not found: " +
                                endpoint);
        return kn::ErrorType::kNotFound;
      }
      mqtt_yaml["subscriptions"].remove(endpoint);
      break;

    case kn::EndpointType::kPub:
      endpoint_to_change = mqtt_yaml["publications"][endpoint];
      if (!endpoint_to_change) {
        this->logger_->ErrorLog("MQTT publication topic not found: " +
                                endpoint);
        return kn::ErrorType::kNotFound;
      }
      mqtt_yaml["publications"].remove(endpoint);
      break;
    default:
      this->logger_->ErrorLog("MQTT endpoint type not supported");
      return kn::ErrorType::kCannotCreate;
      break;
  }

  auto write_result = kn::WriteYaml(mqtt_yaml, conf_file_path);
  if (write_result != kn::ErrorType::kSuccess) {
    this->logger_->FatalLog(
        "Cannot update MQTT configuration file when adding endpoint: " +
        endpoint);
    return write_result;
  }
  this->logger_->InfoLog("MQTT endpoint removed: " + endpoint);
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::CommServerHandler::RemoveEndpointFromEntity(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  auto res_payload = kn::IPayload::CastPayload<kn::CommPayload>(payload);
  auto protocol = res_payload->get_protocol();
  kn::ErrorType result;

  switch (protocol) {
    case kn::CommServerProtocol::kMqtt:
      result = RemoveMqttEndpoint(payload, project_path);
      break;

    default:
      this->logger_->ErrorLog(
          "Protocol not found when removing new endpoint: " +
          res_payload->get_path_to_send());
      result = kn::ErrorType::kNotFound;
      break;
  }

  return result;
}

kn::ErrorType resadmin::CommServerHandler::RemoveCommServerEntity(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  kn::ErrorType result;

  auto res_payload = kn::IPayload::CastPayload<kn::CommPayload>(payload);
  auto protocol = res_payload->get_protocol();

  switch (protocol) {
    case kn::CommServerProtocol::kMqtt:
      result = RemoveMqttEntity(payload, project_path);
      break;

    default:
      result = kn::ErrorType::kNotFound;
      this->logger_->ErrorLog(
          "Cannot remove CommServer Entity, protocolo not found: " +
          std::to_string(static_cast<int>(protocol)));
      break;
  }

  return result;
}

kn::ErrorType resadmin::CommServerHandler::RemoveMqttEntity(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  auto res_payload = kn::IPayload::CastPayload<kn::CommPayload>(payload);
  auto comm_servers_yaml = ReadConfFile(this->base_path_, project_path);
  auto comm_servers_list = comm_servers_yaml["comm-servers"];
  auto comm_server_to_remove = comm_servers_list["mqtt"];

  if (!comm_server_to_remove) {
    this->logger_->ErrorLog(
        "Cannot remove MQTT CommServer entity, not found: " + project_path);
    return kn::ErrorType::kNotFound;
  }

  auto mqtt_file = comm_server_to_remove["file-path"].as<std::string>();
  auto mqtt_file_path =
      fmt::format(kBaseCommServerDirFormat, this->base_path_, mqtt_file);
  comm_server_to_remove["active"] = false;

  auto remove_file_result =
      this->storage_handler_->RemoveSingle(mqtt_file_path);

  if (remove_file_result != kn::ErrorType::kSuccess) {
    this->logger_->FatalLog(
        "Cannot remove MQTT configuration file for project: " + project_path);
    comm_servers_yaml["comm-servers"] = comm_servers_list;
  } else {
    auto remove_result = comm_servers_list.remove("mqtt");
    if (!remove_result) {
      this->logger_->ErrorLog(
          "Write failed when adding MQTT entity for project: " + project_path);
    } else {
      comm_servers_yaml["comm-servers"] = comm_servers_list;
    }
  }

  auto write_result =
      WriteConfFile(this->base_path_, project_path, comm_servers_yaml);

  if (write_result != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(
        "Write failed when removing MQTT entity for project: " + project_path);
    return write_result;
  }

  this->logger_->InfoLog("MQTT added for project: " + project_path);
  return kn::ErrorType::kSuccess;
}
