/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <ILogger.hpp>

#include "ICommServerHandler.hpp"

namespace isms {
namespace resadmin {

class CommServerHandler : public ICommServerHandler {
 public:
  /**
   * @brief Construct a new CommServerHandler object
   *
   */
  CommServerHandler() {
    this->logger_ = log::ILogger::Build(log::LogType::kResadminLog);
    this->logger_->Init("CommServerHandler");
  }

  /**
   * @brief Destroy the CommServerHandler object
   *
   */
  virtual ~CommServerHandler() {}
  /**
   * @brief Adds a CommServerEntity definition for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const string &
   * @return kn::ErrorType
   */
  kn::ErrorType AddCommServerEntity(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) override;

  /**
   * @brief Adds an endpoint to a CommServer entity, this endpoint is associated
   * with a Doee to be redirected
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const string &
   * @return kn::ErrorType
   */
  kn::ErrorType AddEndpointToEntity(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) override;

  /**
   * @brief Deletes an endpoint for a CommServerEntity
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const std::string &, the project path where the files
   * with the information are located
   * @return kn::ErrorType
   */
  kn::ErrorType RemoveEndpointFromEntity(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) override;

  /**
   * @brief Deletes a CommServerEntity definition for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const string &
   * @return kn::ErrorType
   */
  kn::ErrorType RemoveCommServerEntity(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) override;

  /**
   * @brief Set the storage handler object
   *
   * @param storage_handler, const std::shared_ptr<IStorageHandler> &
   */
  void set_storage_handler(
      const std::shared_ptr<IStorageHandler> &storage_handler) override;

  /**
   * @brief Set the base directory path
   *
   * @param path const std::string &
   */
  void set_base_directory(const std::string &path) override;

  /**
   * @brief Get the base directory path
   *
   * @return const std::string&
   */
  const std::string &get_base_directory() const noexcept override;

  /**
   * @brief Initializes the CommServer configuration file for an specific
   * project
   *
   * @param project_path const std::string &, project to use
   * @return kn::ErrorType
   */
  kn::ErrorType Init(const std::string &project_path) override;

  /**
   * @brief Get the storage handler object
   *
   * @return const std::shared_ptr<IStorageHandler>&
   */
  const std::shared_ptr<IStorageHandler> &get_storage_handler()
      const noexcept override;

 private:
  std::shared_ptr<IStorageHandler> storage_handler_;
  std::string base_path_;
  std::shared_ptr<log::ILogger> logger_;

  /**
   * @brief Defines a Comm Server Entity based on the MQTT network protocol
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path onst std::string &
   * @return kn::ErrorType
   */
  kn::ErrorType DefineMqttEntity(const std::shared_ptr<kn::IPayload> &payload,
                                 const std::string &project_path);

  /**
   * @brief Remove a Comm Server Entity based on the MQTT network protocol
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path onst std::string &
   * @return kn::ErrorType
   */
  kn::ErrorType RemoveMqttEntity(const std::shared_ptr<kn::IPayload> &payload,
                                 const std::string &project_path);

  /**
   * @brief Add a MQTT endpoint depending if it is a subscription or a
   * publication topic
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param conf_path const std::string &
   * @return kn::ErrorType
   */
  kn::ErrorType AddMqttEndpoint(const std::shared_ptr<kn::IPayload> &payload,
                                const std::string &conf_path);

  /**
   * @brief Remove a MQTT endpoint depending if it is a subscription or a
   * publication topic
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param conf_path const std::string &
   * @return kn::ErrorType
   */
  kn::ErrorType RemoveMqttEndpoint(const std::shared_ptr<kn::IPayload> &payload,
                                   const std::string &conf_path);
};

inline void CommServerHandler::set_storage_handler(
    const std::shared_ptr<IStorageHandler> &storage_handler) {
  this->storage_handler_ = storage_handler;
}

inline void CommServerHandler::set_base_directory(const std::string &path) {
  this->base_path_ = path;
}

inline const std::string &CommServerHandler::get_base_directory()
    const noexcept {
  return this->base_path_;
}

inline const std::shared_ptr<IStorageHandler>
    &CommServerHandler::get_storage_handler() const noexcept {
  return this->storage_handler_;
}

}  // namespace resadmin
}  // namespace isms
