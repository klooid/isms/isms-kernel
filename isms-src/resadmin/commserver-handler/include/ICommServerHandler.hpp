/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <memory>
#include <string>

/* Local includes */
#include <ErrorType.hpp>
#include <IPayload.hpp>
#include <IStorageHandler.hpp>

namespace isms {
namespace resadmin {

class ICommServerHandler {
 public:
  /**
   * @brief Construct a new ICommServerHandler object
   *
   */
  ICommServerHandler() {}

  /**
   * @brief Destroy the ICommServerHandler object
   *
   */
  virtual ~ICommServerHandler() {}

  /**
   * @brief Adds a CommServerEntity definition for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const string &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType AddCommServerEntity(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) = 0;

  /**
   * @brief Adds an endpoint to a CommServer entity, this endpoint is associated
   * with a Doee to be redirected
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const string &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType AddEndpointToEntity(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) = 0;

  /**
   * @brief Deletes an endpoint for a CommServerEntity
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const std::string &, the project path where the files
   * with the information are located
   * @return kn::ErrorType
   */
  virtual kn::ErrorType RemoveEndpointFromEntity(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) = 0;

  /**
   * @brief Deletes a CommServerEntity definition for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const string &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType RemoveCommServerEntity(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) = 0;

  /**
   * @brief Set the storage handler object
   *
   * @param storage_handler, const std::shared_ptr<IStorageHandler> &
   */
  virtual void set_storage_handler(
      const std::shared_ptr<IStorageHandler> &storage_handler) = 0;

  /**
   * @brief Set the base directory path
   *
   * @param path const std::string &
   */
  virtual void set_base_directory(const std::string &path) = 0;

  /**
   * @brief Get the base directory path
   *
   * @return const std::string&
   */
  virtual const std::string &get_base_directory() const noexcept = 0;

  /**
   * @brief Get the storage handler object
   *
   * @return const std::shared_ptr<IStorageHandler>&
   */
  virtual const std::shared_ptr<IStorageHandler> &get_storage_handler()
      const noexcept = 0;

  /**
   * @brief Initializes the CommServer configuration file for an specific
   * project
   *
   * @param project_path const std::string &, project to use
   * @return kn::ErrorType
   */
  virtual kn::ErrorType Init(const std::string &project_path) = 0;

  /**
   * @brief Creates an ICommServerHandler object
   *
   * @return std::shared_ptr<ICommServerHandler>
   */
  static std::shared_ptr<ICommServerHandler> Build();
};

}  // namespace resadmin
}  // namespace isms
