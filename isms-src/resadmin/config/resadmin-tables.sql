/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
CREATE TABLE IF NOT EXISTS user (
    Id INT AUTO_INCREMENT,
    User VARCHAR(255) NOT NULL,
    Status ENUM('0', '1') NOT NULL DEFAULT '0',
    Password VARCHAR(64) NOT NULL,
    Uid VARCHAR(11) NOT NULL,
    Gid VARCHAR(11) NOT NULL,
    Dir VARCHAR(255) NOT NULL,
    UserType VARCHAR(10) NOT NULL DEFAULT 'user',
    ULBandwidth SMALLINT NOT NULL DEFAULT '0',
    DLBandwidth SMALLINT NOT NULL DEFAULT '0',
    Comment tinytext NOT NULL DEFAULT 'New User',
    IPAccess VARCHAR(32) NOT NULL DEFAULT '*',
    QuotaSize INT UNSIGNED NOT NULL DEFAULT '0',
    QuotaFiles INT UNSIGNED NOT NULL DEFAULT '0',
    PRIMARY KEY (Id),
    UNIQUE KEY User (User)
);
CREATE TABLE IF NOT EXISTS project (
    Id INT AUTO_INCREMENT,
    IdOwner INT NOT NULL,
    ProjectName VARCHAR(255) NOT NULL,
    Dir VARCHAR(255) NOT NULL,
    PRIMARY KEY (Id),
    UNIQUE KEY Project (ProjectName),
    FOREIGN KEY (IdOwner) REFERENCES user (Id)
);