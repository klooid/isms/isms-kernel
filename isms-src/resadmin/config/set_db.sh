#
# IoT Services Management System - 2022
# See LICENSE for more information about licensing
#
# Author: Jose Ortega <ortega.josant@gmail.com>
#         Luis G. Leon Vega <lleon95@gmail.com>
#
# Sponsor: Klooid Innovations 2022
#

# Run and set the databases and their tables

mariadb < "resadmin.sql"
mariadb resadmin < "resadmin-tables.sql"
mariadb resadmin_test < "resadmin-tables.sql"
