/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <memory>
#include <string>

/* Local includes */
#include <ErrorType.hpp>
#include <IPayload.hpp>
#include <IStorageHandler.hpp>

namespace isms {
namespace resadmin {

class IProjectHandler {
 public:
  /**
   * @brief Construct a new IProjectHandler object
   *
   */
  IProjectHandler() {}

  /**
   * @brief Destroy the IProjectHandler object
   *
   */
  virtual ~IProjectHandler() {}

  /**
   * @brief Creates a User in a specific directory for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType CreateProject(
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * @brief Deletes a project tree in a specific directory
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType DeleteProject(
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * @brief Adds a new User to a specified project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType AddUserToProject(
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * @brief Set the storage handler object
   *
   * @param storage_handler, const std::shared_ptr<IStorageHandler> &
   */
  virtual void set_storage_handler(
      const std::shared_ptr<IStorageHandler> &storage_handler) = 0;

  /**
   * @brief Set the base directory path
   *
   * @param path const std::string &
   */
  virtual void set_base_directory(const std::string &path) = 0;

  /**
   * @brief Get the base directory path
   *
   * @return const std::string&
   */
  virtual const std::string &get_base_directory() const noexcept = 0;

  /**
   * @brief Get the storage handler object
   *
   * @return const std::shared_ptr<IStorageHandler>&
   */
  virtual const std::shared_ptr<IStorageHandler> &get_storage_handler()
      const noexcept = 0;

  /**
   * @brief Initialize the project handler and its dependencies
   *
   * @return kn::ErrorType
   */
  virtual kn::ErrorType Init() = 0;
  /**
   * @brief Creates an IProjectHandler object
   *
   * @return std::shared_ptr<IProjectHandler>
   */
  static std::shared_ptr<IProjectHandler> Build();
};

}  // namespace resadmin
}  // namespace isms
