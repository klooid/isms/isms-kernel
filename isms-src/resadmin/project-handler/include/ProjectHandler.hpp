/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once
#include <memory>
#include <string>

/* Local includes */
#include <ErrorType.hpp>
#include <IPayload.hpp>

#include "IProjectHandler.hpp"

namespace isms {
namespace resadmin {

class ProjectHandler : public IProjectHandler {
 public:
  /**
   * @brief Construct a new ProjectHandler object
   *
   */
  ProjectHandler() {}

  /**
   * @brief Destroy the ProjectHandler object
   *
   */
  virtual ~ProjectHandler() {}

  /**
   * @brief Create a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  kn::ErrorType CreateProject(
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * @brief Deletes a project tree in a specific directory
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  kn::ErrorType DeleteProject(
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * @brief Adds a new User to a specified project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  kn::ErrorType AddUserToProject(
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * @brief Set the storage handler object
   *
   * @param storage_handler, const std::shared_ptr<IStorageHandler> &
   */
  void set_storage_handler(
      const std::shared_ptr<IStorageHandler> &storage_handler) override;

  /**
   * @brief Get the storage handler object
   *
   * @return const std::shared_ptr<IStorageHandler>&
   */
  const std::shared_ptr<IStorageHandler> &get_storage_handler()
      const noexcept override;

  /**
   * @brief Set the base directory path
   *
   * @param path const std::string &
   */
  void set_base_directory(const std::string &path) override;

  /**
   * @brief Get the base directory path
   *
   * @return const std::string&
   */
  const std::string &get_base_directory() const noexcept override;

  /**
   * @brief Initialize the project handler and its dependencies
   *
   * @return kn::ErrorType
   */
  kn::ErrorType Init() override;

 private:
  std::shared_ptr<IStorageHandler> storage_handler_;
  std::string base_directory_;
};

inline const std::shared_ptr<IStorageHandler>
    &ProjectHandler::get_storage_handler() const noexcept {
  return this->storage_handler_;
}

inline void ProjectHandler::set_storage_handler(
    const std::shared_ptr<IStorageHandler> &storage_handler) {
  this->storage_handler_ = storage_handler;
}

inline void ProjectHandler::set_base_directory(const std::string &path) {
  this->base_directory_ = path;
}

inline const std::string &ProjectHandler::get_base_directory() const noexcept {
  return this->base_directory_;
}

}  // namespace resadmin
}  // namespace isms
