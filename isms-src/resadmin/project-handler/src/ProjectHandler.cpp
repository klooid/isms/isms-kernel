/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "ProjectHandler.hpp"

#include <ResPayload.hpp>
#include <YamlHandler.hpp>

using namespace isms;

const std::string kProjectsDir{"projects"};
const std::string kProjectsConfFile{"projects-conf.yml"};
static std::string base_file_dir;
static std::string base_projects_dir;

kn::ErrorType resadmin::ProjectHandler::CreateProject(
    const std::shared_ptr<kn::IPayload> &payload) {
  YAML::Node new_project;

  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);

  auto project_name = res_payload->get_project_name();
  auto user_name = res_payload->get_user();
  auto project_dir = kProjectsDir + "/" + user_name + "/" + project_name;

  auto project_result =
      this->storage_handler_->CreateProjectDirectories(project_dir);

  if (project_result.get_error_code() != kn::ErrorType::kSuccess)
    return project_result.get_error_code();

  auto conf_yml = kn::ReadYaml(base_file_dir);

  if (conf_yml.IsNull()) {
    this->storage_handler_->RemoveAll(project_result.get_result());
    return kn::ErrorType::kCannotCreate;
  }

  /* Adding new project information */
  new_project["project-name"] = project_name;
  new_project["owner"] = user_name;
  new_project["path"] = project_result.get_result();
  new_project["allowed-users"] = YAML::Load("[]");

  conf_yml["projects"].push_back(new_project);

  auto write_yml = kn::WriteYaml(conf_yml, base_file_dir);
  if (write_yml != kn::ErrorType::kSuccess) {
    this->storage_handler_->RemoveAll(project_result.get_result());
    return write_yml;
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::ProjectHandler::DeleteProject(
    const std::shared_ptr<kn::IPayload> &payload) {
  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);

  auto project_name = res_payload->get_project_name();
  auto user_name = res_payload->get_user();
  auto project_dir = kProjectsDir + "/" + user_name + "/" + project_name;

  auto rm_result =
      this->storage_handler_->RemoveProjectDirectories(project_dir);

  /* Trying to delete the content */
  if (rm_result.get_error_code() != kn::ErrorType::kSuccess) {
    return kn::ErrorType::kCannotDelete;
  }

  auto conf_yml = kn::ReadYaml(base_file_dir);
  auto projects = conf_yml["projects"];

  for (std::size_t i = 0; i < projects.size(); i++) {
    auto temp_node = projects[i].as<YAML::Node>();
    /* Check for the project */
    if (temp_node["project-name"].as<std::string>() == project_name) {
      auto node_delete = projects.remove(i);
      if (!node_delete) return kn::ErrorType::kCannotClose;
      conf_yml["projects"] = projects;

      /* Update content of the file */
      auto write_result = kn::WriteYaml(conf_yml, base_file_dir);
      if (write_result != kn::ErrorType::kSuccess) return write_result;
      return kn::ErrorType::kSuccess;
    }
  }
}

kn::ErrorType resadmin::ProjectHandler::AddUserToProject(
    const std::shared_ptr<kn::IPayload> &payload) {
  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);

  auto project_name = res_payload->get_project_name();
  auto new_user = res_payload->get_new_user();

  auto conf_yml = kn::ReadYaml(base_file_dir);
  auto projects = conf_yml["projects"];
  int project_index = -1;
  for (std::size_t i = 0; i < projects.size(); i++) {
    auto temp_node = projects[i].as<YAML::Node>();
    /* Check for the project */
    if (temp_node["project-name"].as<std::string>() == project_name) {
      project_index = i;
      auto allowed_users = conf_yml["projects"][project_index]["allowed-users"];
      /* Check for the user */
      for (std::size_t j = 0; i < allowed_users.size(); j++) {
        if (allowed_users[j].as<std::string>() == new_user) {
          return kn::ErrorType::kAlreadyExists;
        }
      }
    }
  }

  if (-1 >= project_index) return kn::ErrorType::kNotFound;

  /* Add the new user */
  conf_yml["projects"][project_index]["allowed-users"].push_back(new_user);

  /* Update content of the file */
  auto write_result = kn::WriteYaml(conf_yml, base_file_dir);
  if (write_result != kn::ErrorType::kSuccess) return write_result;
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::ProjectHandler::Init() {
  if (nullptr == this->storage_handler_)
    this->storage_handler_ = resadmin::IStorageHandler::Build();

  this->storage_handler_->set_base_directory(this->base_directory_);
  auto file_dir = kProjectsDir + "/" + kProjectsConfFile;
  base_projects_dir = this->base_directory_ + "/" + kProjectsDir;
  base_file_dir = this->base_directory_ + "/" + file_dir;

  /* Create projects dir */
  auto folder_result = this->storage_handler_->CreateFolder(kProjectsDir);
  if (folder_result != kn::ErrorType::kSuccess &&
      folder_result != kn::ErrorType::kAlreadyExists)
    return folder_result;

  /* Create projects configuration file */
  auto file_result = this->storage_handler_->AddFile(file_dir);
  if (file_result != kn::ErrorType::kSuccess &&
      file_result != kn::ErrorType::kAlreadyExists)
    return file_result;
  else if (file_result == kn::ErrorType::kSuccess) {
    /* Fulfill the project-conf-yml */
    YAML::Node yml_file;

    yml_file["description"] =
        "This file contains the projects and its information";
    yml_file["projects"] = YAML::Node();

    auto write_result = kn::WriteYaml(yml_file, base_file_dir);
    if (write_result != kn::ErrorType::kSuccess) return write_result;
  }

  return kn::ErrorType::kSuccess;
}
