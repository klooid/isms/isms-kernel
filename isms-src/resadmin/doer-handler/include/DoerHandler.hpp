/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <ILogger.hpp>

#include "IDoerHandler.hpp"

namespace isms {
namespace resadmin {

class DoerHandler : public IDoerHandler {
 public:
  /**
   * @brief Construct a new DoerHandler object
   *
   */
  DoerHandler() {
    this->logger_ = log::ILogger::Build(log::LogType::kResadminLog);
    this->logger_->Init("DoerHandler");
  }

  /**
   * @brief Destroy the DoerHandler object
   *
   */
  virtual ~DoerHandler() {}

  /**
   * @brief Creates a Doee instance for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const std::string & path to the project
   * @return kn::ErrorType
   */
  kn::ErrorType AddDoee(const std::shared_ptr<kn::IPayload> &payload,
                        const std::string &project_path) override;

  /**
   * @brief Provides a Doee volume path for an specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const std::string & path to the project
   * @return kn::ErrorType
   */
  kn::ErrorType RetrieveDoeePath(const std::shared_ptr<kn::IPayload> &payload,
                                 const std::string &project_path) override;

  /**
   * @brief Deletes a Doee instance from a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const std::string & path to the project
   * @return kn::ErrorType
   */
  kn::ErrorType DeleteDoee(const std::shared_ptr<kn::IPayload> &payload,
                           const std::string &project_path) override;

  /**
   * @brief Configures the Doees definition structure file in a project
   * directory
   *
   * @param project_path const std::string &, the project path to add the Doees
   * definition structure
   * @return kn::ErrorType
   */
  kn::ErrorType ConfigureDoeeStructure(
      const std::string &project_path) override;
  /**
   * @brief Set the storage handler object
   *
   * @param storage_handler, const std::shared_ptr<IStorageHandler> &
   */
  void set_storage_handler(
      const std::shared_ptr<IStorageHandler> &storage_handler) override;

  /**
   * @brief Set the base directory path
   *
   * @param path const std::string &
   */
  void set_base_directory(const std::string &path) override;

  /**
   * @brief Get the base directory path
   *
   * @return const std::string&
   */
  const std::string &get_base_directory() const noexcept override;

  /**
   * @brief Get the storage handler object
   *
   * @return const std::shared_ptr<IStorageHandler>&
   */
  const std::shared_ptr<IStorageHandler> &get_storage_handler()
      const noexcept override;

 private:
  std::shared_ptr<IStorageHandler> storage_handler_;
  std::string base_path_;
  std::shared_ptr<log::ILogger> logger_;
};

inline void DoerHandler::set_storage_handler(
    const std::shared_ptr<IStorageHandler> &storage_handler) {
  this->storage_handler_ = storage_handler;
}

inline void DoerHandler::set_base_directory(const std::string &path) {
  this->base_path_ = path;
}

inline const std::string &DoerHandler::get_base_directory() const noexcept {
  return this->base_path_;
}

inline const std::shared_ptr<IStorageHandler>
    &DoerHandler::get_storage_handler() const noexcept {
  return this->storage_handler_;
}

}  // namespace resadmin
}  // namespace isms
