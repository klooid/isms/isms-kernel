/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <memory>
#include <string>

/* Local includes */
#include <ErrorType.hpp>
#include <IPayload.hpp>
#include <IStorageHandler.hpp>

namespace isms {
namespace resadmin {

class IDoerHandler {
 public:
  /**
   * @brief Construct a new IDoerHandler object
   *
   */
  IDoerHandler() {}

  /**
   * @brief Destroy the IDoerHandler object
   *
   */
  virtual ~IDoerHandler() {}

  /**
   * @brief Creates a Doee instance for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const std::string & path to the project
   * @return kn::ErrorType
   */
  virtual kn::ErrorType AddDoee(const std::shared_ptr<kn::IPayload> &payload,
                                const std::string &project_path) = 0;

  /**
   * @brief Provides a Doee volume path for an specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const std::string & path to the project
   * @return kn::ErrorType
   */
  virtual kn::ErrorType RetrieveDoeePath(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &project_path) = 0;

  /**
   * @brief Deletes a Doee instance from a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param project_path const std::string & path to the project
   * @return kn::ErrorType
   */
  virtual kn::ErrorType DeleteDoee(const std::shared_ptr<kn::IPayload> &payload,
                                   const std::string &project_path) = 0;
  /**
   * @brief Configures the Doees definition structure file in a project
   * directory
   *
   * @param project_path const std::string &, the project path to add the Doees
   * definition structure
   * @return kn::ErrorType
   */
  virtual kn::ErrorType ConfigureDoeeStructure(
      const std::string &project_path) = 0;

  /**
   * @brief Set the storage handler object
   *
   * @param storage_handler, const std::shared_ptr<IStorageHandler> &
   */
  virtual void set_storage_handler(
      const std::shared_ptr<IStorageHandler> &storage_handler) = 0;

  /**
   * @brief Set the base directory path
   *
   * @param path const std::string &
   */
  virtual void set_base_directory(const std::string &path) = 0;

  /**
   * @brief Get the base directory path
   *
   * @return const std::string&
   */
  virtual const std::string &get_base_directory() const noexcept = 0;

  /**
   * @brief Get the storage handler object
   *
   * @return const std::shared_ptr<IStorageHandler>&
   */
  virtual const std::shared_ptr<IStorageHandler> &get_storage_handler()
      const noexcept = 0;

  /**
   * @brief Creates an IDoerHandler object
   *
   * @return std::shared_ptr<IDoerHandler>
   */
  static std::shared_ptr<IDoerHandler> Build();
};

}  // namespace resadmin
}  // namespace isms
