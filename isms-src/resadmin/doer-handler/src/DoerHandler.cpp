/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "DoerHandler.hpp"

#include <fmt/core.h>

#include <DoerPayload.hpp>
#include <ResPayload.hpp>
#include <YamlHandler.hpp>

using namespace isms;

const std::string kDoeePathFormat{"{}/doees/{}"};
const std::string kBaseDoeePathFormat{"{}/{}"};
const std::string kDoeeConfFile{"doees_conf.yml"};

kn::ErrorType resadmin::DoerHandler::AddDoee(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  auto res_payload = kn::IPayload::CastPayload<kn::DoerPayload>(payload);
  auto doee_name = res_payload->get_doee_name();

  auto doee_file = fmt::format(kDoeePathFormat, project_path, kDoeeConfFile);
  auto doees_file_path =
      fmt::format(kBaseDoeePathFormat, this->base_path_, doee_file);

  auto doees_yaml = kn::ReadYaml(doees_file_path);

  auto doees_list = doees_yaml["doees"];
  auto doee_to_add = doees_list[doee_name];

  if (doee_to_add) {
    this->logger_->ErrorLog("Doee definition already exists: " + doee_name);
    return kn::ErrorType::kAlreadyExists;
  }

  auto doee_volume = fmt::format(kDoeePathFormat, project_path, doee_name);
  auto doee_folder_path =
      fmt::format(kBaseDoeePathFormat, this->base_path_, doee_volume);
  auto result = this->storage_handler_->CreateFolder(doee_folder_path);

  if (result != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog("Doee folder creation error for: " + doee_name);
    return result;
  }

  /* Set the Doee information */
  doee_to_add = YAML::Node();
  doee_to_add["name"] = doee_name;
  doee_to_add["language"] = res_payload->get_doee_language();
  doee_to_add["src"] = res_payload->get_doee_src();
  doee_to_add["path"] = doee_volume;

  doees_list[doee_name] = doee_to_add;
  doees_yaml["doees"] = doees_list;

  /* Writes the YAML updated */
  auto write_result = kn::WriteYaml(doees_yaml, doees_file_path);
  if (write_result != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(
        "Cannot write the doee information in the config file: " + doee_name);
    this->storage_handler_->RemoveAll(doee_folder_path);
    return write_result;
  }

  res_payload->set_volume_path(doee_volume);
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::DoerHandler::RetrieveDoeePath(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  auto res_payload = kn::IPayload::CastPayload<kn::DoerPayload>(payload);

  auto doee_name = res_payload->get_doee_name();

  auto doee_file = fmt::format(kDoeePathFormat, project_path, kDoeeConfFile);
  auto doees_file_path =
      fmt::format(kBaseDoeePathFormat, this->base_path_, doee_file);
  auto doees_yaml = kn::ReadYaml(doees_file_path);

  auto doees_list = doees_yaml["doees"];
  auto doee_to_read = doees_list[doee_name];

  if (!doee_to_read) {
    this->logger_->TraceLog("Failed when retrieve, Doee not found: " +
                            doee_name);
    return kn::ErrorType::kNotFound;
  }

  auto doee_path = doee_to_read["path"].as<std::string>();
  auto doee_lang = doee_to_read["language"].as<std::string>();

  res_payload->set_volume_path(doee_path);
  res_payload->set_doee_language(doee_lang);
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::DoerHandler::DeleteDoee(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &project_path) {
  auto res_payload = kn::IPayload::CastPayload<kn::DoerPayload>(payload);

  auto doee_name = res_payload->get_doee_name();

  auto doee_file = fmt::format(kDoeePathFormat, project_path, kDoeeConfFile);
  auto doees_file_path =
      fmt::format(kBaseDoeePathFormat, this->base_path_, doee_file);
  auto doees_yaml = kn::ReadYaml(doees_file_path);

  auto doees_list = doees_yaml["doees"];
  auto doee_to_delete = doees_list[doee_name];

  if (!doee_to_delete) {
    this->logger_->ErrorLog("Cannot delete doee not found: " + doee_name);
    return kn::ErrorType::kNotFound;
  }
  auto doee_volume = doee_to_delete["path"].as<std::string>();
  auto doee_path =
      fmt::format(kBaseDoeePathFormat, this->base_path_, doee_volume);

  auto deletion_res = this->storage_handler_->RemoveAll(doee_path);

  if (deletion_res != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog("Cannot delete folder for doee: " + doee_name);
    return deletion_res;
  }

  auto rmv_doee = doees_list.remove(doee_name);
  if (!rmv_doee) {
    this->logger_->ErrorLog("Cannot delete doee definition of: " + doee_name);
    return kn::ErrorType::kCannotDelete;
  }
  doees_yaml["doees"] = doees_list;

  /* Writes the YAML updated */
  auto write_result = kn::WriteYaml(doees_yaml, doees_file_path);
  if (write_result != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog("Cannot update doees definitions after delete: " +
                            doee_name);
    return write_result;
  }

  res_payload->set_volume_path(doee_path);
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::DoerHandler::ConfigureDoeeStructure(
    const std::string &project_path) {
  auto doee_file = fmt::format(kDoeePathFormat, project_path, kDoeeConfFile);
  auto doees_file_path =
      fmt::format(kBaseDoeePathFormat, this->base_path_, doee_file);
  auto doees_yaml = kn::ReadYaml(doees_file_path);

  if (!doees_yaml.IsNull()) {
    this->logger_->ErrorLog(
        "Cannot configure the doee configuration file: it is already there");
    return kn::ErrorType::kAlreadyExists;
  }

  doees_yaml["description"] =
      "This file defines the doees stored for this project";
  doees_yaml["doees"] = YAML::Node();

  auto write_result = kn::WriteYaml(doees_yaml, doees_file_path);
  if (write_result != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(
        "Cannot update the doee configuration file while configuring");
    return write_result;
  }

  return kn::ErrorType::kSuccess;
}
