/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "IDoerHandler.hpp"

#include "DoerHandler.hpp"

using namespace isms::resadmin;

std::shared_ptr<IDoerHandler> IDoerHandler::Build() {
  return std::make_shared<DoerHandler>();
}
