/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <ILogger.hpp>

#include "IUsersHandler.hpp"

namespace isms {
namespace resadmin {

class UsersHandler : public IUsersHandler {
 public:
  /**
   * @brief Construct a new UsersHandler object
   *
   */
  UsersHandler() {
    this->logger_ = log::ILogger::Build(log::LogType::kResadminLog);
    logger_->Init("UserHandler");
  }

  /**
   * @brief Destroy the UsersHandler object
   *
   */
  virtual ~UsersHandler() {}

  /**
   * @brief Create a User in a specific directory for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param path std::string &, path where the user has permissions
   * @param its_admin const bool, indicates if the user its an owner or a user
   * @return kn::ErrorType
   */
  kn::ErrorType CreateUser(const std::shared_ptr<kn::IPayload> &payload,
                           const std::string &path,
                           const bool its_admin) override;

  /**
   * @brief Delete a User in a specific directory for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  kn::ErrorType DeleteUser(
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * @brief Validates the user information
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  kn::ErrorType ValidateUser(
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * @brief Creates the a user group
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param group_path const std::string &, path to the group folder
   * @return kn::ErrorType
   */
  kn::ErrorType CreateUserGroup(const std::shared_ptr<kn::IPayload> &payload,
                                const std::string &group_path) override;

  /**
   * @brief Deletes the User and the Group from the system by its name
   *
   * @param payload, const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  kn::ErrorType DeleteUserGroup(
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * @brief Disables an active user by setting its status to inactive
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  kn::ErrorType DisableUser(
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * @brief Set the db access object
   *
   * @param db_access, const std::shared_ptr<db::IDbAccess> &
   */
  void set_db_access(const std::shared_ptr<db::IDbAccess> &db_access) override;

  /**
   * @brief Get the db access object
   *
   * @return const std::shared_ptr<db::IDbAccess>&
   */
  const std::shared_ptr<db::IDbAccess> &get_db_access() const noexcept override;

 private:
  std::shared_ptr<db::IDbAccess> db_access_;
  std::shared_ptr<log::ILogger> logger_;

  /**
   * @brief Checks if a group exists
   *
   * @param group_name the group name to check
   * @return true
   * @return false
   */
  bool CheckGroupExists(const std::string &group_name);
};

inline void UsersHandler::set_db_access(
    const std::shared_ptr<db::IDbAccess> &db_access) {
  this->db_access_ = db_access;
}

inline const std::shared_ptr<db::IDbAccess> &UsersHandler::get_db_access()
    const noexcept {
  return this->db_access_;
}

}  // namespace resadmin
}  // namespace isms
