/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <memory>
#include <string>

/* Local includes */
#include <ErrorType.hpp>
#include <IDbAccess.hpp>
#include <IPayload.hpp>

namespace isms {
namespace resadmin {

class IUsersHandler {
 public:
  /**
   * @brief Construct a new IUsersHandler object
   *
   */
  IUsersHandler() {}

  /**
   * @brief Destroy the IUsersHandler object
   *
   */
  virtual ~IUsersHandler() {}

  /**
   * @brief Create a User in a specific directory for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param path std::string &, path where the user has permissions
   * @param its_admin const bool, indicates if the user its an owner or a user
   * @return kn::ErrorType
   */
  virtual kn::ErrorType CreateUser(const std::shared_ptr<kn::IPayload> &payload,
                                   const std::string &path,
                                   const bool its_admin) = 0;

  /**
   * @brief Delete a User in a specific directory for a specific project
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType DeleteUser(
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * @brief Validates the user information
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType ValidateUser(
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * @brief Disables an active user by setting its status to inactive
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType DisableUser(
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * @brief Creates the a user group
   *
   * @param payload const std::shared_ptr<kn::IPayload> &
   * @param group_path const std::string &, path to the group folder
   * @return kn::ErrorType
   */
  virtual kn::ErrorType CreateUserGroup(
      const std::shared_ptr<kn::IPayload> &payload,
      const std::string &group_path) = 0;

  /**
   * @brief Deletes the User and the Group from the system by its name
   *
   * @param payload, const std::shared_ptr<kn::IPayload> &
   * @return kn::ErrorType
   */
  virtual kn::ErrorType DeleteUserGroup(
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * @brief Set the db access object
   *
   * @param db_access, const std::shared_ptr<db::IDbAccess> &
   */
  virtual void set_db_access(
      const std::shared_ptr<db::IDbAccess> &db_access) = 0;

  /**
   * @brief Get the db access object
   *
   * @return const std::shared_ptr<db::IDbAccess>&
   */
  virtual const std::shared_ptr<db::IDbAccess> &get_db_access()
      const noexcept = 0;

  /**
   * @brief Creates an IUsersHandler object
   *
   * @return std::shared_ptr<IUsersHandler>
   */
  static std::shared_ptr<IUsersHandler> Build();
};

}  // namespace resadmin
}  // namespace isms
