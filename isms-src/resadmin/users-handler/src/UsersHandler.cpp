/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "UsersHandler.hpp"

#include <fmt/core.h>
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>

#include <ResPayload.hpp>
#include <execommand.hpp>

using namespace isms;

/*-- Constants --*/

const int kCommonStorageSizeMB{500};
const std::string kStatusActive{'1'};
const std::string kStatusInactive{'0'};
const std::string kUserTable{"user"};
const std::string kUserTypeAdmin{"admin"};
const std::string kUserTypeUser{"user"};

/*-- Commands system --*/

const std::string kAllowPermissionCmd{"chown -R {}:{} {}"};
const std::string kUserSystemAddCmd{"useradd {} -U"};
const std::string kUserSystemDeleteCmd{"deluser {}"};
const std::string kGetIdCmd{"id {} {}"};

/*-- Commands PureFTd --*/

const std::string kSingleCondition{"{}.User = '{}'"};
const std::string kUserAddColumns{
    "User, Password, Uid, Gid, Dir, QuotaSize, Status, UserType"};
const std::string kUserAddValues{
    "'{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}'"};
const std::string kValidateUserCondition{
    "{}.User == '{}' AND {}.Password == '{}'"};

/* ---- Functions definition ---- */

kn::ErrorType resadmin::UsersHandler::CreateUser(
    const std::shared_ptr<kn::IPayload> &payload, const std::string &path,
    const bool its_admin) {
  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);
  auto user_type = its_admin ? kUserTypeAdmin : kUserTypeUser;

  auto sysgroup_user_name = res_payload->get_group_name();

  auto sysuser_info = getpwnam(sysgroup_user_name.c_str());

  /* Add the User */
  auto add_user_info = fmt::format(
      kUserAddValues, res_payload->get_new_user(), res_payload->get_password(),
      sysuser_info->pw_uid, sysuser_info->pw_gid, path, kCommonStorageSizeMB,
      kStatusActive, user_type);

  auto insert_result =
      this->db_access_->Insert(kUserTable, kUserAddColumns, add_user_info);

  if (insert_result.get_error_type() != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(insert_result.get_error_message());
    return insert_result.get_error_type();
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::UsersHandler::DisableUser(
    const std::shared_ptr<kn::IPayload> &payload) {
  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);
  auto user = res_payload->get_user();

  auto condition = fmt::format(kSingleCondition, kUserTable, user);
  auto update_value = fmt::format("Status = '{}'", kStatusInactive);
  /* Delete the user */
  auto delete_result =
      this->db_access_->Update(kUserTable, update_value, condition);
  if (delete_result.get_error_type() != kn::ErrorType::kSuccess) {
    return delete_result.get_error_type();
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::UsersHandler::DeleteUser(
    const std::shared_ptr<kn::IPayload> &payload) {
  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);
  auto user = res_payload->get_user();

  auto condition = fmt::format(kSingleCondition, kUserTable, user);

  /* Delete the user */
  auto delete_result = this->db_access_->Delete(kUserTable, condition);
  if (delete_result.get_error_type() != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(delete_result.get_error_message());
    return delete_result.get_error_type();
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::UsersHandler::ValidateUser(
    const std::shared_ptr<kn::IPayload> &payload) {
  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);

  auto user = res_payload->get_user();
  auto password = res_payload->get_password();

  /* Sets the select condition to check the user */
  auto select_condition = fmt::format(kValidateUserCondition, kUserTable, user,
                                      kUserTable, password);

  auto select_result =
      this->db_access_->Select(kUserTable, "User", select_condition);

  if (select_result.get_error_type() != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(select_result.get_error_message());
    return select_result.get_error_type();
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::UsersHandler::CreateUserGroup(
    const std::shared_ptr<kn::IPayload> &payload,
    const std::string &group_path) {
  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);
  auto group_user_name = res_payload->get_group_name();

  if (CheckGroupExists(group_user_name)) return kn::ErrorType::kAlreadyExists;

  /* Create the user and the group */
  auto command = fmt::format(kUserSystemAddCmd, group_user_name);
  if (command.size() == 0) {
    this->logger_->TraceLog("User System Add Command is empty");
    return kn::ErrorType::kCannotCreate;
  }

  auto command_result = cmd::Exec(command);
  if (command_result.get_error_code() != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(command_result.get_error_message());
    return command_result.get_error_code();
  }

  /* Assign permissions to the directory */
  auto sysuser_info = getpwnam(group_user_name.c_str());

  auto perm_status =
      chown(group_path.c_str(), sysuser_info->pw_uid, sysuser_info->pw_gid);

  if (perm_status != 0) {
    auto error_message =
        fmt::format("Can not assign permission to {} for group/user: {}",
                    group_path, group_user_name);
    this->logger_->ErrorLog(error_message);
    this->DeleteUserGroup(payload);
    return kn::ErrorType::kCannotBind;
  }

  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::UsersHandler::DeleteUserGroup(
    const std::shared_ptr<kn::IPayload> &payload) {
  auto res_payload = kn::IPayload::CastPayload<kn::ResPayload>(payload);
  auto group_user_name = res_payload->get_group_name();

  if (!CheckGroupExists(group_user_name)) {
    this->logger_->InfoLog("Group/User name not found: " + group_user_name);
    return kn::ErrorType::kNotFound;
  }

  /* Delete the user and the group */
  auto command = fmt::format(kUserSystemDeleteCmd, group_user_name);
  if (command.size() == 0) {
    return kn::ErrorType::kCannotCreate;
  }

  auto command_result = cmd::Exec(command);
  if (command_result.get_error_code() != kn::ErrorType::kSuccess) {
    this->logger_->ErrorLog(command_result.get_error_message());
    return command_result.get_error_code();
  }

  return kn::ErrorType::kSuccess;
}

bool resadmin::UsersHandler::CheckGroupExists(const std::string &group_name) {
  auto command_result = getgrnam(group_name.c_str());

  if (command_result) {
    return true;
  }

  return false;
}
