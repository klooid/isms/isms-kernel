/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <memory>
#include <string>

/* Internal */
#include <ErrorType.hpp>
#include <IPayload.hpp>

namespace isms {
namespace resadmin {

class IServerHandler {
 public:
  /**
   * @brief Construct a new IServerHandler object
   *
   */
  IServerHandler() {}

  /**
   * @brief Destroy the IServerHandler object
   *
   */
  virtual ~IServerHandler() {}

  /**
   * @brief Starts the Resadmin server
   *
   * @return kn::ErrorType
   */
  virtual kn::ErrorType StartServer() = 0;

  /**
   * @brief Stops the Resadmin server
   *
   * @return kn::ErrorType
   */
  virtual kn::ErrorType StopServer() = 0;

  /**
   * @brief Set the configuration file path object
   *
   * @param path const std::string &
   */
  virtual void set_configuration_file_path(const std::string &path) = 0;

  /**
   * @brief Get the configuration file path object
   *
   * @return const std::string&
   */
  virtual const std::string &get_configuration_file_path() const noexcept = 0;

  /**
   * @brief Set the users file path object
   *
   * @param path const std::string &
   */
  virtual void set_users_file_path(const std::string &path) = 0;

  /**
   * @brief Get the users file path object
   *
   * @return const std::string&
   */
  virtual const std::string &get_users_file_path() const noexcept = 0;

  /**
   * @brief Set the pdb file path object
   *
   * @param path const std::string &
   */
  virtual void set_pdb_file_path(const std::string &path) = 0;

  /**
   * @brief Get the pdb file path object
   *
   * @return const std::string&
   */
  virtual const std::string &get_pdb_file_path() const noexcept = 0;

  /**
   * @brief Creates an IServerHandler object
   *
   * @return std::shared_ptr<IServerHandler>
   */
  static std::shared_ptr<IServerHandler> Build();
};

}  // namespace resadmin
}  // namespace isms
