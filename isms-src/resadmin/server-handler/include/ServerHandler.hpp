/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */
#pragma once

#include <ErrorType.hpp>
#include <IPayload.hpp>
#include <memory>
#include <string>

#include "IServerHandler.hpp"

namespace isms {
namespace resadmin {

class ServerHandler : public IServerHandler {
 public:
  /**
   * @brief Construct a new ServerHandler object
   *
   */
  ServerHandler() : running_{false} {}

  /**
   * @brief Destroy the ServerHandler object
   *
   */
  virtual ~ServerHandler() {}

  /**
   * @brief Starts the Resadmin server
   *
   * @return kn::ErrorType
   */
  kn::ErrorType StartServer() override;

  /**
   * @brief Stops the Resadmin server
   *
   * @return kn::ErrorType
   */
  kn::ErrorType StopServer() override;

  /**
   * @brief Set the configuration file path object
   *
   * @param path const std::string &
   */
  void set_configuration_file_path(const std::string &path) override;

  /**
   * @brief Get the configuration file path object
   *
   * @return const std::string&
   */
  const std::string &get_configuration_file_path() const noexcept override;

  /**
   * @brief Set the users file path object
   *
   * @param path const std::string &
   */
  void set_users_file_path(const std::string &path) override;

  /**
   * @brief Get the users file path object
   *
   * @return const std::string&
   */
  const std::string &get_users_file_path() const noexcept override;

  /**
   * @brief Set the pdb file path object
   *
   * @param path const std::string &
   */
  void set_pdb_file_path(const std::string &path) override;

  /**
   * @brief Get the pdb file path object
   *
   * @return const std::string&
   */
  const std::string &get_pdb_file_path() const noexcept override;

 private:
  std::string users_file_path_;
  std::string pdb_file_path_;
  std::string configuration_file_path_;
  bool running_;
};

inline void ServerHandler::set_configuration_file_path(
    const std::string &path) {
  this->configuration_file_path_ = path;
}

inline const std::string &ServerHandler::get_configuration_file_path()
    const noexcept {
  return this->configuration_file_path_;
}

inline void ServerHandler::set_users_file_path(const std::string &path) {
  this->users_file_path_ = path;
}

inline const std::string &ServerHandler::get_users_file_path() const noexcept {
  return this->users_file_path_;
}

inline void ServerHandler::set_pdb_file_path(const std::string &path) {
  this->pdb_file_path_ = path;
}

inline const std::string &ServerHandler::get_pdb_file_path() const noexcept {
  return this->pdb_file_path_;
}

}  // namespace resadmin
}  // namespace isms
