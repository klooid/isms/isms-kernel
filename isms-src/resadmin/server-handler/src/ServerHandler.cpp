/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "ServerHandler.hpp"

#include <fmt/core.h>

#include <execommand.hpp>

using namespace isms;

const std::string kFormatCmd{"{} {}"};
const std::string kServerCmd{"pure-ftpd"};
const std::string kStopCmd{"pkill"};

kn::ErrorType resadmin::ServerHandler::StartServer() {
  if (this->running_) return kn::ErrorType::kCannotOpen;

  auto start_cmd =
      fmt::format(kFormatCmd, kServerCmd, this->configuration_file_path_);

  auto cmd_result = cmd::Exec(start_cmd);

  if (cmd_result.get_error_code() != kn::ErrorType::kSuccess) {
    return cmd_result.get_error_code();
  }
  this->running_ = true;
  return kn::ErrorType::kSuccess;
}

kn::ErrorType resadmin::ServerHandler::StopServer() {
  if (!this->running_) return kn::ErrorType::kCannotClose;
  auto stop_cmd = fmt::format(kFormatCmd, kStopCmd, kServerCmd);

  auto cmd_result = cmd::Exec(stop_cmd);

  if (cmd_result.get_error_code() != kn::ErrorType::kSuccess)
    return cmd_result.get_error_code();

  this->running_ = false;
  return kn::ErrorType::kSuccess;
}
