/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "DoerAdmin.hpp"

#include <DoerPayload.hpp>
#include <ITask.hpp>
#include <algorithm>
#include <iostream>

using namespace isms::doer;
using namespace isms::kn;

ErrorType DoerAdmin::CreateRequest(const RequestType request_type,
                                   const std::shared_ptr<IPayload> &payload) {
  if (payload->get_pid() > 0) payload->set_subprocess(true);

  auto send_result = this->ipc_connection_->SendRequest(request_type, payload);
  if (send_result == ErrorType::kSuccess)
    this->waiting_tasks_.push_back(payload->get_pid());
  return send_result;
}

ErrorType DoerAdmin::RequestListener() {
  while (ipc_connection_->IsRunning()) {
    this->Sleep();

    if (this->tasks_.size() >= this->get_number_of_tasks()) continue;

    auto request = this->ipc_connection_->PopRequest();

    if (request) {
      auto task = ITask::Build();
      task->CreateTask(request);
      this->tasks_.push_back(task);
      this->worker_handler_->CreateWorker(task);
    }
  }
  return ErrorType::kSuccess;
}

ErrorType DoerAdmin::ResponseListener() {
  while (ipc_connection_->IsRunning()) {
    this->Sleep();
    auto response = this->ipc_connection_->PopResponse();

    if (response) {
      /* Temporal message */
      std::cout << "Response received" << std::endl;
    }
  }
  return ErrorType::kSuccess;
}

ErrorType DoerAdmin::Init() {
  auto ipc_result = ipc_connection_->InitIpc();
  if (ipc_result != ErrorType::kSuccess) return ipc_result;

  this->requests_t = std::thread(&DoerAdmin::RequestListener, this);
  this->response_t = std::thread(&DoerAdmin::ResponseListener, this);
  requests_t.join();
  response_t.join();

  return ErrorType::kSuccess;
}
