/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ErrorType.hpp>
#include <IIpcConnection.hpp>
#include <IPayload.hpp>
#include "IWorkerHandler.hpp"

namespace isms {
namespace doer {

class IWorkerHandler;

class IDoerAdmin {
 public:
  IDoerAdmin() {}
  virtual ~IDoerAdmin() {}

  /**
   * CreateRequest
   * @brief This function requests for a process in an external service
   * @param payload IPayload shared pointer with the request information for
   * process creation
   * @return ErrorType, kSuccess if the request is created, any other means an
   * error
   */
  virtual kn::ErrorType CreateRequest(
      const kn::RequestType request_type,
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * RequestListener
   * @brief This function listens for requests that cames from  a current process
   * which is executed
   * @param payload IPayload shared pointer with the request information for
   * process execution
   * @return ErrorType, kSuccess if the task is terminated, any other means an
   * error
   */
  virtual kn::ErrorType RequestListener() = 0;

  /**
   * ResponseListener
   * @brief This function terminates a process which was executed and return the
   * result
   * @param payload IPayload shared pointer with the request information for
   * process execution
   * @return ErrorType, kSuccess if the task is terminated, any other means an
   * error
   */
  virtual kn::ErrorType ResponseListener() = 0;

  /**
   * Init
   * @brief This function starts the manager execution by starting threads and
   * the IPC connection
   * @return ErrorType, kSuccess if the execution started well, any other means
   * an error
   */
  virtual kn::ErrorType Init() = 0;

  /**
   * set_number_of_tasks
   * @brief Sets the number of tasks that could be processing at the same time
   * in the service
   * @param number int, the number of tasks
   */
  virtual void set_number_of_tasks(const int number) = 0;

  /**
   * get_number_of_tasks
   * @brief Provides the number of tasks that could be processing at the same
   * time in the service
   * @return int, the number of tasks
   */
  virtual const int get_number_of_tasks() const noexcept = 0;

  /**
   * set_ipc_connection
   * @brief Sets the IpcConnection handler for the service
   * @param ipc_connection IIpcConnection shared pointer, it handle the IPC
   * communication between the kernel and the service
   */
  virtual void set_ipc_connection(
      const std::shared_ptr<kn::IIpcConnection> &ipc_connection) = 0;

  /**
   * get_ipc_connection
   * @brief Provides the IpcConnection handler for the service
   * @return IIpcConnection shared pointer, it handle the IPC
   * communication between the kernel and the service
   */
  virtual const std::shared_ptr<kn::IIpcConnection> &get_ipc_connection()
      const noexcept = 0;

  /**
   * @brief Set the worker handler object
   *
   * @param worker_handler std::shared_ptr<IWorkerHandler>
   */
  virtual void set_worker_handler(
      const std::shared_ptr<IWorkerHandler> &worker_handler) = 0;

  static std::shared_ptr<IDoerAdmin> Build();
};
}  // namespace doer
}  // namespace isms
