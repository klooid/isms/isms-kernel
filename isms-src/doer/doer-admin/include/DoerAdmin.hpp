/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ITask.hpp>
#include <TimeHandler.hpp>
#include <thread>

#include "IDoerAdmin.hpp"

namespace isms {
namespace doer {

class DoerAdmin : public IDoerAdmin, public kn::TimeHandler {
 public:
  DoerAdmin() {}
  virtual ~DoerAdmin() {}

  /**
   * CreateRequest
   * @brief This function requests for a process in an external service
   * @param payload IPayload shared pointer with the request information for
   * process creation
   * @return ErrorType, kSuccess if the request is created, any other means an
   * error
   */
  kn::ErrorType CreateRequest(
      const kn::RequestType request_type,
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * Init
   * @brief This function starts the manager execution by starting threads and
   * the IPC connection
   * @return ErrorType, kSuccess if the execution started well, any other means
   * an error
   */
  kn::ErrorType Init() override;

  /**
   * RequestListener
   * @brief This function listens for requests that cames from  a current process
   * which is executed
   * @param payload IPayload shared pointer with the request information for
   * process execution
   * @return ErrorType, kSuccess if the task is terminated, any other means an
   * error
   */
  kn::ErrorType RequestListener() override;

  /**
   * ResponseListener
   * @brief This function terminates a process which was executed and return the
   * result
   * @param payload IPayload shared pointer with the request information for
   * process execution
   * @return ErrorType, kSuccess if the task is terminated, any other means an
   * error
   */
  kn::ErrorType ResponseListener() override;

  /**
   * set_number_of_tasks
   * @brief Sets the number of tasks that could be processing at the same time
   * in the service
   * @param number int, the number of tasks
   */
  void set_number_of_tasks(const int number) override;

  /**
   * get_number_of_tasks
   * @brief Provides the number of tasks that could be processing at the same
   * time in the service
   * @return int, the number of tasks
   */
  const int get_number_of_tasks() const noexcept override;

  /**
   * @brief Set the ipc connection object
   *
   * @param ipc_connection
   */
  void set_ipc_connection(
      const std::shared_ptr<kn::IIpcConnection> &ipc_connection) override;

  /**
   * @brief Get the ipc connection object
   *
   * @return const std::shared_ptr<kn::IIpcConnection>&
   */
  const std::shared_ptr<kn::IIpcConnection> &get_ipc_connection()
      const noexcept override;

  /**
   * @brief Set the worker handler object
   *
   * @param worker_handler std::shared_ptr<IWorkerHandler>
   */
  void set_worker_handler(
      const std::shared_ptr<IWorkerHandler> &worker_handler) override;

 private:
  std::vector<int> waiting_tasks_;
  int max_number_tasks_;
  std::shared_ptr<kn::IIpcConnection> ipc_connection_;
  std::thread requests_t;
  std::thread response_t;
  std::vector<std::shared_ptr<ITask>> tasks_;
  std::shared_ptr<IWorkerHandler> worker_handler_;
};

inline void DoerAdmin::set_number_of_tasks(const int number) {
  this->max_number_tasks_ = number;
}

inline const int DoerAdmin::get_number_of_tasks() const noexcept {
  return this->max_number_tasks_;
}

inline void DoerAdmin::set_ipc_connection(
    const std::shared_ptr<kn::IIpcConnection> &ipc_connection) {
  this->ipc_connection_ = ipc_connection;
}

inline const std::shared_ptr<kn::IIpcConnection>
    &DoerAdmin::get_ipc_connection() const noexcept {
  return this->ipc_connection_;
}

inline void DoerAdmin::set_worker_handler(
    const std::shared_ptr<IWorkerHandler> &worker_handler) {
  this->worker_handler_ = worker_handler;
}

}  // namespace doer
}  // namespace isms
