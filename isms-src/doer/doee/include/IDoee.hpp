/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once
#include <iostream>
#include <memory>
#include <string>

#include <ErrorType.hpp>

namespace isms {
namespace doer {

class IDoee {
 public:
  IDoee() {}
  virtual ~IDoee() {}

  virtual kn::ErrorType Init(const std::string &msg);
  virtual kn::ErrorType Execute();
  virtual kn::ErrorType Finish();

 private:
  std::string msg_;

  kn::ErrorType InitDoee(const std::string &msg);
};

/* Factories & Destroyers */
typedef IDoee *IDoeeFactory();
typedef void IDoeeDestroyer(IDoee *);

}  // namespace tm
}  // namespace isms
