/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once
#include <IDoee.hpp>
#include <memory>

#define SUPER isms::doer::IDoee
#define PROVIDE isms::kn::ErrorType

#define DECLARE_NEW_DOEE(NAME)                     \
  class NAME : public SUPER {                      \
   public:                                         \
    NAME() {}                                      \
    virtual ~NAME() {}                             \
    PROVIDE Init(const std::string &msg) override; \
    PROVIDE Execute() override;                    \
    PROVIDE Finish() override;                     \
                                                   \
   private:
#define END_NEW_DOEE(NAME)                        \
  }                                               \
  ;                                               \
  extern "C" {                                    \
  SUPER *create_doee() { return new NAME; }       \
  void destroy_doee(SUPER *doee) { delete doee; } \
  }
