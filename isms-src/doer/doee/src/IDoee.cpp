/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "IDoee.hpp"

isms::kn::ErrorType isms::doer::IDoee::Init(const std::string &msg) {
  this->InitDoee(msg);
  return isms::kn::ErrorType::kSuccess;
}

isms::kn::ErrorType isms::doer::IDoee::Execute() {
  std::cout << "Hello Doee! :)" << this->msg_ << std::endl;
  return isms::kn::ErrorType::kSuccess;
}

isms::kn::ErrorType isms::doer::IDoee::Finish() {
  std::cout << "Goodbye Doee! :C" << std::endl;
  return isms::kn::ErrorType::kSuccess;
}

isms::kn::ErrorType isms::doer::IDoee::InitDoee(const std::string &msg) {
  if (!msg.empty()) {
    this->msg_ = msg;
    return isms::kn::ErrorType::kSuccess;
  } else
    return isms::kn::ErrorType::kCannotCreate;
}
