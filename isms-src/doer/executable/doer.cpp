/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include <ErrorType.hpp>
#include <IDoerAdmin.hpp>
#include <IIpcConnection.hpp>
#include <RequestType.hpp>
#include <IWorkerHandler.hpp>
#include <iostream>
#include <string>

const int kNumberTasks = 3;
const std::string kKernelEndpoint = "kernel_0.ipc";
const std::string kProtocol = "ipc";

int main(int argc, char** argv) {
  auto connection = isms::kn::IIpcConnection::Build();
  auto doer = isms::doer::IDoerAdmin::Build();
  auto worker_handler = isms::doer::IWorkerHandler::Build();

  connection->set_recv_timeout(3000);
  connection->set_comm_protocol(kProtocol);
  connection->set_kernel_endpoint(kKernelEndpoint);
  connection->set_type_of_service(isms::kn::RequestType::kProc);

  doer->set_ipc_connection(connection);
  doer->set_number_of_tasks(kNumberTasks);
  doer->set_worker_handler(worker_handler);
  worker_handler->set_doer_admin(doer);

  std::cout << "Starting Doer" << std::endl;
  auto init_result = doer->Init();

  if (init_result != isms::kn::ErrorType::kSuccess)
    std::cout << "Error in Doer init" << std::endl;
  std::cout << "Doer finished" << std::endl;

  return 0;
}
