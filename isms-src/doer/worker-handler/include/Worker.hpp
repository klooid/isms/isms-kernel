/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include "IWorker.hpp"

namespace isms {
namespace doer {
class Worker : public IWorker {
 public:
  /**
   * @brief Construct a new Worker object
   *
   */
  Worker() : available_{false} {}

  /**
   * @brief Destroy the Worker object
   *
   */
  virtual ~Worker() {}

  /**
   * @brief Set the task object
   *
   * @param task std::shared_ptr<ITask>
   */
  void set_task(const std::shared_ptr<ITask> &task) override;

  /**
   * @brief Set the wid object
   *
   * @param wid int worker id
   */
  void set_wid(const int wid) override;

  /**
   * @brief Set the instance name object
   *
   * @param instance_name std::string
   */
  void set_instance_name(const std::string &instance_name) override;

  /**
   * @brief Get the task object
   *
   * @return const std::shared_ptr<ITask>&
   */
  const std::shared_ptr<ITask> &get_task() const noexcept override;

  /**
   * @brief Get the wid object
   *
   * @return const int
   */
  const int get_wid() const noexcept override;

  /**
   * @brief Get the instance name object
   *
   * @return const std::string&
   */
  const std::string &get_instance_name() const noexcept override;

  /**
   * @brief Set the start time object
   *
   * @param time std::chrono::time_point<std::chrono::system_clock> &
   */
  void set_start_time(
      const std::chrono::time_point<std::chrono::system_clock> &time) override;

  /**
   * @brief Set the task start time object
   *
   * @param time std::chrono::time_point<std::chrono::system_clock> &
   */
  void set_task_start_time(
      const std::chrono::time_point<std::chrono::system_clock> &time) override;

  /**
   * @brief Get the start time object
   *
   * @return const std::chrono::time_point<std::chrono::system_clock>&
   */
  const std::chrono::time_point<std::chrono::system_clock> &get_start_time()
      const noexcept override;

  /**
   * @brief Get the task start time object
   *
   * @return const std::chrono::time_point<std::chrono::system_clock>&
   */
  const std::chrono::time_point<std::chrono::system_clock>
      &get_task_start_time() const noexcept override;

  /**
   * @brief Set the available object
   *
   * @param available
   */
  void set_available(const bool available) override;

  /**
   * @brief Get the available object
   *
   * @return true
   * @return false
   */
  const bool get_available() const noexcept override;

 private:
  std::shared_ptr<ITask> task_;
  unsigned int wid_;
  std::string instance_name_;
  std::chrono::time_point<std::chrono::system_clock> start_time_;
  std::chrono::time_point<std::chrono::system_clock> task_start_time_;
  bool available_;
};

inline void Worker::set_task(const std::shared_ptr<ITask> &task) {
  this->task_ = task;
}

inline void Worker::set_wid(const int wid) { this->wid_ = wid; }

inline void Worker::set_instance_name(const std::string &instance_name) {
  this->instance_name_ = instance_name;
}

inline const std::shared_ptr<ITask> &Worker::get_task() const noexcept {
  return this->task_;
}

inline const int Worker::get_wid() const noexcept { return this->wid_; }

inline const std::string &Worker::get_instance_name() const noexcept {
  return this->instance_name_;
}

inline void Worker::set_start_time(
    const std::chrono::time_point<std::chrono::system_clock> &time) {
  this->start_time_ = time;
}

inline void Worker::set_task_start_time(
    const std::chrono::time_point<std::chrono::system_clock> &time) {
  this->task_start_time_ = time;
}

inline const std::chrono::time_point<std::chrono::system_clock>
    &Worker::get_start_time() const noexcept {
  return this->start_time_;
}

inline const std::chrono::time_point<std::chrono::system_clock>
    &Worker::get_task_start_time() const noexcept {
  return this->task_start_time_;
}

inline void Worker::set_available(const bool available) {
  this->available_ = available;
}

inline const bool Worker::get_available() const noexcept {
  return this->available_;
}

}  // namespace doer
}  // namespace isms
