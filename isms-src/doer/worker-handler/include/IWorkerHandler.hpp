/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ErrorType.hpp>
#include "IDoerAdmin.hpp"
#include <ITask.hpp>

#include "IWorker.hpp"

namespace isms {
namespace doer {

class IDoerAdmin;

class IWorkerHandler {
 public:
  /**
   * @brief Construct a new IWorkerHandler object
   *
   */
  IWorkerHandler() {}

  /**
   * @brief Destroy the IWorkerHandler object
   *
   */
  virtual ~IWorkerHandler() {}

  /**
   * @brief Create a Worker object and container
   *
   * @param task const std::shared_ptr<ITask>
   * @return kn::ErrorType
   */
  virtual kn::ErrorType CreateWorker(const std::shared_ptr<ITask> task) = 0;

  /**
   * @brief Close an specific Worker container by its ID
   *
   * @param wid unsigned int
   * @return kn::ErrorType
   */
  virtual kn::ErrorType CloseWorker(const unsigned int wid) = 0;

  /**
   * @brief Get the Worker Info object by its id
   *
   * @param wid unsigned int
   * @return kn::ErrorType
   */
  virtual std::shared_ptr<IWorker> GetWorkerInfo(const unsigned int wid) = 0;

  /**
   * @brief Set the doer admin object
   *
   * @param doer_admin std::shared_ptr<IDoerAdmin>
   */
  virtual void set_doer_admin(
      const std::shared_ptr<IDoerAdmin> &doer_admin) = 0;

  /**
   * @brief Builds and provide an IWorkerHandler object
   *
   * @return std::shared_ptr<IWorkerHandler>
   */
  static std::shared_ptr<IWorkerHandler> Build();
};

}  // namespace doer
}  // namespace isms
