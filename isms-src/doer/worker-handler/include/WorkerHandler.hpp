/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <vector>

#include "IWorker.hpp"
#include "IWorkerHandler.hpp"

namespace isms {
namespace doer {

class WorkerHandler : public IWorkerHandler {
 public:
  /**
   * @brief Create a Worker object and container
   *
   * @param task std::shared_ptr<ITask>
   * @return kn::ErrorType kSuccess, any other means an error
   */
  kn::ErrorType CreateWorker(const std::shared_ptr<ITask> task);

  /**
   * @brief Close an specific Worker container by its ID
   *
   * @param wid unsigned int
   * @return kn::ErrorType kSuccess, any other means an error
   */
  kn::ErrorType CloseWorker(const unsigned int wid);

  /**
   * @brief Get the Worker Info object by its ID
   *
   * @param wid unsigned int
   * @return kn::ErrorType kSuccess, any other means an error
   */
  std::shared_ptr<IWorker> GetWorkerInfo(const unsigned int wid);

  /**
   * @brief Set the doer admin object
   *
   * @param doer_admin std::shared_ptr<IDoerAdmin>
   */
  void set_doer_admin(const std::shared_ptr<IDoerAdmin> &doer_admin) override;

 private:
  std::vector<std::shared_ptr<IWorker>> workers_;
  std::vector<std::shared_ptr<IWorker>> request_workers_;
  std::shared_ptr<IDoerAdmin> doer_admin_;

  /**
   * @brief Listen to Workers continuously to receive requests and responses
   *
   * @return kn::ErrorType kSuccess, any other means an error
   */
  kn::ErrorType WorkersListener();

  /**
   * @brief Find a worker by its ID
   *
   * @param wid int worker id
   * @return std::vector<std::shared_ptr<IWorker>>::iterator
   */
  std::vector<std::shared_ptr<IWorker>>::iterator FindWorkerByWid(
      const int wid);

  /**
   * @brief Find a worker that is available to use and return it
   *
   */
  std::vector<std::shared_ptr<doer::IWorker>>::iterator FindAvailableWorker();

  /**
   * @brief Configure the worker tast to be processed
   *
   * @param worker std::shared_ptr<IWorker> worker to configure
   * @param task std::shared_ptr<ITask> task that holds the information to be
   * configured
   * @return kn::ErrorType
   */
  kn::ErrorType ConfigureWorker(const std::shared_ptr<IWorker> &worker,
                                const std::shared_ptr<ITask> &task);
};

inline void WorkerHandler::set_doer_admin(
    const std::shared_ptr<IDoerAdmin> &doer_admin) {
  this->doer_admin_ = doer_admin;
}

}  // namespace doer
}  // namespace isms
