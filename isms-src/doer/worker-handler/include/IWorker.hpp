/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ITask.hpp>
#include <chrono>
#include <memory>

namespace isms {
namespace doer {
class IWorker {
 public:
  /**
   * @brief Construct a new IWorker object
   *
   */
  IWorker() {}

  /**
   * @brief Destroy the IWorker object
   *
   */
  virtual ~IWorker() {}

  /**
   * @brief Set the task object
   *
   * @param task std::shared_ptr<ITask>
   */
  virtual void set_task(const std::shared_ptr<ITask> &task) = 0;

  /**
   * @brief Set the wid object
   *
   * @param wid int worker id
   */
  virtual void set_wid(const int wid) = 0;

  /**
   * @brief Set the instance name object
   *
   * @param instance_name std::string &
   */
  virtual void set_instance_name(const std::string &instance_name) = 0;

  /**
   * @brief Get the task object
   *
   * @return const std::shared_ptr<ITask>&
   */
  virtual const std::shared_ptr<ITask> &get_task() const noexcept = 0;

  /**
   * @brief Get the wid object
   *
   * @return const int
   */
  virtual const int get_wid() const noexcept = 0;

  /**
   * @brief Get the instance name object
   *
   * @return const std::string&
   */
  virtual const std::string &get_instance_name() const noexcept = 0;

  /**
   * @brief Set the start time object
   *
   * @param time std::chrono::time_point<std::chrono::system_clock> &
   */
  virtual void set_start_time(
      const std::chrono::time_point<std::chrono::system_clock> &time) = 0;

  /**
   * @brief Set the task start time object
   *
   * @param time std::chrono::time_point<std::chrono::system_clock> &
   */
  virtual void set_task_start_time(
      const std::chrono::time_point<std::chrono::system_clock> &time) = 0;

  /**
   * @brief Get the start time object
   *
   * @return const std::chrono::time_point<std::chrono::system_clock>&
   */
  virtual const std::chrono::time_point<std::chrono::system_clock>
      &get_start_time() const noexcept = 0;

  /**
   * @brief Get the task start time object
   *
   * @return const std::chrono::time_point<std::chrono::system_clock>&
   */
  virtual const std::chrono::time_point<std::chrono::system_clock>
      &get_task_start_time() const noexcept = 0;

  /**
   * @brief Set the available object
   *
   * @param available
   */
  virtual void set_available(const bool available) = 0;

  /**
   * @brief Get the available object
   *
   * @return true
   * @return false
   */
  virtual const bool get_available() const noexcept = 0;

  /**
   * @brief Builds and provides an IWorker object
   *
   * @return std::shared_ptr<IWorker>
   */
  static std::shared_ptr<IWorker> Build();
};

}  // namespace doer
}  // namespace isms
