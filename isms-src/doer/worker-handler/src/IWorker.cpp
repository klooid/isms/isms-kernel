/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "IWorker.hpp"
#include "Worker.hpp"

using namespace isms::doer;

std::shared_ptr<IWorker> IWorker::Build() {
  return std::make_shared<Worker>();
}
