/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "WorkerHandler.hpp"

#include <chrono>
#include <iostream>
#include <sinwrapper.hpp>

using namespace isms;

std::vector<std::shared_ptr<doer::IWorker>>::iterator
doer::WorkerHandler::FindAvailableWorker() {
  auto is_available = [](std::shared_ptr<doer::IWorker> worker) {
    return worker->get_available();
  };
  auto worker_temp =
      std::find_if(std::begin(workers_), std::end(workers_), is_available);

  return worker_temp;
}

kn::ErrorType doer::WorkerHandler::ConfigureWorker(
    const std::shared_ptr<doer::IWorker> &worker,
    const std::shared_ptr<doer::ITask> &task) {
  worker->set_task(task);
  worker->set_task_start_time(std::chrono::system_clock::now());
  task->set_wpid(worker->get_wid());
  return kn::ErrorType::kSuccess;
}

kn::ErrorType doer::WorkerHandler::CreateWorker(
    const std::shared_ptr<ITask> task) {
  if (nullptr == task) {
    return kn::ErrorType::kInvalidPointer;
  }

  kn::ErrorType configuration_result;
  auto temp_worker = FindAvailableWorker();

  if (temp_worker != this->workers_.end()) {
    configuration_result = ConfigureWorker(*temp_worker, task);
  } else {
    auto wid = this->workers_.size();
    auto instance_name = "worker_" + std::to_string(wid);

    auto instance_result = sinw::StartInstance(instance_name);

    if (instance_result != kn::ErrorType::kSuccess)
      return kn::ErrorType::kCannotCreate;

    auto worker_new = doer::IWorker::Build();

    worker_new->set_wid(wid);
    worker_new->set_instance_name(instance_name);
    worker_new->set_start_time(std::chrono::system_clock::now());
    this->workers_.push_back(worker_new);
    configuration_result = ConfigureWorker(worker_new, task);
  }

  return configuration_result;
}

std::vector<std::shared_ptr<doer::IWorker>>::iterator
doer::WorkerHandler::FindWorkerByWid(const int wid) {
  auto is_wid = [wid](std::shared_ptr<IWorker> worker) {
    std::cout << wid << " " << worker->get_wid() << std::endl;
    return worker->get_wid() == wid;
  };
  auto worker_temp =
      std::find_if(std::begin(workers_), std::end(workers_), is_wid);

  return worker_temp;
}

kn::ErrorType doer::WorkerHandler::CloseWorker(const unsigned int wid) {
  auto worker_temp = this->FindWorkerByWid(wid);
  if (worker_temp == workers_.end()) return kn::ErrorType::kNotFound;

  auto close_result =
      sinw::StopInstance(worker_temp->get()->get_instance_name());

  if (close_result != kn::ErrorType::kSuccess) return close_result;

  this->workers_.erase(worker_temp);

  return kn::ErrorType::kSuccess;
}

std::shared_ptr<doer::IWorker> doer::WorkerHandler::GetWorkerInfo(
    const unsigned int wid) {
  auto worker_temp = this->FindWorkerByWid(wid);
  if (worker_temp == workers_.end()) return nullptr;
  auto result = *worker_temp;
  return result;
}

kn::ErrorType doer::WorkerHandler::WorkersListener() {
  return kn::ErrorType::kSuccess;
}
