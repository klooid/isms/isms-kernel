/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <IDoee.hpp>

#include "IDoeeExecutor.hpp"

namespace isms {
namespace doer {
class DoeeExecutor : public IDoeeExecutor {
 public:
  /**
   * @brief Construct a new DoeeExecutor object
   *
   */
  DoeeExecutor() {}

  /**
   * @brief Destroy the DoeeExecutor object
   *
   */
  virtual ~DoeeExecutor() {}

  /**
   * @brief Load and init a doee shared library specified by the doee_file
   *
   * @param doee_file std::string doee file path to be loaded
   * @return kn::ErrorType kSuccess if the load was correct, any other means an
   * error
   */
  kn::ErrorType LoadDoee(const std::string &doee_file) override;

  /**
   * @brief Run the loaded doee
   *
   * @return kn::ErrorType kSuccess if the load was correct, any other means an
   * error
   */
  kn::ErrorType RunDoee() override;

  /**
   * @brief Close the doee
   *
   * @return kn::ErrorType kSuccess if the load was correct, any other means an
   * error
   */
  kn::ErrorType CloseDoee() override;

  /**
   * @brief Set the path object where the doees are located
   *
   * @param path const std::string &
   */
  void set_path(const std::string &path) override;

  /**
   * @brief Get the path object where the doees are located
   *
   * @param path const std::string &
   */
  const std::string &get_path() const noexcept override;

  /**
   * @brief Set the include path object where the doees are located
   *
   * @param path const std::string &
   */
  void set_include(const std::string &path) override;

  /**
   * @brief Get the include path object where the doees are located
   *
   * @param path const std::string &
   */
  const std::string &get_include() const noexcept override;

 private:
  std::string path_;
  std::string include_;
  std::shared_ptr<IDoee> doee_;
};

inline void DoeeExecutor::set_path(const std::string &path) {
  this->path_ = path;
}

inline const std::string &DoeeExecutor::get_path() const noexcept {
  return this->path_;
}

inline void DoeeExecutor::set_include(const std::string &path) {
  this->include_ = path;
}

inline const std::string &DoeeExecutor::get_include() const noexcept {
  return this->include_;
}

}  // namespace doer
}  // namespace isms
