/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ErrorType.hpp>
#include <memory>
#include <string>

namespace isms {
namespace doer {
class IDoeeExecutor {
 public:
  /**
   * @brief Construct a new IDoeeExecutor object
   *
   */
  IDoeeExecutor() {}

  /**
   * @brief Destroy the IDoeeExecutor object
   *
   */
  virtual ~IDoeeExecutor() {}

  /**
   * @brief Load and init a doee shared library specified by the doee_file
   *
   * @param doee_file std::string doee file path to be loaded
   * @return kn::ErrorType kSuccess if the load was correct, any other means an
   * error
   */
  virtual kn::ErrorType LoadDoee(const std::string &doee_file) = 0;

  /**
   * @brief Run the loaded doee
   *
   * @return kn::ErrorType kSuccess if the load was correct, any other means an
   * error
   */
  virtual kn::ErrorType RunDoee() = 0;

  /**
   * @brief Close the doee
   *
   * @return kn::ErrorType kSuccess if the load was correct, any other means an
   * error
   */
  virtual kn::ErrorType CloseDoee() = 0;

  /**
   * @brief Set the path object where the doees are located
   *
   * @param path const std::string &
   */
  virtual void set_path(const std::string &path) = 0;

  /**
   * @brief Get the path object where the doees are located
   *
   * @param path const std::string &
   */
  virtual const std::string &get_path() const noexcept = 0;

  /**
   * @brief Set the include path object where the doees are located
   *
   * @param path const std::string &
   */
  virtual void set_include(const std::string &path) = 0;

  /**
   * @brief Get the include path object where the doees are located
   *
   * @param path const std::string &
   */
  virtual const std::string &get_include() const noexcept = 0;

  /**
   * @brief Builds an IDoeeExecutor object, this shall be used instead of the
   * constructor
   *
   * @return std::shared_ptr<IDoeeExecutor>
   */
  static std::shared_ptr<IDoeeExecutor> Build();
};
}  // namespace doer
}  // namespace isms
