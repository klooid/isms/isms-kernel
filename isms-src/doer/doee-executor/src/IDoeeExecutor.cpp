/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "IDoeeExecutor.hpp"
#include "DoeeExecutor.hpp"

using namespace isms;

std::shared_ptr<doer::IDoeeExecutor> doer::IDoeeExecutor::Build() {
    return std::make_shared<doer::DoeeExecutor>();
}

