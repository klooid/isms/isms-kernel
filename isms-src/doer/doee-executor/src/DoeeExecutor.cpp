/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "DoeeExecutor.hpp"

#include <dlfcn.h>

#include <ILibraryHandler.hpp>
#include <filesystem>

using namespace isms;

auto library_handler = lib::ILibraryHandler::Build();

kn::ErrorType doer::DoeeExecutor::LoadDoee(const std::string& doee_file) {
  if (doee_file.size() <= 0) {
    return kn::ErrorType::kCannotOpen;
  }
  auto file_name = this->path_ + doee_file;

  if (!std::filesystem::exists(file_name)) {
    return kn::ErrorType::kNotFound;
  }

  /* Load the library */
  void* shared_bin;

  auto ld_result = library_handler->LoadLibrary(file_name, shared_bin);

  if (ld_result == isms::kn::ErrorType::kNotFound) {
    dlerror();
    return ld_result;
  }

  /* Reset errors */
  dlerror();

  static IDoeeFactory* create_doee =
      reinterpret_cast<IDoeeFactory*>(dlsym(shared_bin, "create_doee"));
  const char* dlsym_error = dlerror();

  if (dlsym_error) {
    std::cerr << "Cannot load symbol create: " << dlsym_error << std::endl;
    return kn::ErrorType::kCannotCreate;
  }

  static IDoeeDestroyer* destroy_doee =
      reinterpret_cast<IDoeeDestroyer*>(dlsym(shared_bin, "destroy_doee"));
  dlsym_error = dlerror();
  if (dlsym_error) {
    std::cerr << "Cannot load symbol destroy: " << dlsym_error << std::endl;
    return kn::ErrorType::kCannotCreate;
  }

  this->doee_ = std::shared_ptr<IDoee>(create_doee(), destroy_doee);
  auto init_result = this->doee_->Init("I'm testing");
  
  return init_result;
}

kn::ErrorType doer::DoeeExecutor::RunDoee() {
  auto execution_result = this->doee_->Execute();
  return execution_result;
}

kn::ErrorType doer::DoeeExecutor::CloseDoee() {
  auto finish_result = this->doee_->Finish();
  return finish_result;
}
