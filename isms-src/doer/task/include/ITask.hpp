/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <ErrorType.hpp>
#include <IPayload.hpp>
#include <memory>

namespace isms {
namespace doer {

class ITask {
 public:
  /**
   * @brief Construct a new ITask object
   *
   */
  ITask() {}

  /**
   * @brief Destroy the ITask object
   *
   */
  virtual ~ITask() {}

  /**
   * @brief Create a Task object
   *
   * @param payload std::shared_ptr<kn::IPayload> with the information of the
   * Doee to be executed
   * @return kn::ErrorType
   */
  virtual kn::ErrorType CreateTask(
      const std::shared_ptr<kn::IPayload> &payload) = 0;

  /**
   * @brief Terminate the execution of a task
   *
   * @return kn::ErrorType, kSuccess if the the task finished well, any other
   * case means it failed
   */
  virtual kn::ErrorType TerminateTask() = 0;

  /**
   * @brief Check if the task is running
   *
   * @return true
   * @return false
   */
  virtual bool IsRunning() = 0;

  /**
   * @brief Execute the task
   *
   * @return kn::ErrorType, kSuccess if the execution started well, any other
   * case means it failed
   */
  virtual kn::ErrorType Run() = 0;

  /**
   * @brief Provide the result of the task into a Payload
   *
   * @return std::shared_ptr<kn::IPayload> A payload with the result
   */
  virtual std::shared_ptr<kn::IPayload> RetrieveResult() = 0;

  /**
   * @brief Get the pid
   *
   * @return const int
   */
  virtual const int get_pid() const noexcept = 0;

  /**
   * @brief Set the pid
   *
   * @param pid int, the pid of the task
   */
  virtual void set_pid(const int pid) = 0;

  /**
   * @brief Get the task id
   *
   * @return const int
   */
  virtual const int get_wpid() const noexcept = 0;

  /**
   * @brief Set the worker id for the task
   *
   * @param wpid int, the worker pid for the task
   */
  virtual void set_wpid(const int wpid) = 0;

  /**
   * @brief Build create an ITask object
   *
   * @return std::shared_ptr<ITask>
   */
  static std::shared_ptr<ITask> Build();
};

}  // namespace doer
}  // namespace isms
