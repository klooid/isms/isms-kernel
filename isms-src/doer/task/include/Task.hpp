/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <DoerPayload.hpp>
#include <string>

#include "ITask.hpp"

namespace isms {
namespace doer {

class Task : public ITask {
 public:
  /**
   * @brief Construct a new Task object
   *
   */
  Task() : running_{false} {}

  /**
   * @brief Destroy the Task object
   *
   */
  virtual ~Task() {}

  /**
   * @brief Create a Task object
   *
   * @param payload std::shared_ptr<kn::IPayload> with the information of the
   * Doee to be executed
   * @return kn::ErrorType
   */
  kn::ErrorType CreateTask(
      const std::shared_ptr<kn::IPayload> &payload) override;

  /**
   * @brief Terminate the execution of a task
   *
   * @return kn::ErrorType, kSuccess if the the task finished well, any other
   * case means it failed
   */
  kn::ErrorType TerminateTask() override;

  /**
   * @brief Check if the task is running
   *
   * @return true
   * @return false
   */
  bool IsRunning() override;

  /**
   * @brief Execute the task
   *
   * @return kn::ErrorType, kSuccess if the execution started well, any other
   * case means it failed
   */
  kn::ErrorType Run() override;

  /**
   * @brief Provide the result of the task into a Payload
   *
   * @return std::shared_ptr<kn::IPayload> A payload with the result
   */
  std::shared_ptr<kn::IPayload> RetrieveResult() override;

  /**
   * @brief Get the pid
   *
   * @return const int
   */
  const int get_pid() const noexcept override;

  /**
   * @brief Set the pid
   *
   * @param pid int, the pid of the task
   */
  void set_pid(const int pid) override;

  /**
   * @brief Get the task id
   *
   * @return const int
   */
  const int get_wpid() const noexcept override;

  /**
   * @brief Set the worker id for the task
   *
   * @param wpid int, the worker pid for the task
   */
  void set_wpid(const int wpid) override;

 private:
  int pid_;
  int wpid_;
  std::shared_ptr<kn::DoerPayload> payload_;
  bool running_;
};

inline const int Task::get_pid() const noexcept { return this->pid_; }

inline const int Task::get_wpid() const noexcept { return this->wpid_; }

inline void Task::set_pid(const int pid) { this->pid_ = pid; }

inline void Task::set_wpid(const int wpid) { this->wpid_ = wpid; }

}  // namespace doer
}  // namespace isms
