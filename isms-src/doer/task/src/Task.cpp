/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "Task.hpp"

#include <iostream>
#include <string>

using namespace isms::doer;
using namespace isms::kn;

ErrorType Task::CreateTask(const std::shared_ptr<IPayload>& payload) {
  auto doer_payload = IPayload::CastPayload<DoerPayload>(payload);

  this->payload_ = doer_payload;

  return ErrorType::kSuccess;
}

ErrorType Task::Run() {
  auto result = ErrorType::kSuccess;
  this->running_ = true;

  return result;
}

std::shared_ptr<IPayload> Task::RetrieveResult() { return this->payload_; }

ErrorType Task::TerminateTask() {
  auto result = ErrorType::kSuccess;
  this->running_ = false;

  return result;
}

bool Task::IsRunning() { return running_; }
