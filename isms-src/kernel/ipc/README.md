# IPC Module

## Description

The IPC Module will handle the Inter-process Communication between services. It will be accessible through the IPC Connection, a "Client module" for the services' use. To cope with this communication, the IPC Module will take advantage of the ZeroMQ library.

## ZeroMQ Installation

## Environment

* Linux Ubuntu 20.04
* Programming languege: C++
* ZMQ Library: 
    * `cppzmq`
    * A header-only binding for libzmq

## Dependency
To install the libzmq dependency, please run the following commands:

### Linux Ubuntu 20.04

```bash
echo 'deb http://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/xUbuntu_20.04/ /' | sudo tee /etc/apt/sources.list.d/network:messaging:zeromq:release-stable.list

curl -fsSL https://download.opensuse.org/repositories/network:messaging:zeromq:release-stable/xUbuntu_20.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/network_messaging_zeromq_release-stable.gpg > /dev/null

sudo apt update

sudo apt install libzmq3-dev
```

### For more distributions

Please check the [cppzmq](https://github.com/zeromq/cppzmq) repository, to see the installation steps.

## Add the cppzmq library

Build cppzmq via cmake. This does an out of source build and installs the build files

1. Download and unzip the lib, cd to directory which could be found in the [cppzmq](https://github.com/zeromq/cppzmq) repository.
2. Run the following commands:

```bash
mkdir build
cd build
cmake ..
sudo make -j4 install
```

## ZeroMQ and ISMS Kernel relationship explanatory

ZeroMQ is an external open-source project. ISMS Kernel will take advantage of the ZeroMQ properties, so, it will be used just as a tool.
To know more about the ZeroMQ project, please go their [page](https://zeromq.org/). 
