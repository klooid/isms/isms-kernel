/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "IIpcConnection.hpp"

#include "IpcConnection.hpp"

using namespace isms::kn;

std::shared_ptr<IIpcConnection> IIpcConnection::Build() {
  return std::make_shared<IpcConnection>();
}
