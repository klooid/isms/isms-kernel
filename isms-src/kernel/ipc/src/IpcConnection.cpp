/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include "IpcConnection.hpp"

#include <ConnectionPayload.hpp>
#include <IPayload.hpp>
#include <RequestType.hpp>
#include <iostream>
#include <memory>
#include <serialization.hpp>
#include <string>
#include <thread>
#include <zmq.hpp>

using namespace isms::kn;

const std::string kSubSuffix{"_pub.ipc"};
const std::string kPubSuffix{"_sub.ipc"};
const std::string kSeparator{"://"};
const std::string kTempUser{"user"};
const std::string kTempPassword{"password"};

/* ZMQ topics to send messages */
const std::string kAvailabilityTopic{"avbl"};
const std::string kKernelTopic{"kn"};

/* ZMQ topics to subscribe */
const std::string kRequestTopic{"req"};
const std::string kResponseTopic{"resp"};

IpcConnection::IpcConnection() {
  context_ = std::make_shared<zmq::context_t>(amount_io_threads_);
  socket_subscriber_ =
      std::make_shared<zmq::socket_t>(*context_, zmq::socket_type::sub);
  socket_publisher_ =
      std::make_shared<zmq::socket_t>(*context_, zmq::socket_type::pub);
  /* Set socket option as subscriber and recv waiting time */
}

ErrorType IpcConnection::SendConnectionRequest(
    const std::shared_ptr<IPayload> &input_payload,
    const std::shared_ptr<IPayload> &output_payload) {
  /* Create the socket to handle the connection */
  auto conn_socket =
      std::make_shared<zmq::socket_t>(*context_, zmq::socket_type::req);

  auto kernel_path = this->comm_protocol_ + kSeparator + this->kernel_endpoint_;

  conn_socket->connect(kernel_path);

  auto send_result = SendMessage(conn_socket, input_payload);
  if (send_result != ErrorType::kSuccess) return send_result;

  auto recv_result = ReceiveMessage(conn_socket, output_payload);
  if (recv_result != ErrorType::kSuccess) return recv_result;

  conn_socket->close();

  return ErrorType::kSuccess;
}

int IpcConnection::AddNewServiceConnection() {
  auto i_payload = IPayload::Create();

  auto payload_conn = IPayload::CastPayload<ConnectionPayload>(i_payload);
  auto connection_result =
      IPayload::CastPayload<ConnectionPayload>(IPayload::Create());
  /* Send connection request */

  /* Set the data to send */
  SetBasicPayloadFields(RequestType::kConnect, payload_conn);
  payload_conn->set_type_of_service(this->type_of_service_);

  auto send_result = SendConnectionRequest(payload_conn, connection_result);

  if (send_result != ErrorType::kSuccess) return -1;

  this->set_endpoint(connection_result->get_endpoint());

  /* Set the service id*/
  this->set_service_id(connection_result->get_origin_sid());
  return connection_result->get_origin_sid();
}

int IpcConnection::Disconnect() {
  auto dcon_payload = IPayload::Create();
  auto connection_result =
      IPayload::CastPayload<ConnectionPayload>(IPayload::Create());

  SetBasicPayloadFields(RequestType::kDisconnect, dcon_payload);

  /* Send connection request */
  auto send_result = SendConnectionRequest(dcon_payload, connection_result);
  if (send_result != ErrorType::kSuccess) return -1;

  this->running_ = false;

  subscriber_t_.join();

  auto dconn_endpoint = this->comm_protocol_ + kSeparator + this->endpoint_;

  socket_publisher_->disconnect(dconn_endpoint + kPubSuffix);
  socket_subscriber_->disconnect(dconn_endpoint + kSubSuffix);

  socket_publisher_->close();
  socket_subscriber_->close();
  context_->close();

  return connection_result->get_origin_sid();
}

ErrorType IpcConnection::MessageListener() {
  zmq::message_t request;
  while (running_) {
    this->Sleep();
    /* Create the new payload */
    std::shared_ptr<IPayload> payload = IPayload::Create();

    std::string topic;

    auto topic_result = ReceiveTopic(topic);

    if (!running_ || topic_result != ErrorType::kSuccess) continue;

    auto recv_result = ReceiveMessage(socket_subscriber_, payload);

    if (recv_result != ErrorType::kSuccess) continue;

    /* Add the request to the queue */
    if (topic == kRequestTopic) {
      requests_queue_.push(payload);
      this->ChangeAvailability();
    } else if (topic == kResponseTopic) {
      response_queue_.push(payload);
    }
  }
  return ErrorType::kSuccess;
}

ErrorType IpcConnection::InitIpc() {
  auto conn_result = this->AddNewServiceConnection();

  if (conn_result < 0) {
    /* This will include the error handling */
    std::cout << "Connection request failed with number " << conn_result
              << std::endl;
    return ErrorType::kCannotCreate;
  }

  auto conn_endpoint = this->comm_protocol_ + kSeparator + this->endpoint_;

  /* Set the subscriber options */
  this->socket_subscriber_->setsockopt(ZMQ_SUBSCRIBE, kRequestTopic.c_str(),
                                       kRequestTopic.size());
  this->socket_subscriber_->setsockopt(ZMQ_SUBSCRIBE, kResponseTopic.c_str(),
                                       kResponseTopic.size());

  this->socket_subscriber_->setsockopt(ZMQ_RCVTIMEO, this->recv_timeout_);

  /* Set the publication and subscription endpoint to be used */
  this->socket_publisher_->connect(conn_endpoint + kPubSuffix);
  this->socket_subscriber_->connect(conn_endpoint + kSubSuffix);

  /* Setting up the running flag as true to allow message listener */
  this->running_ = true;

  /* Start the MessageListener (subscriber) thread */
  this->subscriber_t_ = std::thread(&IpcConnection::MessageListener, this);
  return ErrorType::kSuccess;
}

ErrorType IpcConnection::SendTopic(const std::string &topic) {
  auto send_result = this->socket_publisher_->send(zmq::buffer(topic),
                                                   zmq::send_flags::sndmore);
  auto result =
      send_result.has_value() ? ErrorType::kSuccess : ErrorType::kCannotSend;
  return result;
}

ErrorType IpcConnection::SendMessage(
    const std::shared_ptr<zmq::socket_t> &socket,
    const std::shared_ptr<IPayload> &payload) {
  std::string stream_str;

  auto ser_result = Serialize(payload, stream_str);
  if (ser_result != ErrorType::kSuccess) return ser_result;

  auto send_result =
      socket->send(zmq::buffer(stream_str), zmq::send_flags::none);

  if (!send_result.has_value()) return ErrorType::kCannotSend;

  return ErrorType::kSuccess;
}

ErrorType IpcConnection::ReceiveTopic(std::string &topic) {
  /* Receive the topic */

  zmq::message_t topic_msg{};
  auto rcv_result = socket_subscriber_->recv(&topic_msg, ZMQ_RCVMORE);
  if (!rcv_result) return ErrorType::kCannotReceive;
  topic = topic_msg.to_string();

  return ErrorType::kSuccess;
}

ErrorType IpcConnection::ReceiveMessage(
    const std::shared_ptr<zmq::socket_t> &socket,
    const std::shared_ptr<IPayload> &payload) {
  /* Message to handle the response */
  zmq::message_t reply{};

  /* Receive the message */
  auto recv_result = socket->recv(reply, zmq::recv_flags::none);

  if (!recv_result.has_value()) return ErrorType::kCannotReceive;

  /* Get the connection request result */
  auto deserialization_result = Deserialize(reply.to_string(), payload);
  if (deserialization_result != ErrorType::kSuccess)
    return deserialization_result;

  return ErrorType::kSuccess;
}

ErrorType IpcConnection::SendRequest(const RequestType request_type,
                                     const std::shared_ptr<IPayload> &payload) {
  if (payload) {
    if (request_type == RequestType::kDisconnect) {
      auto result = this->Disconnect();
      if (result < 0) return ErrorType::kCannotDelete;
    } else {
      SetBasicPayloadFields(request_type, payload);

      auto topic = request_type == RequestType::kChangeAvailability
                       ? kAvailabilityTopic
                       : kKernelTopic;

      auto topic_result = SendTopic(topic);
      if (topic_result != ErrorType::kSuccess) return topic_result;

      auto result = SendMessage(socket_publisher_, payload);
      if (result != ErrorType::kSuccess) return result;
    }
  } else
    return ErrorType::kCannotSend;

  return ErrorType::kSuccess;
}

ErrorType IpcConnection::SendResponse(
    const std::shared_ptr<IPayload> &payload) {
  if (payload) {
    auto topic_result = SendTopic(kKernelTopic);
    if (topic_result != ErrorType::kSuccess) return topic_result;
    payload->set_request_type(RequestType::kResponse);
    auto result = SendMessage(socket_publisher_, payload);
    if (result != ErrorType::kSuccess) return result;
  } else
    return ErrorType::kCannotSend;

  return ErrorType::kSuccess;
}

std::shared_ptr<IPayload> IpcConnection::PopRequest() {
  if (this->requests_queue_.empty()) return nullptr;
  auto payload = this->requests_queue_.front();
  this->requests_queue_.pop();
  ChangeAvailability();

  return payload;
}

std::shared_ptr<IPayload> IpcConnection::PopResponse() {
  if (this->response_queue_.empty()) return nullptr;
  auto payload = this->response_queue_.front();
  this->response_queue_.pop();

  return payload;
}

void IpcConnection::SetBasicPayloadFields(
    const RequestType request_type, const std::shared_ptr<IPayload> &payload) {
  payload->set_origin_sid(this->get_service_id());
  payload->set_user(kTempUser);
  payload->set_password(kTempPassword);
  payload->set_request_type(request_type);
}

ErrorType IpcConnection::ChangeAvailability() {
  this->mtx.lock();
  ErrorType result;
  bool change_result = !this->requests_queue_.empty() ^ this->reception_status_;
  if (change_result) {
    auto conn_payload =
        IPayload::CastPayload<ConnectionPayload>(IPayload::Create());
    conn_payload->set_availability(change_result);

    result = SendRequest(RequestType::kChangeAvailability, conn_payload);
    if (result == ErrorType::kSuccess) this->reception_status_ = !this->reception_status_;
  }
  this->mtx.unlock();

  return result;
}
