/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "IpcModule.hpp"

#include <iostream>
#include <memory>
#include <thread>
#include <zmq.hpp>

#include <serialization.hpp>

using namespace isms::kn;

const std::string kSubSuffix{"_sub.ipc"};
const std::string kPubSuffix{"_pub.ipc"};
const std::string kSeparator{"://"};

/* ZMQ topics to send messages */
const std::string kResponseTopic{"resp"};
const std::string kRequestTopic{"req"};

/* ZMQ topics to subscribe */
const std::string kKernelTopic{"kn"};
const std::string kAvailabilityTopic{"avbl"};

std::shared_ptr<IpcModule> IpcModule::instance_{nullptr};

std::shared_ptr<IpcModule> IpcModule::GetInstance() {
  if (nullptr == instance_) {
    instance_ = std::make_shared<IpcModule>();
  }
  return instance_;
}

/**
 * Constructor
 */
IpcModule::IpcModule() {
  context_ = std::make_shared<zmq::context_t>(amount_io_threads_);
  socket_kernel_ =
      std::make_shared<zmq::socket_t>(*context_, zmq::socket_type::rep);
  socket_subscriber_ =
      std::make_shared<zmq::socket_t>(*context_, zmq::socket_type::sub);
}

int IpcModule::CreateNewService(const std::shared_ptr<IPayload> &payload) {
  auto result = service_controller_->AddService(payload);

  if (result < 0) {
    std::cout << "The SID is: " << result << std::endl;
  }
  return result;
}

ErrorType IpcModule::ConnectionListener() {
  socket_kernel_->bind(comm_protocol_ + kSeparator + endpoint_);
  const int kConnectionErrorSID = -1;

  zmq::message_t request;
  std::string str_stream;
  RequestType request_type;

  while (running_) {
    this->Sleep();
    auto connection_payload =
        IPayload::CastPayload<ConnectionPayload>(IPayload::Create());

    auto recv_result = ReceiveMessage(socket_kernel_, connection_payload);

    if (recv_result != ErrorType::kSuccess) {
      connection_payload->set_origin_sid(kConnectionErrorSID);
    } else {
      request_type = connection_payload->get_request_type();

      /* Check the request type, for connection and disconnection*/
      if (RequestType::kConnect == request_type) {
        /* New service creation */
        ConnectService(connection_payload);
      } else if (RequestType::kDisconnect == request_type) {
        /* Service Disconnection */
        DisconnectService(connection_payload);
      }
    }

    auto send_result = SendMessage(socket_kernel_, connection_payload);
    if (send_result != ErrorType::kSuccess)
      std::cout << "Can't send connection response" << std::endl;
  }
  return ErrorType::kSuccess;
}

ErrorType IpcModule::MessageListener() {
  int dest_service_id;

  while (running_) {
    this->Sleep();

    std::string topic;

    auto topic_result = ReceiveTopic(topic);
    if (topic_result != ErrorType::kSuccess) continue;

    /* Input payload */
    auto payload = IPayload::Create();

    /* Receiving the message*/
    auto result = ReceiveMessage(socket_subscriber_, payload);

    if (result != ErrorType::kSuccess) continue;

    /* Check the topic received */
    if (topic == kAvailabilityTopic) {
      auto change_result = this->scheduler_->ChangeServiceAvailability(payload);

      if (change_result != ErrorType::kSuccess)
        std::cout << "Cannot change" << std::endl;

    } else if (topic == kKernelTopic) {
      /* Search for the service destination identifier */
      if (payload->get_request_type() == RequestType::kResponse)
        dest_service_id = payload->get_origin_sid();
      else
        dest_service_id =
            service_controller_->FindServiceId(payload->get_request_type());

      payload->set_dest_sid(dest_service_id);
      process_controller_->AddProcess(payload);
    }
  }

  return ErrorType::kSuccess;
}

ErrorType IpcModule::SendTopic(const std::shared_ptr<zmq::socket_t> &socket,
                               const std::string &topic) {
  auto send_result = socket->send(zmq::buffer(topic), zmq::send_flags::sndmore);
  auto result =
      send_result.has_value() ? ErrorType::kSuccess : ErrorType::kCannotSend;
  return result;
}

ErrorType IpcModule::SendMessage(const std::shared_ptr<zmq::socket_t> &socket,
                                 const std::shared_ptr<IPayload> &payload) {
  std::string stream_str;

  auto ser_result = Serialize(payload, stream_str);
  if (ser_result != ErrorType::kSuccess) return ser_result;

  auto send_result =
      socket->send(zmq::buffer(stream_str), zmq::send_flags::none);
  if (!send_result.has_value()) return ErrorType::kCannotSend;

  return ErrorType::kSuccess;
}

ErrorType IpcModule::ReceiveTopic(std::string &topic) {
  /* Receive the topic */

  zmq::message_t topic_msg{};
  auto rcv_result = socket_subscriber_->recv(&topic_msg, ZMQ_RCVMORE);
  if (!rcv_result) return ErrorType::kCannotReceive;
  topic = topic_msg.to_string();

  return ErrorType::kSuccess;
}

ErrorType IpcModule::ReceiveMessage(
    const std::shared_ptr<zmq::socket_t> &socket,
    const std::shared_ptr<IPayload> &payload) {
  /* Message to handle the response */
  zmq::message_t reply{};

  /* Receive the message */
  auto recv_result = socket->recv(reply, zmq::recv_flags::none);
  if (!recv_result.has_value()) return ErrorType::kCannotReceive;

  /* Get the connection request result */
  auto deserialization_result = Deserialize(reply.to_string(), payload);
  if (deserialization_result != ErrorType::kSuccess)
    return deserialization_result;

  return ErrorType::kSuccess;
}

ErrorType IpcModule::SendMessageToService(
    const std::shared_ptr<IPayload> &payload) {
  /* Find the pub socket for the respective destination service */
  auto publisher_iter = publishers_.find(payload->get_dest_sid());

  if (publisher_iter != publishers_.end()) {
    auto socket = publisher_iter->second;

    /* Select the topic to send */
    auto topic = (payload->get_request_type() == RequestType::kResponse)
                     ? kResponseTopic
                     : kRequestTopic;

    auto topic_result = SendTopic(socket, topic);
    if (topic_result != ErrorType::kSuccess) {
      std::cout << "Cannot send the topic" << std::endl;
      return topic_result;
    }
    auto result = SendMessage(socket, payload);

    if (result != ErrorType::kSuccess) {
      std::cout << "It is not sending properly" << std::endl;
      return result;
    }
    process_controller_->PrintKernelStatus();
  } else {
    std::cout << "Service not found " << payload->get_dest_sid() << std::endl;
    return ErrorType::kNotFound;
  }
  return ErrorType::kSuccess;
}

int IpcModule::ConnectService(const std::shared_ptr<IPayload> &payload) {
  auto connection_payload = IPayload::CastPayload<ConnectionPayload>(payload);

  auto service_id = CreateNewService(connection_payload);

  /* Check the Connection result */
  if (service_id < 0) {
    std::cout << "Connection Request failed by " << service_id << std::endl;
    connection_payload->set_origin_sid(service_id);
  } else {
    auto service_endpoint = connection_payload->get_endpoint();
    auto bind_result = BindService(service_id, service_endpoint);
    if (bind_result != ErrorType::kSuccess) {
      return -2;
    }
  }
  return service_id;
}

int IpcModule::DisconnectService(const std::shared_ptr<IPayload> &payload) {
  auto service_id = payload->get_origin_sid();
  std::shared_ptr<Service> service_temp;
  auto return_result =
      service_controller_->ReturnServiceInfo(service_id, service_temp);

  if (return_result == ErrorType::kSuccess) {
    auto service_endpoint = service_temp->get_endpoint();

    auto unbind_result = this->UnbindService(service_id, service_endpoint);

    if (unbind_result != ErrorType::kSuccess) {
      std::cout << "Can't unbind" << std::endl;
      return -3;
    }

    /* Delete the service from the service Controller register */
    auto deletion_result = service_controller_->DeleteService(payload);

    /* Check if the deletion was correct */
    if (deletion_result < 0) {
      std::cout << "Deletion fails by: " << deletion_result << std::endl;
      return -4;
    }

  } else {
    std::cout << "Cant found the service" << std::endl;
    return -2;
  }

  return service_id;
}

ErrorType IpcModule::BindService(const int service_id,
                                 const std::string &endpoint) {
  /* Create the pub socket for the new service */
  auto new_pub_service =
      std::make_shared<zmq::socket_t>(*context_, zmq::socket_type::pub);

  /* Adding the subscribtion and the publication socket for the service */
  socket_subscriber_->bind(this->comm_protocol_ + kSeparator + endpoint +
                           kSubSuffix);
  /* Adding the publisher for the service */
  new_pub_service->bind(this->comm_protocol_ + kSeparator + endpoint +
                        kPubSuffix);

  /* Add the pub socket to the publishers unoredered map */
  auto insertion_result =
      publishers_.insert(std::make_pair(service_id, new_pub_service));

  if (!insertion_result.second) return ErrorType::kCannotCreate;
  return ErrorType::kSuccess;
}

ErrorType IpcModule::UnbindService(const int service_id,
                                   const std::string &endpoint) {
  /* Unbind the endpoint from the socket subscriber */
  socket_subscriber_->unbind(this->comm_protocol_ + kSeparator + endpoint +
                             kSubSuffix);

  /* Find the publisher for the service */
  auto publisher_iter = publishers_.find(service_id);
  if (publisher_iter == publishers_.end()) return ErrorType::kNotFound;

  /* Unbind the publication endpoint */
  publisher_iter->second->unbind(this->comm_protocol_ + kSeparator + endpoint +
                                 kPubSuffix);
  /* Close the publisher socket */
  publisher_iter->second->close();

  /* Delete the publisher from the publishers register */
  auto deletion_result = publishers_.erase(service_id);
  if (deletion_result == 0) {
    std::cout << "Can't delete" << std::endl;
    return ErrorType::kCannotDelete;
  }

  return ErrorType::kSuccess;
}

ErrorType IpcModule::InitIpc() {
  this->running_ = true;

  /* Set the subscriber options */
  this->socket_subscriber_->setsockopt(
      ZMQ_SUBSCRIBE, kAvailabilityTopic.c_str(), kAvailabilityTopic.size());
  this->socket_subscriber_->setsockopt(ZMQ_SUBSCRIBE, kKernelTopic.c_str(),
                                       kKernelTopic.size());
  this->socket_subscriber_->setsockopt(ZMQ_RCVTIMEO, this->recv_timeout_);

  /* Start the ConnectionListenner and MessageListener threads */
  connection_t_ = std::thread(&IpcModule::ConnectionListener, instance_);
  subscriber_t_ = std::thread(&IpcModule::MessageListener, instance_);

  connection_t_.join();
  subscriber_t_.join();

  return ErrorType::kSuccess;
}
