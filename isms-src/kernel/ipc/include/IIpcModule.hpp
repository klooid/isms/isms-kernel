/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <memory>

#include "IIpc.hpp"
#include <IPayload.hpp>
#include "IProcessController.hpp"
#include "IScheduler.hpp"
#include "IServiceController.hpp"
#include <TimeHandler.hpp>
#include <zmq.hpp>

namespace isms {
namespace kn {

/**
 * Ipc Class
 * @brief This is the definition class of the Ipc module which handle the
 * Inter-process communication between the services into the ISMS Kernel. It is
 * the brain of the system communication.
 */
class IIpcModule : public virtual IIpc, public TimeHandler {
 public:
  IIpcModule() {}
  virtual ~IIpcModule() {}
  /**
   * InitIpc
   * @brief Starts the IPC module execution by creating the MessageListener
   * thread, the ConnectionListener and the Publication handler
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType InitIpc() = 0;

  /**
   * SendMessageToService
   * @brief SendMessageToService is used to send a new message to a service
   * @param payload It's an IPayload instance which contains the information of
   * the message
   * @return ErrorType enum class, kSuccess if the execution could be done, any
   * other Error means there was a problem
   */
  virtual ErrorType SendMessageToService(
      const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * Build
   * @brief Provides the IpcModule shared pointer which is a singleton
   * implementation that handles the services communication
   * @return IpcModule shared pointer, it is the Ipc implemetation which is used
   * into the Kernel
   */
  static std::shared_ptr<IIpcModule> Build();

  /**
   * set_service_controller
   * @brief Sets the ServiceController object which handles the services
   * information, the IpcModule use the ServiceController for connections,
   * disconnections and any other service request
   * @param s_controller ServiceController shared pointer used to handle
   * services information
   */
  virtual void set_service_controller(
      const std::shared_ptr<IServiceController> &s_controller) = 0;

  /**
   * set_process_controller
   * @brief Sets the ProcessController object which handles the processes
   * information, the IpcModule use the ProcessController to handle the
   * processes creation, deletion, information, and any other function related
   * to processes requested from services.
   * @param p_controller ProcessController shared pointer used to handle
   * processes
   */
  virtual void set_process_controller(
      const std::shared_ptr<IProcessController> &p_controller) = 0;

  /**
   * set_scheduler
   * @brief Sets the ProcScheduler object which handles the scheduling process
   * it will be used to change the services reception status when a change
   * availability is requested from them
   * @param scheduler Scheduler shared pointer
   */
  virtual void set_scheduler(const std::shared_ptr<IScheduler> &scheduler) = 0;

  /**
   * CreateNewService
   * @brief CreateNewService is used to create a new service connection
   * @param payload it's an IPayload instance which contains the information to
   * add new service
   * @return The creation result
   */
  virtual int CreateNewService(const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * DisconnectService
   * @brief DisconnectService is used to remove a service connection
   * @param payload it's an IPayload instance which contains the information to
   * remove the service
   * @return Int, ServiceID
   */
  virtual int DisconnectService(const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * ConnectService
   * @brief ConnectService is used to connect a new service
   * @param payload it's an IPayload instance which contains the information to
   * add the service
   * @return int, ServiceId
   */
  virtual int ConnectService(const std::shared_ptr<IPayload> &payload) = 0;
};

}  // namespace kn
}  // namespace isms
