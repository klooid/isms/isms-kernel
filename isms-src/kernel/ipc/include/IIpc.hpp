/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <memory>
#include <string>

#include <IPayload.hpp>
#include <ErrorType.hpp>
#include <RequestType.hpp>

namespace isms {
namespace kn {
/**
 * IIpc Class
 * @brief This is the Interface IIpc definition class for the Ipc components
 * This class will be used to implement the infrastructure for Inter-process
 * communication between the services and the kernel
 */
class IIpc {
 public:
  IIpc() {}
  virtual ~IIpc() {}
  /**
   * get_endpoint
   * @brief Provides the endpoint assigned to the service
   * @return String, endpoint
   */
  const std::string &get_endpoint() const noexcept;

  /**
   * set_endpoint
   * @brief Sets the endpoint assigned to the service
   * @param endpoint String, endpoint
   */
  void set_endpoint(const std::string &endpoint);

  /**
   * get_comm_protocol
   * @brief Provides the communication setted to handle the communication
   * @return string, protocol
   */
  const std::string &get_comm_protocol() const noexcept;

  /**
   * set_comm_protocol
   * @brief Sets the communication protocol that will be used to communicate
   * thorugh the IPC
   * @param protocol string, protocol
   */
  void set_comm_protocol(const std::string &protocol);

  /**
   * IsRunning
   * @brief Provides the execution state of the ipc
   * @return bool, true if the communication is running, false if don't
   */
  const bool IsRunning() const noexcept;

  /**
   * InitIpc
   * @brief Starts the IPC connection execution by starting the needed sockets,
   * threads, etc, it should be overrited in inherited classes
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType InitIpc() = 0;

  /**
   * MessageListener
   * @brief MessageListener is used to handle the Ipc input requests,
   * these inputs can be different depending on the implementation, for
   * IpcConnection or Ipc module
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType MessageListener() = 0;

 protected:
  std::string endpoint_;
  std::string comm_protocol_;
  bool running_;
  const int amount_io_threads_ = 1;
};

inline const bool IIpc::IsRunning() const noexcept { return this->running_; }

inline const std::string &IIpc::get_endpoint() const noexcept {
  return this->endpoint_;
}

inline void IIpc::set_endpoint(const std::string &endpoint) {
  this->endpoint_ = endpoint;
}

inline const std::string &IIpc::get_comm_protocol() const noexcept {
  return this->comm_protocol_;
}

inline void IIpc::set_comm_protocol(const std::string &protocol) {
  this->comm_protocol_ = protocol;
}

}  // namespace kn
}  // namespace isms
