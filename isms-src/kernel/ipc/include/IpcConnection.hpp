/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#pragma once

#include <memory>
#include <queue>
#include <thread>
#include <zmq.hpp>
#include <mutex>

#include "IIpcConnection.hpp"

namespace isms {
namespace kn {

/**
 * IpcConnection Class
 * @brief This is the definition class for the IpcConnection, which
 * provide the communication interface to the services, it handles the
 * connection function and the messaging transference.
 */
class IpcConnection : public IIpcConnection {
 public:
  IpcConnection();
  virtual ~IpcConnection() {}
  /** AddNewServiceConnection
   * @brief This function allows services to connect to the ISMS Kernel through
   * the IPC
   * @param user user registered in the system to be verified before make the
   * connection
   * @param password user password
   * @return The Service ID / -1 if the connection fails
   */
  int AddNewServiceConnection() override;

  /** Disconnect
   * @brief This function allows services disconnection to the ISMS Kernel
   * through the IPC
   * @param user user registered in the system to be verified before make the
   * connection
   * @param password user password
   * @return The Service ID / -1 if the request fails
   */
  int Disconnect() override;

  /**
   * PopRequest
   * @brief Provides the last request which arrived to the queue
   * @return IPayload, Shared pointer
   */
  std::shared_ptr<IPayload> PopRequest() override;

  /**
   * PopResponse
   * @brief Provides the last response that arrived through the IPC which the
   * origin is a request called from the current service
   * @return IPayload, Shared pointer
   */
  std::shared_ptr<IPayload> PopResponse() override;

  /**
   * InitIpc
   * @brief Starts the IPCConnection execution by starting the needed sockets,
   * threads for publisher, subscriber.
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType InitIpc() override;

  /**
   * SendRequest
   * @brief SendRequest is used to send a new message to the
   * @param request_type RequestType, it is the request type for the message,
   * such as:
   *    - kCon : connect
   *    - kDcon : disconnect
   *    - kComm: communication
   *    - kProc: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @param payload It's an IPayload instance which contains the information of
   * the message
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType SendRequest(const RequestType request_type,
                        const std::shared_ptr<IPayload> &payload) override;

  /**
   * SendResponse
   * @brief SendResponse is used to send a the response for a request
   * @param payload It's an IPayload instance which contains the message
   * information
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType SendResponse(const std::shared_ptr<IPayload> &payload) override;

  /**
   * get_ipc_endpoint
   * @brief Provides the IPC endpoint of the IPC kernel module server
   * @return std::string with the endpoint
   */
  const std::string &get_kernel_endpoint() const noexcept override;

  /**
   * set_ipc_ip
   * @brief Sets the ipc_ip for the IPC kernel module
   * @param endpoint the endpoint string
   */
  void set_kernel_endpoint(const std::string &endpoint) override;

  /**
   * get_service_id
   * @brief Provides the Service ID for this service
   * @return int greater than -1
   */
  const int get_service_id() const noexcept override;

  /**
   * get_type_of_service
   * @brief Provides the service type
   * @return RequestType, it indicates the type of requests that handle the
   * service
   */
  const RequestType get_type_of_service() const noexcept override;

  /**
   * set_type_of_service
   * @brief Sets the service type
   * @param type_of_service int, service type
   */
  void set_type_of_service(const RequestType type_of_service) override;

 private:
  std::shared_ptr<zmq::socket_t> socket_kernel_;
  std::shared_ptr<zmq::socket_t> socket_publisher_;
  std::shared_ptr<zmq::socket_t> socket_subscriber_;
  std::shared_ptr<zmq::context_t> context_;

  std::string kernel_endpoint_;
  int service_id_;
  bool reception_status_;
  RequestType type_of_service_;
  std::queue<std::shared_ptr<IPayload>> requests_queue_;
  std::queue<std::shared_ptr<IPayload>> response_queue_;
  std::thread subscriber_t_;
  std::string input_topic_;
  std::mutex mtx;

  /**
   * set_service_endpoint
   * @brief Sets the service endpoint which allows the service to
   * communicate with the kernel module
   * @param service_endpoint It is a string with the service endpoint
   * connection
   */
  void set_service_endpoint(const std::string &_service_endpoint);

  /**
   * set_ipc_ip
   * @brief Sets the Service ID after the new connection
   * @param service_id the int value with the Service ID after the
   * successed connection
   */
  void set_service_id(int service_id);

  /**
   * SendConnectionRequest
   * @brief SendConnectionRequest is used to send requests related to
   * connections and disconnections
   * @param input_payload It's an IPayload instance which contains the
   * information of the message
   * @param output_payload It's an IPayload instance which will contain the
   * result of the message
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType SendConnectionRequest(
      const std::shared_ptr<IPayload> &input_output,
      const std::shared_ptr<IPayload> &output_payload) override;

  /**
   * MessageListener
   * @brief MessageListener is used to listen incomming requests from kernel,
   * these messages are requests that shall be processed and their origin are
   * from other services.
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType MessageListener();

  /**
   * SendMessage
   * @brief SendMessage is used to send a new message to the requested socket
   * @param socket It is a zmq::socket sahred pointer which will be used to
   * send the message
   * @param payload It's an IPayload instance which contains the information of
   * the message
   * @return ErrorType enum class, kSuccess if the execution could be done, any
   * other Error instead
   */
  ErrorType SendMessage(const std::shared_ptr<zmq::socket_t> &socket,
                        const std::shared_ptr<IPayload> &payload);

  /**
   * SendFunction
   * @brief The messages are sent to different topics, before sending a message,
   * the topic shall be specified then, this functions is responsible for
   * sending the topicc where the message will be sent
   * @param topic std::string, the specific topic
   * @return ErrorType enum class, kSuccess if the execution could be done, any
   * other Error instead
   */
  ErrorType SendTopic(const std::string &topic);

  /**
   * ReceiveMessage
   * @brief Receive message is used to provide the entry messages incoming
   * through the IPC
   * @param socket It is a zmq::socket sahred pointer which will be used to
   * receive the message
   * @param payload It's an IPayload instance which will contain the received
   * message
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType ReceiveMessage(const std::shared_ptr<zmq::socket_t> &socket,
                           const std::shared_ptr<IPayload> &payload);

  /**
   * ReceiveTopic
   * @brief This functions will receive the topic of the incomming message
   * @param topic string, it is the received topic
   * @return ErrorType enum class, kSuccess if the execution could be done, any
   * other Error instead
   */
  ErrorType ReceiveTopic(std::string &topic);

  /**
   * ChangeAvailability
   * @brief Checks the services reception status, if the queue is filled, the
   * status will be setted as false which means the service is unavailable, if
   * it is setted as true, the service is available
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType ChangeAvailability();

  /**
   * SetBasicPayloadFields
   * @brief Sets the basic fileds for a payloads
   * @param request_type The RequestType to be sent
   * @param payload Payload to be sent, it will be setted
   */
  void SetBasicPayloadFields(const RequestType request_type,
                            const std::shared_ptr<IPayload> &payload);
};

inline const RequestType IpcConnection::get_type_of_service() const noexcept {
  return this->type_of_service_;
}

inline void IpcConnection::set_type_of_service(
    const RequestType type_of_service) {
  this->type_of_service_ = type_of_service;
}

inline const std::string &IpcConnection::get_kernel_endpoint() const noexcept {
  return this->kernel_endpoint_;
}

inline void IpcConnection::set_kernel_endpoint(
    const std::string &ipc_endpoint) {
  this->kernel_endpoint_ = ipc_endpoint;
}

inline const int IpcConnection::get_service_id() const noexcept {
  return this->service_id_;
}

inline void IpcConnection::set_service_id(int service_id) {
  this->service_id_ = service_id;
}
}  // namespace kn
}  // namespace isms
