/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <memory>
#include <thread>
#include <unordered_map>
#include <zmq.hpp>

#include "IIpcModule.hpp"

namespace isms {
namespace kn {
/**
 * Ipc Class
 * @brief This is the definition class of the Ipc module which handle the
 * Inter-process communication between the services into the ISMS Kernel. It is
 * the brain of the system communication.
 */
class IpcModule : public IIpcModule {
 public:
  /**
   * GetInstance
   * @brief GetInstance is used to create a new instance for the Ipc as
   * singleton
   * @return The Ipc instance
   */
  static std::shared_ptr<IpcModule> GetInstance();

  /**
   * InitIpc
   * @brief Starts the IPC module execution by creating the MessageListener
   * thread, the ConnectionListener and the Publication handler
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType InitIpc() override;

  /**
   * SendMessageToService
   * @brief SendMessageToService is used to send a new message to a service
   * @param payload It's an IPayload instance which contains the information of
   * the message
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType SendMessageToService(
      const std::shared_ptr<IPayload> &payload) override;

  /**
   * set_service_controller
   * @brief Sets the ServiceController object which handles the services
   * information, the IpcModule use the ServiceController for connections,
   * disconnections and any other service request
   * @param s_controller ServiceController shared pointe used to handle
   * services information
   */
  void set_service_controller(
      const std::shared_ptr<IServiceController> &s_controller) override;

  /**
   * set_scheduler
   * @brief Sets the ProcScheduler object which handles the scheduling process
   * it will be used to change the services reception status when a change
   * availability is reqeusted from them
   * @param scheduler Scheduler shared pointer
   */
  void set_scheduler(const std::shared_ptr<IScheduler> &scheduler) override;

  /**
   * set_process_controller
   * @brief Sets the ProcessController object which handles the processes
   * information, the IpcModule use the ProcessController to handle the
   * processes creation, deletion, information, and any other function related
   * to processes requested from services.
   * @param p_controller ProcessController shared pointer used to handle
   * processes
   */
  void set_process_controller(
      const std::shared_ptr<IProcessController> &p_controller) override;

  IpcModule();
  /* Destructor */
  virtual ~IpcModule() {}

 private:
  std::shared_ptr<zmq::socket_t> socket_kernel_;
  std::shared_ptr<zmq::socket_t> socket_subscriber_;
  std::unordered_map<int, std::shared_ptr<zmq::socket_t>> publishers_;
  std::shared_ptr<zmq::context_t> context_;

  std::shared_ptr<IServiceController> service_controller_;
  std::shared_ptr<IProcessController> process_controller_;
  std::shared_ptr<IScheduler> scheduler_;
  static std::shared_ptr<IpcModule> instance_;
  /* Instance for the Singleton Pattern */
  std::thread subscriber_t_;
  std::thread connection_t_;

  /**
   * CreateNewService
   * @brief CreateNewService is used to create a new service connection
   * @param payload it's an IPayload instance which contains the information to
   * add new service
   * @return The creation result
   */
  int CreateNewService(const std::shared_ptr<IPayload> &payload) override;

  /**
   * DisconnectService
   * @brief DisconnectService is used to remove a service connection
   * @param payload it's an IPayload instance which contains the information to
   * remove the service
   * @return Int, ServiceID
   */
  int DisconnectService(const std::shared_ptr<IPayload> &payload) override;

  /**
   * ConnectService
   * @brief ConnectService is used to connect a new service
   * @param payload it's an IPayload instance which contains the information to
   * add the service
   * @return int, ServiceId
   */
  int ConnectService(const std::shared_ptr<IPayload> &payload) override;

  /**
   * BindService
   * @brief BindService is used to create and bind the services sockets
   * @param service_id it's an int which indicates the service id of the service
   * sockets that will be added
   * @param endpoint it's a string which contains the information of the service
   * endpoint to create and bind the sockets
   * @return Int, ServiceIDs
   */
  ErrorType BindService(const int service_id, const std::string &endpoint);

  /**
   * UnbindService
   * @brief UnbindService is unbind and close the services sockets
   * @param service_id it's an int which indicates the service id of the service
   * sockets that will be removed
   * @param endpoint it's a string which contains the information of the service
   * endpoint to close and unbind the sockets
   * @return Int, ServiceIDs
   */
  ErrorType UnbindService(const int service_id, const std::string &endpoint);

  /**
   * ConnectionListener
   * @brief ConnectionListener is used to listen connection and disconnection
   * requests from services
   */
  ErrorType ConnectionListener();

  /**
   * MessageListener
   * @brief It is the function that handles the incomming messages from
   * services, these messages are requests that shall be processed by the
   * Process Controller
   */
  ErrorType MessageListener();

  /**
   * SendTopic
   * @brief The messages are sent to different topics, before sending a message,
   * the topic shall be specified then, this functions is responsible for
   * sending the topic where the message will be sent
   * @param topic std::string, the specific topic
   * @param socket It is a zmq::socket shared pointer which will be used to
   * send the topic message
   * @return ErrorType enum class, kSuccess if the execution could be done, any
   * other Error instead
   */
  ErrorType SendTopic(const std::shared_ptr<zmq::socket_t> &socket,
                      const std::string &topic);

  /**
   * SendMessage
   * @brief SendMessage is used to send a new message to the requested socket
   * @param socket It is a zmq::socket sahred pointer which will be used to
   * send the message
   * @param payload It's an IPayload instance which contains the information of
   * the message
   * @return ErrorType enum class, kSuccess if the execution could be done, any
   * other Error instead
   */
  ErrorType SendMessage(const std::shared_ptr<zmq::socket_t> &socket,
                        const std::shared_ptr<IPayload> &payload);

  /**
   * ReceiveMessage
   * @brief Receive message is used to provide the entry messages incoming
   * through the IPC
   * @param socket It is a zmq::socket sahred pointer which will be used to
   * receive the message
   * @param payload It's an IPayload instance which will contain the received
   * message
   * @return ErrorType enum class, kSuccess if the execution could be done, any
   * other Error instead
   */
  ErrorType ReceiveMessage(const std::shared_ptr<zmq::socket_t> &socket,
                           const std::shared_ptr<IPayload> &payload);
  /**
   * ReceiveTopic
   * @brief This function will receive the topic of the incomming message
   * @param topic string, it is the received topic
   * @return ErrorType enum class, kSuccess if the execution could be done, any
   * other Error instead
   */
  ErrorType ReceiveTopic(std::string &topic);
};

inline void IpcModule::set_service_controller(
    const std::shared_ptr<IServiceController> &s_controller) {
  this->service_controller_ = s_controller;
}

inline void IpcModule::set_process_controller(
    const std::shared_ptr<IProcessController> &p_controller) {
  this->process_controller_ = p_controller;
}

inline void IpcModule::set_scheduler(
    const std::shared_ptr<IScheduler> &scheduler) {
  this->scheduler_ = scheduler;
}

}  // namespace kn
}  // namespace isms
