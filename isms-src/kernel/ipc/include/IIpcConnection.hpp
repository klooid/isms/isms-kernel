/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <memory>

#include "IIpc.hpp"
#include <IPayload.hpp>
#include <RequestType.hpp>
#include <TimeHandler.hpp>

namespace isms {
namespace kn {

/**
 * IpcConnection Class
 * @brief This is the interface class for the IpcConnection, which
 * provide the communication tools to the services, it handles the
 * connection functions and the messaging transference.
 */
class IIpcConnection : public virtual IIpc, public TimeHandler {
 public:
  IIpcConnection() {}
  virtual ~IIpcConnection() {}

  /** AddNewServiceConnection
   * @brief This function allows services to connect to the ISMS Kernel through
   * the IPC
   * @return The Service ID / -1 if the connection fails
   */
  virtual int AddNewServiceConnection() = 0;

  /** Disconnect
   * @brief This function allows services disconnection to the ISMS Kernel
   * through the IPC
   * @return The Service ID / -1 if the request fails
   */
  virtual int Disconnect() = 0;

  /**
   * PopRequest
   * @brief Provides the last request which arrived to the queue
   * @return IPayload, Shared pointer
   */
  virtual std::shared_ptr<IPayload> PopRequest() = 0;

  /**
   * PopResponse
   * @brief Provides the last response that arrived through the IPC which the
   * origin is a request called from the current service
   * @return IPayload, Shared pointer
   */
  virtual std::shared_ptr<IPayload> PopResponse() = 0;

  /**
   * InitIpc
   * @brief Starts the IPCConnection execution by starting the needed sockets,
   * threads for publisher, subscriber.
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType InitIpc() = 0;

  /**
   * SendRequest
   * @brief SendRequest is used to send a new message to the
   * @param request_type RequestType, it is the request type for the message,
   * such as:
   *    - kCon : connect
   *    - kDcon : disconnect
   *    - kComm: communication
   *    - kProc: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @param payload It's an IPayload instance which contains the information of
   * the message
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType SendRequest(const RequestType request_type,
                                const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * SendResponse
   * @brief SendResponse is used to send a the response for a request
   * @param payload It's an IPayload instance which contains the message
   * information
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType SendResponse(const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * Build
   * @brief Provides the IIpcConnection shared pointer which is a allow the
   * services communication between the Ipc implementation
   * @return IpcConnection shared pointer, it is the Ipc implemetation which is
   * used into the services
   */
  static std::shared_ptr<IIpcConnection> Build();

  /**
   * get_ipc_endpoint
   * @brief Provides the IPC endpoint of the IPC kernel module server
   * @return std::string with the endpoint
   */
  virtual const std::string &get_kernel_endpoint() const noexcept = 0;

  /**
   * set_ipc_ip
   * @brief Sets the ipc_ip for the IPC kernel module
   * @param endpoint the endpoint string
   */
  virtual void set_kernel_endpoint(const std::string &endpoint) = 0;

  /**
   * get_service_id
   * @brief Provides the Service ID for this service
   * @return int greater than -1
   */
  virtual const int get_service_id() const noexcept = 0;

  /**
   * get_type_of_service
   * @brief Provides the service type
   * @return int, service type
   */
  virtual const RequestType get_type_of_service() const noexcept = 0;

  /**
   * set_type_of_service
   * @brief Sets the service type
   * @param type_of_service int, service type
   */
  virtual void set_type_of_service(const RequestType type_of_service) = 0;

  /**
   * set_service_endpoint
   * @brief Sets the service endpoint which allows the service to
   * communicate with the kernel module
   * @param service_endpoint It is a string with the service endpoint
   * connection
   */
  void set_service_endpoint(const std::string &_service_endpoint);

  /**
   * set_ipc_ip
   * @brief Sets the Service ID after the new connection
   * @param service_id the int value with the Service ID after the
   * successed connection
   */
  void set_service_id(int service_id);

  /**
   * SendConnectionRequest
   * @brief SendConnectionRequest is used to send requests related to
   * connections and disconnections
   * @param input_payload It's an IPayload instance which contains the
   * information of the message
   * @param output_payload It's an IPayload instance which will contain the
   * result of the message
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType SendConnectionRequest(
      const std::shared_ptr<IPayload> &input_output,
      const std::shared_ptr<IPayload> &output_payload) = 0;
};

}  // namespace kn
}  // namespace isms
