/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once
#include <list>
#include <memory>

#include "Interfaces.hpp"
#include "IIpcModule.hpp"
#include "Process.hpp"
#include <TimeHandler.hpp>

namespace isms {
namespace kn {

/**
 *
 * IScheduler Class
 * @brief This is the definition class of the Scheduler module which implements
 * the process scheduling. For each process that arrives to the ISMS Kernel, the
 * scheduler is going to handle the output queue for each connected service.
 *
 */
class IScheduler : public TimeHandler {
 public:
  /**
   * RemoveProcessScheduled
   * @brief Remove a process from the scheduler queue
   * @param process Process to be removed
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType RemoveProcessScheduled(
      const std::shared_ptr<Process> &process) = 0;

  /**
   * ScheduleProcess
   * @brief Function to handle the process scheduling into the respectively
   * Service Tree
   * @param process the Process that will be added to the Service Tree
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType ScheduleProcess(
      const std::shared_ptr<Process> &process) = 0;

  /**
   * addServiceQueue
   * @brief Add a new service queue
   * @param sid the SID of the service that will be added
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType AddServiceQueue(int sid) = 0;

  /**
   * removeServiceQueue
   * @brief Remove a service queue
   * @param sid the SID of the service that will be removed
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType RemoveServiceQueue(int sid) = 0;

  /**
   * set_ipc_module
   * @brief Sets the Ipc Module
   * @param ipc_module the Ipc Module shared pointer
   */
  virtual void set_ipc_module(
      const std::shared_ptr<IIpcModule> &ipc_module) = 0;

  /**
   * Build
   * @brief Provides the Scheduler shared pointer which is used to
   * handle the processes scheduling and the services "queues" for the
   * concurrency handling, implemented using a Red-Black Binary Tree
   * @return Scheduler shared pointer
   */
  static std::shared_ptr<IScheduler> Build();

  /**
   * ChangeServiceAvailability
   * @brief This function change the service reception status.
   * @param payload Payload shared pointer, It indicates the availability, and
   * the origin sid, when availability is true, the service is setted as
   * available, if it is false, the service is setted as unavailable
   * @return ErrorType, if it is kSuccess, the process was executed well, if it
   * is another, there is an error.
   */
  virtual ErrorType ChangeServiceAvailability(
      const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * StartScheduling
   * @brief Starts the scheduling process, it will be reviewing the connected
   * services and their availability then the processes will be sent if they are
   * avaiable
   * @return ErrorType, if it is kSuccess, the process was executed well, if it
   * is another, there is an error.
   */
  virtual ErrorType StartScheduling() = 0;

  /**
   * StopScheduling
   * @brief Stops the scheduling process, it will stop sending the processes to
   * the services
   * @return ErrorType, if it is kSuccess, the process was executed well, if it
   * is another, there is an error.
   */
  virtual ErrorType StopScheduling() = 0;

  IScheduler() {}
  virtual ~IScheduler() {}

  /**
   * sendProcess
   * @brief Send a process when a service is available to receive it
   * @param process the Process that will be added sent
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType SendProcess(const std::shared_ptr<Process> &process) = 0;
};

}  // namespace kn
}  // namespace isms
