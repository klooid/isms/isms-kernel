/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */
#pragma once

#include <memory>
#include <set>

#include "Process.hpp"

namespace isms {
namespace kn {

/**
 * ServiceQueue
 * @brief This class is responsible for handling the Service Tree
 * functionalities that handles the Process organization for the Scheduler.
 * It uses a 'set', which is based in a Red-Black Tree structure.
 */
class ServiceTree {
 public:
  ServiceTree();
  virtual ~ServiceTree() {}

  /**
   * GetProcess
   * @brief Returns the most priority process.
   * @return Process shared_ptr
   */
  std::shared_ptr<Process> GetProcess();

  /**
   * InsertProcess
   * @brief Insert new process.
   * @param process Process shared_ptr
   * @return int, Process ID if the insertion was successful / -1 if didn't
   */
  int InsertProcess(const std::shared_ptr<Process> &process);

  /**
   * RemoveProcess
   * @brief Remove the process indicated.
   * @param process Process shared_ptr
   */
  int RemoveProcess(const std::shared_ptr<Process> &process);

  /**
   * get_service_id
   * @brief Provide the ServiceId of the Service Tree
   * @return int, Service ID
   */
  const int get_service_id() const noexcept;

  /**
   * set_service_id
   * @brief Sets the ServiceId of the Service Tree
   * @param sid int, Service ID
   */
  void set_service_id(const int sid);

  /**
   * IsAvailable
   * @brief Provides the requests reception status of a service
   * @return bool, true indicates the service is available, false indicates it
   * is unavailable
   */
  const bool IsAvailable() const noexcept;

  /**
   * set_available
   * @brief Sets the requests reception status of a service
   * @param availability bool, true indicates the service is available, false
   * indicates it is unavailable
   * */
  void ChangeAvailability();

 private:
  int service_id_;
  bool available_;

  static bool PriorityCmq(const std::shared_ptr<Process> &lhs,
                          const std::shared_ptr<Process> &rhs);
  std::set<std::shared_ptr<Process>, decltype(ServiceTree::PriorityCmq) *>
      tree_;
};

inline const bool ServiceTree::IsAvailable() const noexcept {
  return this->available_;
}

inline void ServiceTree::ChangeAvailability() {
  this->available_ = !this->available_;
}

inline void ServiceTree::set_service_id(const int sid) {
  this->service_id_ = sid;
}

inline const int ServiceTree::get_service_id() const noexcept { return this->service_id_; }

}  // namespace kn
}  // namespace isms
