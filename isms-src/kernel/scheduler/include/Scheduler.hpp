/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <thread>

#include "IScheduler.hpp"
#include "ServiceTree.hpp"

namespace isms {
namespace kn {

/**
 *
 * Scheduler Class
 * @brief This is the definition class of the Scheduler module which implements
 * the process scheduling. For each process that arrives to the ISMS Kernel, the
 * scheduler is going to handle the output queue for each connected service.
 *
 */
class Scheduler : public IScheduler {
 public:
  Scheduler() {}
  virtual ~Scheduler() {}

  /**
   * RemoveProcessScheduled
   * @brief Remove a process from the scheduler queue
   * @param process Process to be removed
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType RemoveProcessScheduled(
      const std::shared_ptr<Process> &process) override;

  /**
   * ScheduleProcess
   * @brief Function to handle the process scheduling into the respectively
   * Service Tree
   * @param process the Process that will be added to the Service Tree
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType ScheduleProcess(const std::shared_ptr<Process> &process) override;

  /**
   * addServiceQueue
   * @brief Add a new service queue
   * @param sid the SID of the service that will be added
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType AddServiceQueue(int sid) override;

  /**
   * removeServiceQueue
   * @brief Remove a service queue
   * @param sid the SID of the service that will be removed
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType RemoveServiceQueue(int sid) override;

  /**
   * set_ipc_module
   * @brief Sets the Ipc Module
   * @param ipc_module the Ipc Module shared pointer
   */
  void set_ipc_module(const std::shared_ptr<IIpcModule> &ipc_module) override;

  /**
   * sendProcess
   * @brief Send a process when a service is available to receive it
   * @param process the Process that will be added sent
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType SendProcess(const std::shared_ptr<Process> &process) override;

  /**
   * ChangeServiceAvailability
   * @brief This function change the service reception status.
   * @param payload Payload shared pointer, It indicates the availability, and
   * the origin sid, when availability is true, the service is setted as
   * available, if it is false, the service is setted as unavailable
   * @return ErrorType, if it is kSuccess, the process was executed well, if it
   * is another, there is an error.
   */
  ErrorType ChangeServiceAvailability(
      const std::shared_ptr<IPayload> &payload) override;

  /**
   * StartScheduling
   * @brief Starts the scheduling process, it will be reviewing the connected
   * services and their availability then the processes will be sent if they are
   * avaiable
   * @return ErrorType, if it is kSuccess, the process was executed well, if it
   * is another, there is an error.
   */
  ErrorType StartScheduling() override;

  /**
   * StopScheduling
   * @brief Stops the scheduling process, it will stop sending the processes to
   * the services
   * @return ErrorType, if it is kSuccess, the process was executed well, if it
   * is another, there is an error.
   */
  ErrorType StopScheduling() override;

 private:
  std::vector<ServiceTree> services_queue_;
  std::shared_ptr<IIpcModule> ipc_module_;
  std::thread observer_t_;
  bool running_;

  /**
   * QueuesObserver
   * @brief This function is responsible for review the services queues and send
   * the processes to the respective service if they have processes waiting
   * @return ErrorType, if it is kSuccess, the process was executed well, if it
   * is another, there is an error.
   */
  ErrorType QueuesObserver();

  /**
   * ContainsServiceTree
   * @brief Check if the scheduler contains a Service Tree for an specific
   * Service by its ID
   * @return std::vector<isms::kn::ServiceTree>::iterator, iterator which
   * contains where it is located / vector end if doesn't exists
   */
  std::vector<isms::kn::ServiceTree>::iterator ContainsServiceTree(
      const int sid);
};

inline void Scheduler::set_ipc_module(
    const std::shared_ptr<IIpcModule> &ipc_module) {
  this->ipc_module_ = ipc_module;
}

}  // namespace kn
}  // namespace isms
