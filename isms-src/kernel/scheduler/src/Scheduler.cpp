/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "Scheduler.hpp"

#include "ServiceTree.hpp"

using namespace isms::kn;

ErrorType Scheduler::QueuesObserver() {
  while (this->running_) {
    this->Sleep();
    for (auto &queue : services_queue_) {
      if (queue.IsAvailable()) {
        auto process = queue.GetProcess();
        if (process) {
          this->SendProcess(process);
        }
      }
    }
  }
  return ErrorType::kSuccess;
}

ErrorType Scheduler::StartScheduling() {
  this->running_ = true;
  this->observer_t_ = std::thread(&Scheduler::QueuesObserver, this);
  return ErrorType::kSuccess;
}

ErrorType Scheduler::StopScheduling() {
  this->running_ = false;
  this->observer_t_.join();
  return ErrorType::kSuccess;
}

ErrorType Scheduler::ChangeServiceAvailability(
    const std::shared_ptr<IPayload> &payload) {
  auto conn_payload = IPayload::CastPayload<ConnectionPayload>(payload);

  if (nullptr == conn_payload) return ErrorType::kCannotCast;

  auto sid = conn_payload->get_origin_sid();
  auto availability = conn_payload->get_availability();

  auto serv_tree = this->ContainsServiceTree(sid);

  if (serv_tree == services_queue_.end()) return ErrorType::kServiceNotFound;

  serv_tree->ChangeAvailability();

  return ErrorType::kSuccess;
}

ErrorType Scheduler::RemoveProcessScheduled(
    const std::shared_ptr<Process> &process) {
  if (process) {
    auto serv_tree =
        ContainsServiceTree(process->get_payload()->get_dest_sid());

    if (serv_tree == services_queue_.end()) return ErrorType::kServiceNotFound;

    auto deletion_result = serv_tree->RemoveProcess(process);

    if (deletion_result < 0) return ErrorType::kCannotDelete;

    return ErrorType::kSuccess;
  }
  return ErrorType::kInvalidPointer;
}

ErrorType Scheduler::ScheduleProcess(const std::shared_ptr<Process> &process) {
  if (process->get_payload()->get_request_type() == RequestType::kResponse) {
    return SendProcess(process);
  }

  auto serv_tree = ContainsServiceTree(process->get_payload()->get_dest_sid());

  if (serv_tree == services_queue_.end()) return ErrorType::kServiceNotFound;

  auto insertion_result = serv_tree->InsertProcess(process);

  if (insertion_result < 0) return ErrorType::kCannotCreate;

  if (process->get_state() != ProcessState::kFinished)
    process->set_state(ProcessState::kReady);

  return ErrorType::kSuccess;
}

ErrorType Scheduler::AddServiceQueue(int sid) {
  auto serv_tree = ContainsServiceTree(sid);
  if (serv_tree != services_queue_.end()) return ErrorType::kCannotCreate;

  auto tree = ServiceTree();
  tree.set_service_id(sid);

  services_queue_.push_back(tree);

  return ErrorType::kSuccess;
}

ErrorType Scheduler::RemoveServiceQueue(int sid) {
  auto index = ContainsServiceTree(sid);
  if (index == services_queue_.end()) return ErrorType::kServiceNotFound;

  services_queue_.erase(index);

  return ErrorType::kSuccess;
}

ErrorType Scheduler::SendProcess(const std::shared_ptr<Process> &process) {
  if (process) {
    auto payload = process->get_payload();
    auto send_result = ipc_module_->SendMessageToService(payload);
    if (send_result == ErrorType::kSuccess and
        process->get_state() != ProcessState::kFinished) {
      process->set_state(ProcessState::kRunning);
    }

    return send_result;
  }

  return ErrorType::kInvalidPointer;
}

std::vector<isms::kn::ServiceTree>::iterator Scheduler::ContainsServiceTree(
    const int sid) {
  auto has_id = [sid](ServiceTree tree) {
    return tree.get_service_id() == sid;
  };
  auto serv_tree = std::find_if(std::begin(services_queue_),
                                std::end(services_queue_), has_id);

  return serv_tree;
}
