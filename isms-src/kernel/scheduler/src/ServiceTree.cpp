/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "ServiceTree.hpp"

#include <iostream>

#include "Process.hpp"

using namespace isms::kn;

ServiceTree::ServiceTree() {
  available_ = true;
  tree_ =
      std::set<std::shared_ptr<Process>, decltype(PriorityCmq) *>(PriorityCmq);
}

bool ServiceTree::PriorityCmq(const std::shared_ptr<Process> &lhs,
                              const std::shared_ptr<Process> &rhs) {
  bool comparison_result = false;
  if (lhs && rhs) {
    comparison_result =
        (lhs->get_priority() <= rhs->get_priority()) && (lhs != rhs);
  }
  return comparison_result;
}

std::shared_ptr<Process> ServiceTree::GetProcess() {
  auto process_itr = this->tree_.begin();

  if (process_itr == tree_.end()) return nullptr;
  auto &process = *process_itr;

  tree_.erase(process_itr);
  return process;
}

int ServiceTree::InsertProcess(const std::shared_ptr<Process> &process) {
  if (nullptr == process) return -2;

  auto insertion_result = this->tree_.insert(process);

  if (insertion_result.second) {
    return (*insertion_result.first)->get_pid();
  }

  return -1;
}

int ServiceTree::RemoveProcess(const std::shared_ptr<Process> &process) {
  auto deletion_result = this->tree_.erase(process);

  if (deletion_result == 0) return -1;

  return deletion_result;
}
