/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

/**
 * This file is used to declared the interfaces for the classes used in this
 * project every class shall include this file to avoid compiler error. Then,
 * with the forward declaration, the problem is solved.
 */

#pragma once

namespace isms {
namespace kn {

class IIpcModule;
class IIpcConnection;
class IServiceController;
class IProcessController;
class IScheduler;

}  // namespace kn
}  // namespace isms
