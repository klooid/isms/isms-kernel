/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <chrono>
#include <memory>
#include <IPayload.hpp>
#include <Payload.hpp>

#include <ProcessState.hpp>

namespace isms {
namespace kn {
/**
 * Process
 * @brief This class handle the process instances, it contains the relevant
 * information for created process.
 */
class Process {
 public:
  Process() : pid_{-1}, parent_{nullptr}, origin_sid_{-1}, current_sid_{-1} {}
  virtual ~Process() {}
  /* Getters and Setters */

  /**
   * get_pid
   * @brief Provides the Process ID
   * @return int, Process ID
   */
  const int get_pid() const noexcept;

  /**
   * get_origin_sid
   * @brief Provides the origin service ID
   * @return int, origin service ID
   */
  const int get_origin_sid() const noexcept;

  /**
   * get_current_sid
   * @brief Provides the current service ID where the process is running
   * @return int, current serivice ID
   */
  const int get_current_sid() const noexcept;

  /**
   * get_priority
   * @brief Provides the current service ID
   * @return double, priority
   */
  const float get_priority() const noexcept;

  /**
   * get_start_time
   * @brief Provides the time when the process was created
   * @return time_point, start time
   */
  const std::chrono::time_point<std::chrono::system_clock> &get_start_time()
      const noexcept;

  /**
   * get_payload
   * @brief Provides the process payload
   * @return IPayload, Payload interface
   */
  const std::shared_ptr<IPayload> &get_payload() const noexcept;

  /**
   * get_state
   * @brief Provides the process state which could be:
   *    - newp : new process
   *    - ready : the process can be sent
   *    - blocked : the process is blocked waiting for an event to continue
   *    - running : the process was sent and is running
   *    - finished : the process have finished
   * @return ProcessState
   */
  const ProcessState get_state() const noexcept;

  /**
   * get_parent_pid
   * @brief Provide the process parent pid when the process is a subprocess
   * @return int, Process Parent ID
   */
  const std::shared_ptr<Process> &get_parent() const noexcept;

  /**
   * set_pid
   * @brief Sets the Process ID
   * @param pid Process ID
   */
  void set_pid(const int pid);

  /**
   * set_origin_sid
   * @brief Sets the origin service ID that request the process
   * @param sid origin service ID
   */
  void set_origin_sid(const int sid);

  /**
   * set_current_sid
   * @brief Sets the service id where the process is running
   * @param sid, int current service ID
   */
  void set_current_sid(const int sid);

  /**
   * set_priority
   * @brief Sets the process priority
   * @param priority, int priority
   */
  void set_priority(const float priority);

  /**
   * set_start_time
   * @brief Sets the time when the process was created
   * @param timestamp, time_t start time
   */
  void set_start_time(
      const std::chrono::time_point<std::chrono::system_clock> &timestamp);

  /**
   * set_payload
   * @brief Sets the process Payload that comes from the owner service
   * @param payload, IPayload
   */
  void set_payload(const std::shared_ptr<IPayload> &payload);

  /**
   * set_state
   * @brief Sets the process state which could be:
   *    - newp : new process
   *    - ready : the process can be sent
   *    - blocked : the process is blocked waiting for an event to continue
   *    - running : the process was sent and is running
   *    - finished : the process have finished
   * @param state, ProcessState
   */
  void set_state(ProcessState state);

  /**
   * set_parent_pid
   * @brief Sets the process parent pid when the process is a subprocess
   * @param parent_pid, int, Process Parent ID
   */
  void set_parent(const std::shared_ptr<Process> &parent);

 private:
  int pid_;
  std::shared_ptr<Process> parent_;
  int origin_sid_;
  int current_sid_;
  float priority_;
  std::chrono::time_point<std::chrono::system_clock> start_time_;
  std::shared_ptr<IPayload> payload_;
  ProcessState state_;
};

inline const int Process::get_pid() const noexcept { return this->pid_; }

inline const int Process::get_origin_sid() const noexcept {
  return this->origin_sid_;
}

inline const int Process::get_current_sid() const noexcept {
  return this->current_sid_;
}

inline const float Process::get_priority() const noexcept {
  return this->priority_;
}

inline const ProcessState Process::get_state() const noexcept {
  return this->state_;
}

inline const std::chrono::time_point<std::chrono::system_clock>
    &Process::get_start_time() const noexcept {
  return this->start_time_;
}

inline const std::shared_ptr<IPayload> &Process::get_payload() const noexcept {
  return this->payload_;
}

inline const std::shared_ptr<Process> &Process::get_parent() const noexcept {
  return this->parent_;
}

inline void Process::set_pid(const int pid) { this->pid_ = pid; }

inline void Process::set_origin_sid(const int sid) { this->origin_sid_ = sid; }

inline void Process::set_current_sid(const int sid) {
  this->current_sid_ = sid;
}

inline void Process::set_priority(const float priority) {
  this->priority_ = priority;
}

inline void Process::set_state(ProcessState state) { this->state_ = state; }

inline void Process::set_start_time(
    const std::chrono::time_point<std::chrono::system_clock> &timestamp) {
  this->start_time_ = timestamp;
}

inline void Process::set_payload(const std::shared_ptr<IPayload> &payload) {
  this->payload_ = payload;
}

inline void Process::set_parent(const std::shared_ptr<Process> &parent) {
  this->parent_ = parent;
}
}  // namespace kn
}  // namespace isms
