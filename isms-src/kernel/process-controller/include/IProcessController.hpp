/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <memory>
#include <unordered_map>

#include "Interfaces.hpp"
#include "Process.hpp"
#include <IPayload.hpp>
#include "IScheduler.hpp"
#include "IServiceController.hpp"
#include <ErrorType.hpp>

namespace isms {
namespace kn {

/**
 * IProcessController
 * @brief This class handle each process created information, controls the
 * processes administration functions such as, creation, deletetion, priority
 * assigment, provide the processes information, etc.
 */
class IProcessController {
 public:
  IProcessController() {}
  virtual ~IProcessController() {}

  /**
   * showCurrentProcesses
   * @brief Provides the current processes information
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType ShowCurrentProcesses(
      const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * addProcessProcess
   * @brief This functions add a process to system, it could create a new if it
   * doesn't exists or could redirect the process execution if does
   * @param payload Payload with the process information
   * @return integer with PID of the process / -1 if an error ocurrs
   */
  virtual int AddProcess(const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * killProcess
   * @brief kill current process execution/scheduling
   * @param pid PID of the process
   * @return PID of the process / -1 if an error ocurrs
   */
  virtual int KillProcess(int pid) = 0;

  /**
   * stopProcess
   * @brief stop current process execution/scheduling
   * @param pid PID of the process
   * @return PID of the process / -1 if an error ocurrs
   */
  virtual int StopProcess(int pid) = 0;

  /**
   * Build
   * @brief Provides the ProcessController shared pointer which is used to
   * handle the processes management
   * @return ProcessController shared pointer
   */
  static std::shared_ptr<IProcessController> Build();

  /**
   * set_service_controller
   * @brief Sets the ServiceController object which handles the services
   * information, the ProcessController use the ServiceController to get
   * information about the  origin and destination of the processes
   * @param s_controller ServiceController shared pointer used to get services
   * information
   */
  virtual void set_service_controller(
      const std::shared_ptr<IServiceController> &s_controller) = 0;

  /**
   * set_scheduler
   * @brief Sets the Scheduler object which handles the process scheduling, the
   * ProcessController send the processes to Scheduler when they are ready to be
   * sent
   * @param scheduler IScheduler shared pointer used for process scheduling
   */
  virtual void set_scheduler(const std::shared_ptr<IScheduler> &scheduler) = 0;

  /**
   * setPriority
   * @brief set the priority for a process
   * @param process Process created
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType SetPriority(const std::shared_ptr<Process> &process) = 0;

  /**
   * AddProcessToScheduler
   * @brief Send a created process to the Scheduler
   * @param process Process created
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType AddProcessToScheduler(
      const std::shared_ptr<Process> &process) = 0;

  /**
   * GetProcessById
   * @brief Returns a process based on the process ID provided
   * @param pid PID of the process
   * @param process_output Process shared pointer to be assigned
   * @return ErrorType, kSuccess if the process is found, any other means a
   * problem
   */
  virtual ErrorType GetProcessById(
      const int pid, std::shared_ptr<Process> &process_output) = 0;
  
  /**
   * PrintKernelStatus
   * @brief Prints the current processes information in the kernel
   */
  virtual void PrintKernelStatus() = 0;
};

}  // namespace kn
}  // namespace isms
