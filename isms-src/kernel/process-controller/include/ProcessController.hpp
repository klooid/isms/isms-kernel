/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include "IProcessController.hpp"

namespace isms {
namespace kn {

class Scheduler;

/**
 * ProcessController
 * @brief This class handle each process created information, controls the
 * processes administration functions such as, creation, deletetion, priority
 * assigment, provide the processes information, etc.
 */
class ProcessController : public IProcessController {
 public:
  ProcessController();
  virtual ~ProcessController() {}

  /**
   * showCurrentProcesses
   * @brief return the current processes information
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType ShowCurrentProcesses(
      const std::shared_ptr<IPayload> &payload) override;

  /**
   * AddProcess
   * @brief Add the process to the process controllers, update it if exists
   * @param payload Payload with the request information
   * @return integer with PID of the process / -1 if an error ocurrs
   */
  int AddProcess(const std::shared_ptr<IPayload> &payload) override;

  /**
   * killProcess
   * @brief kill current process execution/scheduling
   * @param pid PID of the process
   * @return PID of the process / -1 if an error ocurrs
   */
  int KillProcess(int pid) override;

  /**
   * stopProcess
   * @brief stop current process execution/scheduling
   * @param pid PID of the process
   * @return PID of the process / -1 if an error ocurrs
   */
  int StopProcess(int pid) override;

  /**
   * set_service_controller
   * @brief Sets the ServiceController object which handles the services
   * information, the ProcessController use the ServiceController to get
   * information about the  origin and destination of the processes
   * @param s_controller ServiceController shared pointe used to get services
   * information
   */
  void set_service_controller(
      const std::shared_ptr<IServiceController> &s_controller) override;

  /**
   * set_scheduler
   * @brief Sets the Scheduler object which handles the process scheduling, the
   * ProcessController send the processes to Scheduler when they are ready to be
   * sent
   * @param scheduler IScheduler shared pointe used for process scheduling
   */
  void set_scheduler(const std::shared_ptr<IScheduler> &scheduler) override;

  /**
   * GetProcessById
   * @brief Returns a process based on the process ID provided
   * @param pid PID of the process
   * @param process_output Process shared pointer to be assigned
   * @return ErrorType, kSuccess if the process is found, any other means a
   * problem
   */
  ErrorType GetProcessById(const int pid,
                           std::shared_ptr<Process> &process_output) override;

  /**
   * PrintKernelStatus
   * @brief Prints the current processes information in the kernel
   */
  void PrintKernelStatus() override;

 private:
  int current_pid_;
  std::unordered_map<int, std::shared_ptr<Process>> processes_;
  std::shared_ptr<IScheduler> scheduler_;
  std::shared_ptr<IServiceController> service_controller_;

  /**
   * setPriority
   * @brief set the priority for a process
   * @param process Process created
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType SetPriority(const std::shared_ptr<Process> &process) override;

  /**
   * scheduleProcess
   * @brief Send a created process to the Scheduler
   * @param process Process created
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType AddProcessToScheduler(
      const std::shared_ptr<Process> &process) override;

  /**
   * CreateProcess
   * @brief Create new process
   * @param payload Payload with the request information
   * @return integer with the PID of the new process / -1 if an error occurs
   */
  std::shared_ptr<Process> CreateProcess(
      const std::shared_ptr<IPayload> &payload);
};

inline void ProcessController::set_service_controller(
    const std::shared_ptr<IServiceController> &s_controller) {
  this->service_controller_ = s_controller;
}
inline void ProcessController::set_scheduler(
    const std::shared_ptr<IScheduler> &scheduler) {
  this->scheduler_ = scheduler;
}

}  // namespace kn
}  // namespace isms
