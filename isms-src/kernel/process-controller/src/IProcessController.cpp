/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "IProcessController.hpp"

#include "ProcessController.hpp"

using namespace isms::kn;

std::shared_ptr<IProcessController> IProcessController::Build() {
  return std::make_shared<ProcessController>();
}
