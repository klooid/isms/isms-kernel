/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "ProcessController.hpp"

#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <thread>

#include "ProcessState.hpp"
#include "RequestType.hpp"

using namespace isms::kn;

ProcessController::ProcessController() { this->current_pid_ = 0; }

ErrorType ProcessController::ShowCurrentProcesses(
    const std::shared_ptr<IPayload> &payload) {
  return ErrorType::kSuccess;
}

int ProcessController::KillProcess(int pid) { return 0; }

int ProcessController::StopProcess(int pid) { return 0; }

ErrorType ProcessController::GetProcessById(
    const int pid, std::shared_ptr<Process> &process_output) {
  auto process = this->processes_.find(pid);
  if (process == processes_.end()) return ErrorType::kNotFound;

  process_output = process->second;

  return ErrorType::kSuccess;
}

int ProcessController::AddProcess(const std::shared_ptr<IPayload> &payload) {
  /* Check if the payload is valid */
  if (nullptr == payload.get()) return -3;

  int result_pid = -1;

  /* Check if the request comes from existing process */
  if (payload->get_pid() >= 0) {
    std::shared_ptr<Process> input_process;
    GetProcessById(payload->get_pid(), input_process);

    /* Check if the comming request is a subproces
        which means it has a parent process */
    if (payload->IsSubprocess()) {
      /* Create the new (child)process */
      auto sub_process = CreateProcess(payload);
      /* Set the (parent)process as blocked */
      input_process->set_state(ProcessState::kBlocked);
      /* Scheduling the new (child)process */
      AddProcessToScheduler(sub_process);

      result_pid = sub_process->get_pid();
    }
    /* Checking if the process is a response */
    else if (payload->get_request_type() == RequestType::kResponse) {
      auto parent_process = input_process->get_parent();
      /* Check the (parent)process*/
      if (parent_process) {
        payload->set_pid(parent_process->get_pid());
        payload->set_origin_sid(parent_process->get_origin_sid());
        /* Adding the response message to the parent process */
        parent_process->set_payload(payload);
        SetPriority(parent_process);
        AddProcessToScheduler(parent_process);

        result_pid = parent_process->get_pid();
      } else {
        input_process->set_payload(payload);
        AddProcessToScheduler(input_process);

        result_pid = input_process->get_pid();
      }
      /* Set the current process as finished */
      input_process->set_state(ProcessState::kFinished);
    }
  } /* It is a request for a new process */
  else {
    auto new_process = CreateProcess(payload);
    if (new_process) {
      auto schedule_result = this->AddProcessToScheduler(new_process);

      if (schedule_result == ErrorType::kSuccess)
        result_pid = new_process->get_pid();
    }
  }
  this->PrintKernelStatus();

  return result_pid;
}

ErrorType ProcessController::AddProcessToScheduler(
    const std::shared_ptr<Process> &process) {
  auto schedule_result = scheduler_->ScheduleProcess(process);
  if (schedule_result != ErrorType::kSuccess) {
    return schedule_result;
  }

  return ErrorType::kSuccess;
}

ErrorType ProcessController::SetPriority(
    const std::shared_ptr<Process> &process) {
  /* Get the current duration time of the process */
  if (process->get_payload()->get_request_type() == RequestType::kResponse) {
    process->set_priority(0);
    return ErrorType::kSuccess;
  }

  auto current_time = std::chrono::system_clock::now();
  auto start_time = process->get_start_time();

  auto duration =
      static_cast<int>(std::chrono::duration_cast<std::chrono::nanoseconds>(
                           current_time - start_time)
                           .count());

  /* Retrieve the origin service to get the requests priority*/
  auto service = std::make_shared<Service>();
  auto result = service_controller_->ReturnServiceInfo(
      process->get_origin_sid(), service);
  if (result != ErrorType::kSuccess) return result;

  auto req_priority = static_cast<float>(service->get_service_type());

  /* Compute the priority */
  auto priority = (duration) / req_priority;

  process->set_priority(priority);

  return ErrorType::kSuccess;
}

std::shared_ptr<Process> ProcessController::CreateProcess(
    const std::shared_ptr<IPayload> &payload) {
  auto process_new = std::make_shared<Process>();

  process_new->set_start_time(std::chrono::system_clock::now());
  process_new->set_pid(this->current_pid_);
  process_new->set_origin_sid(payload->get_origin_sid());
  process_new->set_state(ProcessState::kNewProcess);
  process_new->set_payload(payload);
  process_new->set_current_sid(payload->get_dest_sid());

  if (payload->IsSubprocess()) {
    std::shared_ptr<Process> parent_process;
    GetProcessById(payload->get_pid(), parent_process);
    if (parent_process)
      process_new->set_parent(parent_process);
    else
      return nullptr;
  }
  process_new->set_start_time(std::chrono::system_clock::now());

  payload->set_pid(process_new->get_pid());

  auto priority_result = SetPriority(process_new);
  if (priority_result != ErrorType::kSuccess) {
    return nullptr;
  }

  this->processes_.insert(std::make_pair(this->current_pid_, process_new));

  ++(this->current_pid_);

  return process_new;
}

void ProcessController::PrintKernelStatus() {
  const char separator = ' ';
  const char pipe = '|';
  const int width = 8;
  std::system("clear");
  std::cout << std::endl;
  std::cout << std::left << std::setw(width) << std::setfill(separator) << "PID"
            << pipe;
  std::cout << std::left << std::setw(width + 12) << std::setfill(separator)
            << "Parent" << pipe;
  std::cout << std::left << std::setw(width) << std::setfill(separator)
            << "OSID" << pipe;
  std::cout << std::left << std::setw(width) << std::setfill(separator)
            << "DSID" << pipe;
  std::cout << std::left << std::setw(width) << std::setfill(separator)
            << "Priority" << pipe;
  std::cout << std::left << std::setw(width) << std::setfill(separator)
            << "S Time" << pipe;
  std::cout << std::left << std::setw(width) << std::setfill(separator)
            << "State" << pipe;
  std::cout << std::endl;

  for (auto &process : this->processes_) {
    std::time_t t =
        std::chrono::system_clock::to_time_t(process.second->get_start_time());
    char buf[20];
    strftime(buf, 20, "%H:%M:%S", localtime(&t));
    std::string time_str{buf};
    auto state_cast = static_cast<int>(process.second->get_state());
    std::cout << std::left << std::setw(width) << std::setfill(separator)
              << process.second->get_pid() << pipe;
    std::cout << std::left << std::setw(width + 12) << std::setfill(separator)
              << process.second->get_parent() << pipe;
    std::cout << std::left << std::setw(width) << std::setfill(separator)
              << process.second->get_origin_sid() << pipe;
    std::cout << std::left << std::setw(width) << std::setfill(separator)
              << process.second->get_current_sid() << pipe;
    std::cout << std::left << std::setw(width) << std::setfill(separator)
              << process.second->get_priority() << pipe;
    std::cout << std::left << std::setw(width) << std::setfill(separator)
              << time_str << pipe;
    std::cout << std::left << std::setw(width) << std::setfill(separator)
              << state_cast << pipe;
    std::cout << std::endl;
  }
}
