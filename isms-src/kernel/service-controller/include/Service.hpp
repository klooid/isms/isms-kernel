/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <ctime>
#include <string>

#include <RequestType.hpp>
#include <ServiceState.hpp>

namespace isms {
namespace kn {
/**
 * Service
 * @brief This class handle the Service instance which conatains each relevant
 * data for a service connected to the system.
 */
class Service {
 public:
  /**
   * get_sid
   * @brief Provides the Service ID
   * @return int, Service ID
   */
  const int get_sid() const noexcept;

  /**
   * get_start_time
   * @brief Provides the time when the service was created
   * @return time_t, start time
   */
  const std::time_t &get_start_time() const noexcept;

  /**
   * getState
   * @brief Provides the current state of the service, such as:
   *    - kRun
   *    - kStop
   *    - kDead
   * @return ServiceState, state
   */
  const ServiceState get_state() const noexcept;

  /**
   * get_service_type
   * @brief Provides the service type, such as:
   *    - kCon : connect
   *    - kDcon : disconnect
   *    - kComm: communication
   *    - kProc: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @return string, service type
   */
  const RequestType get_service_type() const noexcept;

  /**
   * get_endpoint
   * @brief Provides the service endpoint which is used for the communication
   * @return string, endpoint
   */
  const std::string &get_endpoint() const noexcept;

  /**
   * set_sid
   * @brief Sets the Service ID
   * @param sid, int Service ID
   */
  void set_sid(const int sid);

  /**
   * set_start_time
   * @brief Sets the time when the process was created
   * @param timestamp, time_t start time
   */
  void set_start_time(const std::time_t &timestamp);

  /**
   * set_state
   * @brief Sets the service state, such as:
   *    - e, running
   *    - s, stopped
   *    - d, dead
   * @param state char, service state
   */
  void set_state(ServiceState state);

  /**
   * set_service_type
   * @brief Sets the service type, such as:
   *    - kCon : connect
   *    - kDcon : disconnect
   *    - kComm: communication
   *    - kProc: process execution
   *    - kDb: databases
   *    - kSp: storage and permissions
   *    - kCm: custom service
   * @param service_type string, service type
   */
  void set_service_type(const RequestType service_type);

  /**
   * set_endpoint
   * @brief Sets the endpoint where the service is connected and could be
   * accessed
   * @param endpoint string, endpoint
   */
  void set_endpoint(const std::string &endpoint);

  Service() {}
  virtual ~Service() {}

 private:
  int sid_;
  std::time_t start_time_;
  ServiceState state_;
  RequestType service_type_;
  std::string endpoint_;
};

inline const int Service::get_sid() const noexcept { return this->sid_; }

inline const std::time_t &Service::get_start_time() const noexcept {
  return this->start_time_;
}

inline const ServiceState Service::get_state() const noexcept {
  return this->state_;
}

inline const RequestType Service::get_service_type() const noexcept {
  return this->service_type_;
}

inline const std::string &Service::get_endpoint() const noexcept {
  return this->endpoint_;
}

inline void Service::set_endpoint(const std::string &path_name) {
  this->endpoint_ = path_name;
}

inline void Service::set_sid(const int sid) { this->sid_ = sid; }

inline void Service::set_start_time(const std::time_t &timestamp) {
  this->start_time_ = timestamp;
}

inline void Service::set_state(const ServiceState state) {
  this->state_ = state;
}

inline void Service::set_service_type(const RequestType service_type) {
  this->service_type_ = service_type;
}
}  // namespace kn
}  // namespace isms
