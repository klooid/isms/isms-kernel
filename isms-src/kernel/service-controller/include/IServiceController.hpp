/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <memory>

#include "Interfaces.hpp"
#include "Service.hpp"
#include <IPayload.hpp>
#include <ErrorType.hpp>
#include <RequestType.hpp>
#include "IScheduler.hpp"

namespace isms {
namespace kn {
/**
 * IServiceController
 * @brief This class handles each service information which is connected to the
 * ISMS Kernel, it controls the services administration functions such as, the
 * services creation, deletion, provide the services information, etc.
 */
class IServiceController {
 public:
  /**
   * AddService
   * @brief Add new service to the current services list
   * @param payload Payload with the new service information to be connected
   * @return int with the SID of the service / -1 if an error ocurrs
   */
  virtual int AddService(const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * DeleteService
   * @brief Delete a service from current services list
   * @param sid integer with the SID of the service that will be removed
   * @return int with the SID of the service / -1 if an error ocurrs
   */
  virtual int DeleteService(const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * FindServiceId
   * @brief Provides the service Id for an specific service by its type tag
   * @param request_type char with the type of the service that is requested
   * @return int, service id
   */
  virtual const int FindServiceId(const RequestType request_type) const = 0;

  /**
   * returnServiceInfo
   * @brief return the service information for an specific service type
   * @param sid int with the Service ID that is requested
   * @param output_service Service shared ptr which will store the information
   * of the found service
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType ReturnServiceInfo(
      const int sid, std::shared_ptr<Service> &output_service) = 0;

  /**
   * ShowCurrentServices
   * @brief return the current services information
   * @param payload Payload that will store the services information
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  virtual ErrorType ShowCurrentServices(
      const std::shared_ptr<IPayload> &payload) = 0;

  /**
   * set_scheduler
   * @brief Sets the scheduler module which will be used to add and remove
   * service trees
   * @param scheduler Scheduler which will be used to add and remove services
   */
  virtual void set_scheduler(const std::shared_ptr<IScheduler> &scheduler) = 0;

  /**
   * Build
   * @brief Provides the ServicesController shared pointer which is used to
   * handle the services management, its connections, disconnections, and
   * information
   * @return ProcessController shared pointer
   */
  static std::shared_ptr<IServiceController> Build();

  IServiceController() {}
  virtual ~IServiceController() {}

  /**
   * CheckService
   * @brief Check the service information to validate its connection request
   * @param payload Payload with the new service information to be connected
   * @return bool / true if the service is authenticated / false if not
   */
  virtual bool CheckService(const std::shared_ptr<IPayload> &payload) = 0;
};
}  // namespace kn
}  // namespace isms
