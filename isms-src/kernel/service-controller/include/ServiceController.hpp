/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <memory>
#include <vector>

#include "IServiceController.hpp"

namespace isms {
namespace kn {
/**
 * ServiceController
 * @brief This class handles each service information which is connected to the
 * ISMS Kernel Controls the services administration functions such as, the
 * services creation, deletion, provide the services information, etc.
 */
class ServiceController : public IServiceController {
 public:
  /**
   * AddService
   * @brief Add new service to the current services list
   * @param payload Payload with the new service information to be connected
   * @return int with the SID of the service / -1 if an error ocurrs
   */
  int AddService(const std::shared_ptr<IPayload> &payload) override;

  /**
   * DeleteService
   * @brief Delete a service from current services list
   * @param sid integer with the SID of the service that will be removed
   * @return int with the SID of the service / -1 if an error ocurrs
   */
  int DeleteService(const std::shared_ptr<IPayload> &payload) override;

  /**
   * FindServiceId
   * @brief return the service Id for an specific service by its type tag
   * @param request_type char with the type of the service that is requested
   * @return int, service id
   */
  const int FindServiceId(const RequestType request_type) const override;

  /**
   * returnServiceInfo
   * @brief return the service information for an specific service type
   * @param sid int with the Service ID that is requested
   * @param output_service Service shared ptr which will store the information
   * of the found service
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType ReturnServiceInfo(
      const int sid, std::shared_ptr<Service> &output_service) override;

  /**
   * ShowCurrentServices
   * @brief return the current services information
   * @param payload Payload that will store the services information
   * @return ErrorType, kSuccess if the process was ok, any other means a
   * problem
   */
  ErrorType ShowCurrentServices(
      const std::shared_ptr<IPayload> &payload) override;

  ServiceController();
  virtual ~ServiceController() {}

  /**
   * CheckService
   * @brief Check the service information to validate its connection request
   * @param payload Payload with the new service information to be connected
   * @return bool / true if the service is authenticated / false if not
   */
  bool CheckService(const std::shared_ptr<IPayload> &payload) override;

  /**
   * set_scheduler
   * @brief Sets the scheduler module which will be used to add and remove
   * service trees
   * @param scheduler Scheduler which will be used to add and remove services
   */
  void set_scheduler(const std::shared_ptr<IScheduler> &scheduler) override;

 private:
  std::vector<std::shared_ptr<Service>> services_;
  int current_sid_;
  std::shared_ptr<IScheduler> scheduler_;

  /**
   * IsServiceAvailable
   * @brief Checks the service availability, if the requested service type is
   * already in use, or the authentication parameters are not verified, the
   * requested is rejected
   * @return bool, true if is available, false if not
   */
  bool IsServiceAvailable(const std::shared_ptr<IPayload> &payload);

  /**
   * @brief Check if there is a service that handle the RequestType by the type
   *
   * @param service_type RequestType, to check the type of service
   * @return std::vector<std::shared_ptr<Service>>::const_iterator with the
   * service if there is, the iterator will be as same as the services
   * vector ends, if there is not a service
   */
  std::vector<std::shared_ptr<Service>>::const_iterator IsServiceType(
      const RequestType service_type) const;
};

inline void ServiceController::set_scheduler(
    const std::shared_ptr<IScheduler> &scheduler) {
  this->scheduler_ = scheduler;
}

}  // namespace kn
}  // namespace isms
