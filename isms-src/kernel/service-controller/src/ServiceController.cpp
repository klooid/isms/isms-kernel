/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "ServiceController.hpp"

#include <ctime>
#include <iostream>
#include <memory>
#include <ConnectionPayload.hpp>
#include <string>

#include <ServiceState.hpp>

using namespace isms::kn;

const std::string kTempUser{"user"};
const std::string kTempPassword{"password"};
const std::string kEndpointPreffix{"service_"};

ServiceController::ServiceController() { this->current_sid_ = 0; }

int ServiceController::AddService(const std::shared_ptr<IPayload> &payload) {
  auto checked = this->IsServiceAvailable(payload);

  if (!checked) return -2;

  /* Assign the endpoint for the service */

  int sid_temp = current_sid_;
  std::string endpoint_temp = kEndpointPreffix + std::to_string(sid_temp);

  auto connection_payload = IPayload::CastPayload<ConnectionPayload>(payload);
  /* Set the parameters if the cast was successful*/
  if (connection_payload) {
    connection_payload->set_endpoint(endpoint_temp);
    connection_payload->set_origin_sid(sid_temp);
  } else {
    std::cout << "It is not inheriting properly" << std::endl;
    return -3;
  }

  /* Create the new service */
  auto service_new = std::make_shared<Service>();

  auto start_time = std::time(nullptr);
  auto service_type = connection_payload->get_type_of_service();
  service_new->set_endpoint(endpoint_temp);
  service_new->set_service_type(service_type);
  service_new->set_sid(sid_temp);
  service_new->set_start_time(start_time);
  service_new->set_state(ServiceState::kRun);

  /* Adding the service to the services list */
  services_.push_back(service_new);

  /* Create the service tree queue into the scheduler */
  auto insert_result =
      this->scheduler_->AddServiceQueue(service_new->get_sid());

  if (insert_result != ErrorType::kSuccess) {
    services_.erase(services_.begin());

    return -1;
  }

  /* Increment the current index */
  current_sid_++;

  return service_new->get_sid();
}

bool ServiceController::IsServiceAvailable(
    const std::shared_ptr<IPayload> &payload) {
  if (CheckService(payload)) {
    auto connection_payload = IPayload::CastPayload<ConnectionPayload>(payload);

    auto service_found =
        IsServiceType(connection_payload->get_type_of_service());
    if (service_found != services_.end()) return false;

  } else
    return false;

  return true;
}

std::vector<std::shared_ptr<Service>>::const_iterator
ServiceController::IsServiceType(const RequestType service_type) const {
  auto check_service_type =
      [service_type](std::shared_ptr<isms::kn::Service> service) {
        return service->get_service_type() == service_type;
      };
  auto service_found =
      std::find_if(std::begin(this->services_), std::end(this->services_),
                   check_service_type);
  return service_found;
}

bool ServiceController::CheckService(const std::shared_ptr<IPayload> &payload) {
  /* This is a temporal implementation for the service checking, it will be
   * implemented in the Resadmin later */
  if (0 == payload->get_user().compare(kTempUser) &&
      0 == payload->get_password().compare(kTempPassword)) {
    return true;
  }

  return false;
}

const int ServiceController::FindServiceId(
    const RequestType request_type) const {
  /* Temp string to check the service type */
  RequestType type_temp;

  auto service_found = IsServiceType(request_type);
  if (service_found == services_.end()) return -1;

  return service_found->get()->get_sid();
}

ErrorType ServiceController::ReturnServiceInfo(
    const int sid, std::shared_ptr<Service> &output_service) {
  int sid_temp;

  auto check_service_id = [sid](std::shared_ptr<isms::kn::Service> service) {
    return service->get_sid() == sid;
  };
  auto service_found = std::find_if(
      std::begin(this->services_), std::end(this->services_), check_service_id);

  if (service_found != services_.end()) {
    output_service = *service_found;
    return ErrorType::kSuccess;
  }

  return ErrorType::kNotFound;
}

int ServiceController::DeleteService(const std::shared_ptr<IPayload> &payload) {
  auto validated = this->CheckService(payload);

  if (!validated) return -2;

  auto sid = payload->get_origin_sid();

  /* Set the iterator */
  auto itr = services_.begin();
  int sid_temp;
  int index = 0;

  for (auto &service : services_) {
    sid_temp = service->get_sid();

    if (sid == sid_temp) {
      auto value_to_delete = itr + index;
      auto remove_result = this->scheduler_->RemoveServiceQueue(sid_temp);
      if (remove_result != ErrorType::kSuccess) return -2;
      services_.erase(value_to_delete);
      return sid_temp;
    }
    index++;
  }
  /* Return -1 as not found service result */
  return -1;
}

ErrorType ServiceController::ShowCurrentServices(
    const std::shared_ptr<IPayload> &payload) {
  return ErrorType::kSuccess;
}
