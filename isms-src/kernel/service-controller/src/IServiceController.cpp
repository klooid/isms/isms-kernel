/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include "IServiceController.hpp"

#include "ServiceController.hpp"

using namespace isms::kn;

std::shared_ptr<IServiceController> IServiceController::Build() {
  return std::make_shared<ServiceController>();
}
