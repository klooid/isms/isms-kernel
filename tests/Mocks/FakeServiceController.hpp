/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <ConnectionPayload.hpp>
#include <IPayload.hpp>
#include <Service.hpp>
#include <ErrorType.hpp>
#include <vector>

#include <IServiceController.hpp>

using namespace isms::kn;

class FakeServiceController : public IServiceController {
 public:
  int AddService(const std::shared_ptr<IPayload> &payload) override {
    auto service = Service();

    auto payload_test = IPayload::CastPayload<ConnectionPayload>(payload);

    service.set_sid(service_id_);
    service.set_service_type(payload_test->get_type_of_service());
    service.set_endpoint("test.ipc");

    services_.push_back(service);
    ++service_id_;

    return service.get_sid();
  }

  int DeleteService(const std::shared_ptr<IPayload> &payload) override {
    auto index = 0;
    for (auto ser : services_) {
      if (ser.get_sid() == payload->get_origin_sid()) {
        services_.erase(services_.begin() + index);
        return payload->get_origin_sid();
      }
      ++index;
    }
    return -1;
  }

  ErrorType ReturnServiceInfo(
      int sid, std::shared_ptr<Service> &output_service) override {
    for (auto serv : services_) {
      if (serv.get_sid() == sid) {
        *output_service = serv;
        return ErrorType::kSuccess;
      }
    }
    return ErrorType::kNotFound;
  }

  const int FindServiceId(const RequestType request_type) const override {
    for (auto serv : services_) {
      if (serv.get_service_type() == request_type) {
        return serv.get_sid();
      }
    }
    return -1;
  }

  MOCK_METHOD(ErrorType, ShowCurrentServices,
              (const std::shared_ptr<IPayload> &payload), (override));

  MOCK_METHOD(void, set_scheduler,
              (const std::shared_ptr<IScheduler> &scheduler), (override));

  FakeServiceController() {
    auto conn_pay =
        IPayload::CastPayload<ConnectionPayload>(IPayload::Create());
    conn_pay->set_type_of_service(RequestType::kComm);

    this->AddService(conn_pay);
    conn_pay->set_type_of_service(RequestType::kDb);

    this->AddService(conn_pay);
    conn_pay->set_type_of_service(RequestType::kProc);

    this->AddService(conn_pay);
  }

 private:
  int service_id_;
  std::vector<Service> services_;

  MOCK_METHOD(bool, CheckService, (const std::shared_ptr<IPayload> &payload),
              (override));
};
