/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <IPayload.hpp>
#include <ErrorType.hpp>

#include "FakeServiceController.hpp"
#include <IServiceController.hpp>

using namespace isms::kn;

class MockServiceController : public IServiceController {
 public:
  MOCK_METHOD(int, AddService, (const std::shared_ptr<IPayload> &payload),
              (override));

  MOCK_METHOD(int, DeleteService, (const std::shared_ptr<IPayload> &payload),
              (override));

  MOCK_METHOD(ErrorType, ReturnServiceInfo,
              (const int sid, std::shared_ptr<Service> &output_service),
              (override));

  MOCK_METHOD(const int, FindServiceId, (const RequestType request_type),
              (const override));

  MOCK_METHOD(ErrorType, ShowCurrentServices,
              (const std::shared_ptr<IPayload> &payload), (override));

  MOCK_METHOD(void, set_scheduler,
              (const std::shared_ptr<IScheduler> &scheduler), (override));

  void DelegateToFake() {
    ON_CALL(*this, AddService)
        .WillByDefault([this](const std::shared_ptr<IPayload> &payload) {
          return fake_.AddService(payload);
        });
    ON_CALL(*this, ReturnServiceInfo)
        .WillByDefault(
            [this](const int sid, std::shared_ptr<Service> &output_service) {
              return fake_.ReturnServiceInfo(sid, output_service);
            });
    ON_CALL(*this, DeleteService)
        .WillByDefault([this](const std::shared_ptr<IPayload> &payload) {
          return fake_.DeleteService(payload);
        });
    ON_CALL(*this, FindServiceId)
        .WillByDefault([this](const RequestType request_type) {
          return fake_.FindServiceId(request_type);
        });
  }

 private:
  FakeServiceController fake_;
  MOCK_METHOD(bool, CheckService, (const std::shared_ptr<IPayload> &payload),
              (override));
};
