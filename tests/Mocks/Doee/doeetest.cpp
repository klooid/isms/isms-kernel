#include <Doee.hpp>
#include <iostream>

DECLARE_NEW_DOEE(DoeeTest)
int custom_atribute_;
void callme();
END_NEW_DOEE(DoeeTest)

PROVIDE DoeeTest::Init(const std::string &msg) {
  SUPER::Init(msg);
  this->custom_atribute_ = 0;
  std::cout << "Custom init" << std::endl;
  return PROVIDE::kSuccess;
}

void DoeeTest::callme() { std::cout << "Calling me" << std::endl; }

PROVIDE DoeeTest::Execute() {
  SUPER::Execute();
  std::cout << "Custom execute: " << this->custom_atribute_ << std::endl;
  callme();
  return PROVIDE::kSuccess;
}

PROVIDE DoeeTest::Finish() {
  std::cout << "Custom finish" << std::endl;
  return PROVIDE::kSuccess;
}
