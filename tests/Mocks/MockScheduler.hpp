/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#pragma once

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <Process.hpp>

#include <IScheduler.hpp>

using namespace isms::kn;

class MockScheduler : public IScheduler {
 public:
  MOCK_METHOD(ErrorType, RemoveProcessScheduled,
              (const std::shared_ptr<Process> &process), (override));

  MOCK_METHOD(ErrorType, ScheduleProcess,
              (const std::shared_ptr<Process> &process), (override));

  MOCK_METHOD(ErrorType, AddServiceQueue, (const int sid), (override));

  MOCK_METHOD(ErrorType, RemoveServiceQueue, (const int sid), (override));

  MOCK_METHOD(void, set_ipc_module,
              (const std::shared_ptr<IIpcModule> &ipc_module), (override));

  MOCK_METHOD(ErrorType, ChangeServiceAvailability,
              (const std::shared_ptr<IPayload> &payload), (override));

  MOCK_METHOD(ErrorType, StartScheduling, (), (override));

  MOCK_METHOD(ErrorType, StopScheduling, (), (override));

  MOCK_METHOD(ErrorType, SendProcess, (const std::shared_ptr<Process> &process),
              (override));
};