/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include <gtest/gtest.h>

#include <ILibraryHandler.hpp>
#include <string>

using namespace isms;

auto lib_handler = lib::ILibraryHandler::Build();

TEST(LibraryHandlerTest, CreationTest) {
  std::vector<std::string> path_lib = {"isms-src/doer/doee/",
                                       "isms-src/common-src/tools/"};
  std::vector<std::string> path_inc = {"../isms-src/doer/doee/include/",
                                       "../isms-src/common-src/tools/include/"};

  const std::string path{"../tests/Mocks/Doee/"};
  const std::string dependency{"-ldoee -lisms-tools"};
  const std::string name{"doeetest"};

  lib_handler->set_dependencies(dependency);
  lib_handler->set_lib_path(path_lib);
  lib_handler->set_include_path(path_inc);
  auto result1 = lib_handler->CreateLibrary(path, name);
  ASSERT_EQ(result1, kn::ErrorType::kSuccess);
}

TEST(LibraryHandlerTest, LoadTest) {}
