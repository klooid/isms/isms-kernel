/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include <gtest/gtest.h>

#include <IDoeeExecutor.hpp>
#include <string>

using namespace isms;

auto doee_executor = doer::IDoeeExecutor::Build();

TEST(DoeeExecutorTest, CreationTest) {
  const std::string path{"../tests/Mocks/Doee/"};
  const std::string doee_name{"libdoeetest.so"};
  doee_executor->set_path(path);
  auto result1 = doee_executor->LoadDoee(doee_name);
  ASSERT_EQ(result1, kn::ErrorType::kSuccess);
}

TEST(DoeeExecutorTest, RunTest) {
  auto result1 = doee_executor->RunDoee();
  ASSERT_EQ(result1, kn::ErrorType::kSuccess);
}

TEST(DoeeExecutorTest, FinishTest) {
  auto result1 = doee_executor->CloseDoee();
  ASSERT_EQ(result1, kn::ErrorType::kSuccess);
}