/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */
#include <gtest/gtest.h>

#include <type_traits>
#include <memory>

#include <CommPayload.hpp>
#include <ConnectionPayload.hpp>
#include <DBPayload.hpp>
#include <DoerPayload.hpp>
#include <IPayload.hpp>
#include <ProcessInfoPayload.hpp>
#include <ResPayload.hpp>
#include <ServiceInfoPayload.hpp>

auto payload = isms::kn::IPayload::Create();

TEST(PayloadTest, CommCast) {
  auto payload_casted =
      isms::kn::IPayload::CastPayload<isms::kn::CommPayload>(payload);
  ASSERT_EQ(typeid(std::shared_ptr<isms::kn::CommPayload>), typeid(payload_casted));
}

TEST(PayloadTest, ConnectionCast) {
  auto payload_casted =
      isms::kn::IPayload::CastPayload<isms::kn::ConnectionPayload>(payload);
  ASSERT_EQ(typeid(std::shared_ptr<isms::kn::ConnectionPayload>), typeid(payload_casted));
}

TEST(PayloadTest, ResCast) {
  auto payload_casted =
      isms::kn::IPayload::CastPayload<isms::kn::ResPayload>(payload);
  ASSERT_EQ(typeid(std::shared_ptr<isms::kn::ResPayload>), typeid(payload_casted));
}

TEST(PayloadTest, DbCast) {
  auto payload_casted =
      isms::kn::IPayload::CastPayload<isms::kn::DbPayload>(payload);
  ASSERT_EQ(typeid(std::shared_ptr<isms::kn::DbPayload>), typeid(payload_casted));
}

TEST(PayloadTest, ProcessCast) {
  auto payload_casted =
      isms::kn::IPayload::CastPayload<isms::kn::ProcessInfoPayload>(payload);
  ASSERT_EQ(typeid(std::shared_ptr<isms::kn::ProcessInfoPayload>), typeid(payload_casted));
}

TEST(PayloadTest, ServiceCast) {
  auto payload_casted =
      isms::kn::IPayload::CastPayload<isms::kn::ServiceInfoPayload>(payload);
  ASSERT_EQ(typeid(std::shared_ptr<isms::kn::ServiceInfoPayload>), typeid(payload_casted));
}

TEST(PayloadTest, DoerCast) {
  auto payload_casted =
      isms::kn::IPayload::CastPayload<isms::kn::DoerPayload>(payload);
  ASSERT_EQ(typeid(std::shared_ptr<isms::kn::DoerPayload>), typeid(payload_casted));
}
