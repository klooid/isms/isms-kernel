/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include <gtest/gtest.h>

#include <ITask.hpp>
#include <IWorkerHandler.hpp>

using namespace isms;

std::string kWorkerName1{"workertest1"};
std::shared_ptr<isms::doer::ITask> task_test;

auto worker_handler = doer::IWorkerHandler::Build();

TEST(WorkerHandlerTest, CreationTest) {
  task_test = doer::ITask::Build();
  task_test->set_pid(0);

  auto result1 = worker_handler->CreateWorker(task_test);
  ASSERT_EQ(result1, kn::ErrorType::kSuccess);
}

TEST(WorkerHandlerTest, GetInfoTest) {
  auto result1 = worker_handler->GetWorkerInfo(0);
  ASSERT_EQ(result1->get_instance_name(), "worker_0");
}

TEST(WorkerHandlerTest, CloseWorkerTest) {
  auto result1 = worker_handler->CloseWorker(task_test->get_wpid());
  ASSERT_EQ(result1, kn::ErrorType::kSuccess);
  auto result2 = worker_handler->CloseWorker(task_test->get_wpid());
  ASSERT_EQ(result2, kn::ErrorType::kNotFound);
  auto result3 = worker_handler->CloseWorker(1);
  ASSERT_EQ(result3, kn::ErrorType::kNotFound);
}
