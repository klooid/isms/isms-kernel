/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <IPayload.hpp>

#include <IProcessController.hpp>
#include <Process.hpp>
#include <IServiceController.hpp>
#include "MockScheduler.hpp"
#include "MockServiceController.hpp"

using namespace isms::kn;


auto scheduler = std::make_shared<MockScheduler>();
auto s_controller = std::make_shared<MockServiceController>();

auto p_controller = IProcessController::Build();

void SetVariables() {
  p_controller->set_scheduler(scheduler);
  p_controller->set_service_controller(s_controller);
}

TEST(ProcessTests, AddProcessTest) {
  SetVariables();
  auto conn_pay = IPayload::CastPayload<ConnectionPayload>(IPayload::Create());
  conn_pay->set_password("password");
  conn_pay->set_user("user");

  conn_pay->set_type_of_service(RequestType::kComm);
  s_controller->AddService(conn_pay);

  conn_pay->set_type_of_service(RequestType::kDb);
  s_controller->AddService(conn_pay);

  conn_pay->set_type_of_service(RequestType::kProc);
  s_controller->AddService(conn_pay);

  auto payload = IPayload::Create();
  payload->set_origin_sid(0);
  payload->set_dest_sid(1);

  ASSERT_GE(0, p_controller->AddProcess(payload));

  ASSERT_EQ(-1, p_controller->AddProcess(payload));

  payload->set_pid(-1);

  ASSERT_EQ(1, p_controller->AddProcess(payload));

  payload->set_subprocess(true);

  ASSERT_EQ(2, p_controller->AddProcess(payload));
}

TEST(ProcessTests, GetProcessTest) {
  auto payload = IPayload::Create();
  payload->set_origin_sid(0);
  payload->set_dest_sid(1);
  std::shared_ptr<Process> process_test;
  ASSERT_EQ(ErrorType::kSuccess,
            p_controller->GetProcessById(0, process_test));
  std::cout << "The process after return is: " << process_test << std::endl;
}
