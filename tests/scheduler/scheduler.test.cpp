/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <Scheduler.hpp>

#include <gtest/gtest.h>

#include <IPayload.hpp>
#include <Process.hpp>
#include <IScheduler.hpp>

using namespace isms::kn;

auto scheduler = IScheduler::Build();

TEST(SchedulerTest, AddServiceTreeTest) {
  ASSERT_EQ(ErrorType::kSuccess, scheduler->AddServiceQueue(0));
  ASSERT_EQ(ErrorType::kSuccess, scheduler->AddServiceQueue(1));
  ASSERT_EQ(ErrorType::kSuccess, scheduler->AddServiceQueue(2));

  ASSERT_NE(ErrorType::kSuccess, scheduler->AddServiceQueue(2));
}

TEST(SchedulerTest, RemoveServiceTreeTest) {
  ASSERT_GE(ErrorType::kSuccess, scheduler->RemoveServiceQueue(0));
  ASSERT_GE(ErrorType::kSuccess, scheduler->RemoveServiceQueue(1));
  ASSERT_GE(ErrorType::kSuccess, scheduler->RemoveServiceQueue(2));

  ASSERT_EQ(ErrorType::kServiceNotFound, scheduler->RemoveServiceQueue(3));
}

TEST(SchedulerTest, SchedulingProcessTest) {
  scheduler->AddServiceQueue(1);
  scheduler->AddServiceQueue(2);
  scheduler->AddServiceQueue(3);

  auto payload_test = IPayload::Create();

  payload_test->set_dest_sid(2);
  payload_test->set_origin_sid(1);
  payload_test->set_pid(2);

  auto process_0 = std::make_shared<Process>();
  process_0->set_current_sid(2);
  process_0->set_origin_sid(1);
  process_0->set_pid(2);
  process_0->set_priority(8);
  process_0->set_payload(payload_test);

  ASSERT_GE(ErrorType::kSuccess, scheduler->ScheduleProcess(process_0));

  payload_test->set_dest_sid(3);
  payload_test->set_origin_sid(2);
  payload_test->set_pid(3);

  auto process_1 = std::make_shared<Process>();
  process_1->set_current_sid(3);
  process_1->set_origin_sid(2);
  process_1->set_pid(3);
  process_1->set_priority(3);
  process_1->set_payload(payload_test);

  ASSERT_GE(ErrorType::kSuccess, scheduler->ScheduleProcess(process_1));

  payload_test->set_dest_sid(1);
  payload_test->set_origin_sid(3);
  payload_test->set_pid(4);

  auto process_2 = std::make_shared<Process>();
  process_2->set_current_sid(1);
  process_2->set_origin_sid(3);
  process_2->set_pid(4);
  process_2->set_priority(1);
  process_2->set_payload(payload_test);

  ASSERT_GE(ErrorType::kSuccess, scheduler->ScheduleProcess(process_2));

  payload_test->set_dest_sid(2);
  payload_test->set_origin_sid(1);
  payload_test->set_pid(5);

  auto process_3 = std::make_shared<Process>();
  process_3->set_current_sid(2);
  process_3->set_origin_sid(1);
  process_3->set_pid(5);
  process_3->set_priority(6);
  process_3->set_payload(payload_test);

  ASSERT_GE(ErrorType::kSuccess, scheduler->ScheduleProcess(process_3));

  payload_test->set_dest_sid(3);
  payload_test->set_origin_sid(1);
  payload_test->set_pid(6);

  auto process_4 = std::make_shared<Process>();
  process_4->set_current_sid(3);
  process_4->set_origin_sid(1);
  process_4->set_pid(6);
  process_4->set_priority(12);
  process_4->set_payload(payload_test);

  payload_test->set_dest_sid(1);
  payload_test->set_origin_sid(2);
  payload_test->set_pid(7);

  ASSERT_GE(ErrorType::kSuccess, scheduler->ScheduleProcess(process_4));

  auto process_5 = std::make_shared<Process>();
  process_5->set_current_sid(1);
  process_5->set_origin_sid(2);
  process_5->set_pid(7);
  process_5->set_priority(9);
  process_5->set_payload(payload_test);

  ASSERT_GE(ErrorType::kSuccess, scheduler->ScheduleProcess(process_5));
}

TEST(SchedulerTest, RemoveProcessTest) {
  auto payload_test = IPayload::Create();

  payload_test->set_dest_sid(2);
  payload_test->set_origin_sid(1);
  payload_test->set_pid(9);

  auto process_0 = std::make_shared<Process>();
  process_0->set_current_sid(2);
  process_0->set_origin_sid(1);
  process_0->set_pid(9);
  process_0->set_priority(8);
  process_0->set_payload(payload_test);

  ASSERT_EQ(ErrorType::kCannotDelete,
            scheduler->RemoveProcessScheduled(process_0));

  payload_test->set_dest_sid(4);
  process_0->set_current_sid(4);

  ASSERT_EQ(ErrorType::kServiceNotFound,
            scheduler->RemoveProcessScheduled(process_0));

  payload_test->set_dest_sid(3);
  process_0->set_current_sid(3);
  scheduler->ScheduleProcess(process_0);

  ASSERT_EQ(ErrorType::kSuccess, scheduler->RemoveProcessScheduled(process_0));
}
