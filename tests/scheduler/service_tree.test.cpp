/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <random>

#include <Process.hpp>
#include <ServiceTree.hpp>

using namespace isms::kn;

auto service_tree = ServiceTree();

TEST(ServiceTreeTest, InsertionTest) {
  auto process = std::make_shared<Process>();
  std::shared_ptr<Process> process_null;

  process->set_current_sid(0);
  process->set_origin_sid(1);
  process->set_pid(9);
  process->set_priority(10);

  ASSERT_EQ(9, service_tree.InsertProcess(process));
  ASSERT_EQ(-1, service_tree.InsertProcess(process));
  ASSERT_EQ(-2, service_tree.InsertProcess(process_null));

  ASSERT_EQ(1, service_tree.RemoveProcess(process));
}

TEST(ServiceTreeTest, RemoveTest) {
  auto process = std::make_shared<Process>();
  process->set_current_sid(0);
  process->set_origin_sid(1);
  process->set_pid(1);
  process->set_priority(20);

  ASSERT_EQ(-1, service_tree.RemoveProcess(process));

  service_tree.InsertProcess(process);

  ASSERT_GE(1, service_tree.RemoveProcess(process));
}

TEST(ServiceTreeTest, GettingTest) {
  auto process_0 = std::make_shared<Process>();
  process_0->set_current_sid(0);
  process_0->set_origin_sid(1);
  process_0->set_pid(2);
  process_0->set_priority(8);

  auto process_1 = std::make_shared<Process>();
  process_1->set_current_sid(0);
  process_1->set_origin_sid(1);
  process_1->set_pid(3);
  process_1->set_priority(3);

  auto process_2 = std::make_shared<Process>();
  process_2->set_current_sid(0);
  process_2->set_origin_sid(1);
  process_2->set_pid(4);
  process_2->set_priority(1);

  auto process_3 = std::make_shared<Process>();
  process_3->set_current_sid(0);
  process_3->set_origin_sid(1);
  process_3->set_pid(5);
  process_3->set_priority(6);

  service_tree.InsertProcess(process_0);
  service_tree.InsertProcess(process_1);
  service_tree.InsertProcess(process_2);
  service_tree.InsertProcess(process_3);

  ASSERT_EQ(process_2, service_tree.GetProcess());

  EXPECT_EQ(process_1, service_tree.GetProcess());
  EXPECT_EQ(process_3, service_tree.GetProcess());
  EXPECT_EQ(process_0, service_tree.GetProcess());
  EXPECT_EQ(nullptr, service_tree.GetProcess());
  service_tree.InsertProcess(process_3);
  auto process_7 = service_tree.GetProcess();
  std::cout << "The process in tree before is: " << process_3 << " : "
            << process_7 << std::endl;
}
