/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <IPayload.hpp>
#include <IProjectHandler.hpp>
#include <IStorageHandler.hpp>
#include <ResPayload.hpp>
#include <iostream>

using namespace isms;

auto project_handler = resadmin::IProjectHandler::Build();
auto payload =
    kn::IPayload::CastPayload<kn::ResPayload>(kn::IPayload::Create());

TEST(ProjectHandlerTest, CreateProject) {
  project_handler->set_base_directory("../assets");

  payload->set_user("test_project_user");
  payload->set_project_name("test_project_name");
  payload->set_new_user("user_test_new");

  ASSERT_EQ(kn::ErrorType::kSuccess, project_handler->Init());
  ASSERT_EQ(kn::ErrorType::kSuccess, project_handler->CreateProject(payload));
  ASSERT_NE(kn::ErrorType::kSuccess, project_handler->CreateProject(payload));
}

TEST(ProjectHandlerTest, AddNewUser) {
  ASSERT_EQ(kn::ErrorType::kSuccess,
            project_handler->AddUserToProject(payload));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists,
            project_handler->AddUserToProject(payload));
  payload->set_project_name("wrong_project");
  ASSERT_EQ(kn::ErrorType::kNotFound,
            project_handler->AddUserToProject(payload));
}

TEST(ProjectHandlerTest, DeleteProject) {
  payload->set_project_name("test_project_name");
  ASSERT_EQ(kn::ErrorType::kSuccess, project_handler->DeleteProject(payload));
  ASSERT_NE(kn::ErrorType::kSuccess, project_handler->DeleteProject(payload));
}
