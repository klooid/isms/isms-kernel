/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <CommPayload.hpp>
#include <ICommServerHandler.hpp>
#include <IPayload.hpp>
#include <IStorageHandler.hpp>
#include <ResPayload.hpp>
#include <iostream>

using namespace isms;

auto commserver_handler = resadmin::ICommServerHandler::Build();
auto payload =
    kn::IPayload::CastPayload<kn::CommPayload>(kn::IPayload::Create());
auto storage_handler = resadmin::IStorageHandler::Build();

const std::string kUserTestFolder{"projects-test/commserver_test_folder"};
const std::string kCommServerFolder{"/comm-server"};
const std::string kCommServerConfigFile{"/comm-server-conf.yml"};
const std::string kEndpointName{"topic/test"};
const std::string kEndpointNameWrong{"topic/test/wrong"};

TEST(CommServerHandlerTest, InitializeCommServerFile) {
  commserver_handler->set_base_directory("../assets");
  storage_handler->set_base_directory("../assets");
  storage_handler->CreateFolder(kUserTestFolder);
  storage_handler->CreateFolder(kUserTestFolder + kCommServerFolder);
  storage_handler->AddFile(kUserTestFolder + kCommServerFolder +
                           kCommServerConfigFile);
  commserver_handler->set_storage_handler(storage_handler);

  payload->set_user("test_project_user");

  ASSERT_EQ(kn::ErrorType::kSuccess, commserver_handler->Init(kUserTestFolder));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists,
            commserver_handler->Init(kUserTestFolder));
}

TEST(CommServerHandlerTest, AddEntity) {
  payload->set_protocol(kn::CommServerProtocol::kMqtt);

  ASSERT_EQ(kn::ErrorType::kSuccess,
            commserver_handler->AddCommServerEntity(payload, kUserTestFolder));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists,
            commserver_handler->AddCommServerEntity(payload, kUserTestFolder));
}

TEST(CommServerHandlerTest, AddEnpoints) {
  payload->set_path_to_send(kEndpointName);
  payload->set_function_type(kn::EndpointType::kSub);

  ASSERT_EQ(kn::ErrorType::kSuccess,
            commserver_handler->AddEndpointToEntity(payload, kUserTestFolder));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists,
            commserver_handler->AddEndpointToEntity(payload, kUserTestFolder));

  payload->set_path_to_send(kEndpointNameWrong);
  payload->set_function_type(kn::EndpointType::kPub);

  ASSERT_EQ(kn::ErrorType::kSuccess,
            commserver_handler->AddEndpointToEntity(payload, kUserTestFolder));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists,
            commserver_handler->AddEndpointToEntity(payload, kUserTestFolder));
}

TEST(CommServerHandlerTest, DeleteEndpoint) {
  payload->set_path_to_send(kEndpointNameWrong);
  payload->set_function_type(kn::EndpointType::kSub);
  ASSERT_EQ(
      kn::ErrorType::kNotFound,
      commserver_handler->RemoveEndpointFromEntity(payload, kUserTestFolder));

  payload->set_path_to_send(kEndpointNameWrong);
  payload->set_function_type(kn::EndpointType::kPub);
  ASSERT_EQ(
      kn::ErrorType::kSuccess,
      commserver_handler->RemoveEndpointFromEntity(payload, kUserTestFolder));
  ASSERT_EQ(
      kn::ErrorType::kNotFound,
      commserver_handler->RemoveEndpointFromEntity(payload, kUserTestFolder));

  payload->set_path_to_send(kEndpointName);
  payload->set_function_type(kn::EndpointType::kPub);
  ASSERT_EQ(
      kn::ErrorType::kNotFound,
      commserver_handler->RemoveEndpointFromEntity(payload, kUserTestFolder));

  payload->set_function_type(kn::EndpointType::kSub);
  ASSERT_EQ(
      kn::ErrorType::kSuccess,
      commserver_handler->RemoveEndpointFromEntity(payload, kUserTestFolder));
  ASSERT_EQ(
      kn::ErrorType::kNotFound,
      commserver_handler->RemoveEndpointFromEntity(payload, kUserTestFolder));
}

TEST(CommServerHandlerTest, DeleteEntity) {
  ASSERT_EQ(kn::ErrorType::kSuccess, commserver_handler->RemoveCommServerEntity(
                                         payload, kUserTestFolder));
  ASSERT_EQ(
      kn::ErrorType::kNotFound,
      commserver_handler->RemoveCommServerEntity(payload, kUserTestFolder));

  storage_handler->RemoveAll(kUserTestFolder + kCommServerFolder);
}
