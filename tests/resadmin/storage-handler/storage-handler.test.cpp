/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <IStorageHandler.hpp>
#include <iostream>

using namespace isms;

auto storage_handle = resadmin::IStorageHandler::Build();

const std::string kTestDir{"projects/test"};
const std::string kTestProject{"projects/test_project"};
const std::string kTestFile{"projects/test.txt"};

TEST(StorageHandlerTest, Createsingle) {
  storage_handle->set_base_directory("../assets");

  /* Testing directory */
  ASSERT_EQ(kn::ErrorType::kSuccess, storage_handle->CreateFolder(kTestDir));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists, storage_handle->CreateFolder(kTestDir));

  /* Testing directory */
  ASSERT_EQ(kn::ErrorType::kSuccess, storage_handle->AddFile(kTestFile));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists, storage_handle->AddFile(kTestFile));
}

TEST(StorageHandlerTest, RemoveSingle) {
  /* Testing directory */
  ASSERT_EQ(kn::ErrorType::kSuccess, storage_handle->RemoveSingle(kTestDir));
  ASSERT_EQ(kn::ErrorType::kNotFound, storage_handle->RemoveSingle(kTestDir));

  /* Testing directory */
  ASSERT_EQ(kn::ErrorType::kSuccess, storage_handle->RemoveSingle(kTestFile));
  ASSERT_EQ(kn::ErrorType::kNotFound, storage_handle->RemoveSingle(kTestFile));
}

TEST(StorageHandlerTest, CreateProject) {
  /* Testing directory */
  ASSERT_EQ(
      kn::ErrorType::kSuccess,
      storage_handle->CreateProjectDirectories(kTestProject).get_error_code());
  ASSERT_EQ(
      kn::ErrorType::kCannotCreate,
      storage_handle->CreateProjectDirectories(kTestProject).get_error_code());
}

TEST(StorageHandlerTest, RemoveProject) {
  /* Testing directory */
  ASSERT_EQ(
      kn::ErrorType::kSuccess,
      storage_handle->RemoveProjectDirectories(kTestProject).get_error_code());
  ASSERT_EQ(
      kn::ErrorType::kNotFound,
      storage_handle->RemoveProjectDirectories(kTestProject).get_error_code());
}
