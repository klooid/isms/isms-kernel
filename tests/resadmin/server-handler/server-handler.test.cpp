/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <IServerHandler.hpp>
#include <iostream>

using namespace isms;

auto server_handler = resadmin::IServerHandler::Build();
const std::string kConfigurationFile{"/usr/local/etc/isms/resadmin/pure-ftpd.conf"};

TEST(ServerHandlerTest, StartServer) {
  std::cout << "Starting the server" << std::endl;
  server_handler->set_configuration_file_path(kConfigurationFile);
  ASSERT_EQ(kn::ErrorType::kSuccess, server_handler->StartServer());
  ASSERT_EQ(kn::ErrorType::kCannotOpen, server_handler->StartServer());
}

TEST(ServerHandlerTest, StopServer) {
  ASSERT_EQ(kn::ErrorType::kSuccess, server_handler->StopServer());
  ASSERT_EQ(kn::ErrorType::kCannotClose, server_handler->StopServer());
}
