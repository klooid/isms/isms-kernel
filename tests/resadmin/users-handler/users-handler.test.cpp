/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <IDbAccess.hpp>
#include <IPayload.hpp>
#include <IUsersHandler.hpp>
#include <ResPayload.hpp>
#include <iostream>

using namespace isms;

auto users_handler = resadmin::IUsersHandler::Build();
auto db_access = db::IDbAccess::Build();

auto res_payload =
    kn::IPayload::CastPayload<kn::ResPayload>(kn::IPayload::Create());

const std::string kTestDir{"../assets/projects/test_project_user"};
const std::string kUserName{"test_user"};
const std::string kUserPassword{"password"};
const std::string kUserGroup{"group_test"};
const std::string kDatabaseName{"resadmin_test"};

TEST(UsersHandlerTest, CreateGroup) {
  db_access->set_ini_file_path("/usr/local/etc/isms/resadmin/resadmin-db.ini");
  db_access->set_database_name(kDatabaseName);
  db_access->Init();
  users_handler->set_db_access(db_access);
  
  res_payload->set_user(kUserName);
  res_payload->set_group_name(kUserGroup);

  /* Testing groups */
  ASSERT_EQ(kn::ErrorType::kSuccess,
            users_handler->CreateUserGroup(res_payload, kTestDir));
  ASSERT_NE(kn::ErrorType::kSuccess,
            users_handler->CreateUserGroup(res_payload, kTestDir));
}

TEST(UsersHandlerTest, CreateUser) {
  res_payload->set_new_user(kUserName);
  res_payload->set_password(kUserPassword);
  /* Testing groups */
  ASSERT_EQ(kn::ErrorType::kSuccess,
            users_handler->CreateUser(res_payload, kTestDir, false));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists,
            users_handler->CreateUser(res_payload, kTestDir, false));
}

TEST(UsersHandlerTest, DisableUser) {
  /* Testing groups */
  ASSERT_EQ(kn::ErrorType::kSuccess, users_handler->DisableUser(res_payload));
}

TEST(UsersHandlerTest, DeleteUser) {
  /*/* Testing groups */
  ASSERT_EQ(kn::ErrorType::kSuccess, users_handler->DeleteUser(res_payload));
  ASSERT_NE(kn::ErrorType::kSuccess, users_handler->DeleteUser(res_payload));
  ASSERT_EQ(kn::ErrorType::kSuccess,
            users_handler->DeleteUserGroup(res_payload));
}
