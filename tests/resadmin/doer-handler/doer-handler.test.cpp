/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <DoerPayload.hpp>
#include <IDoerHandler.hpp>
#include <IPayload.hpp>
#include <IStorageHandler.hpp>
#include <ResPayload.hpp>
#include <iostream>

using namespace isms;

auto doer_handler = resadmin::IDoerHandler::Build();
auto payload =
    kn::IPayload::CastPayload<kn::DoerPayload>(kn::IPayload::Create());
auto storage_handler = resadmin::IStorageHandler::Build();

const std::string kUserTestFolder{"projects-test/doer_test_folder"};
const std::string kDoeeFolder{"/doees"};
const std::string kDoeeConfigFile{"/doees_conf.yml"};
const std::string kDoeeName{"doee_test"};
const std::string kDoeeNameWrong{"doee_test_wrong"};

TEST(DoerHandlerTest, ConfigureDoee) {
  doer_handler->set_base_directory("../assets");
  storage_handler->set_base_directory("../assets");
  storage_handler->CreateFolder(kUserTestFolder);
  storage_handler->CreateFolder(kUserTestFolder + kDoeeFolder);
  storage_handler->AddFile(kUserTestFolder + kDoeeFolder + kDoeeConfigFile);

  doer_handler->set_storage_handler(storage_handler);

  payload->set_user("test_project_user");

  ASSERT_EQ(kn::ErrorType::kSuccess,
            doer_handler->ConfigureDoeeStructure(kUserTestFolder));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists,
            doer_handler->ConfigureDoeeStructure(kUserTestFolder));
}

TEST(DoerHandlerTest, AddDoee) {
  payload->set_doee_name(kDoeeName);
  payload->set_doee_src("DoeeCustom.cpp");
  payload->set_doee_language("cpp");

  ASSERT_EQ(kn::ErrorType::kSuccess,
            doer_handler->AddDoee(payload, kUserTestFolder));
  ASSERT_EQ(kn::ErrorType::kAlreadyExists,
            doer_handler->AddDoee(payload, kUserTestFolder));
}

TEST(DoerHandlerTest, RetrieveDoee) {
  auto payload_cmp =
      kn::IPayload::CastPayload<kn::DoerPayload>(kn::IPayload::Create());

  payload_cmp->set_doee_name(kDoeeNameWrong);
  ASSERT_EQ(kn::ErrorType::kNotFound,
            doer_handler->RetrieveDoeePath(payload_cmp, kUserTestFolder));

  payload_cmp->set_doee_name(kDoeeName);
  ASSERT_EQ(kn::ErrorType::kSuccess,
            doer_handler->RetrieveDoeePath(payload_cmp, kUserTestFolder));
  ASSERT_EQ(true, payload->get_volume_path() == payload_cmp->get_volume_path());
  ASSERT_EQ(true,
            payload->get_doee_language() == payload_cmp->get_doee_language());
}

TEST(DoerHandlerTest, DeleteDoee) {
  payload->set_doee_name(kDoeeNameWrong);
  ASSERT_EQ(kn::ErrorType::kNotFound,
            doer_handler->DeleteDoee(payload, kUserTestFolder));

  payload->set_doee_name(kDoeeName);
  ASSERT_EQ(kn::ErrorType::kSuccess,
            doer_handler->DeleteDoee(payload, kUserTestFolder));
  storage_handler->RemoveAll(kUserTestFolder + kDoeeFolder);
}
