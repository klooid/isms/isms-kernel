/*
 * IoT Services Management System - 2022
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2022
 */

#include <gtest/gtest.h>

#include <sinwrapper.hpp>

std::string kWorkerName1{"workertest1"};

using namespace isms;

TEST(SinwrapperTest, CreationTest) {
    auto result1 = sinw::StartInstance(kWorkerName1);
    ASSERT_EQ(result1, kn::ErrorType::kSuccess);
}

TEST(SinwrapperTest, CloseTest) {
    auto result1 = sinw::StopInstance(kWorkerName1);
    ASSERT_EQ(result1, kn::ErrorType::kSuccess);
}

TEST(SinwrapperTest, CantCreateTest) {
    auto result1 = sinw::StartInstance(kWorkerName1);
    ASSERT_EQ(result1, kn::ErrorType::kSuccess);

    result1 = sinw::StartInstance(kWorkerName1);
    ASSERT_NE(result1, kn::ErrorType::kSuccess);

    result1 = sinw::StopInstance(kWorkerName1);
    ASSERT_EQ(result1, kn::ErrorType::kSuccess);
}

TEST(SinwrapperTest, ExecuteTest) {
    auto result1 = sinw::StartInstance(kWorkerName1);
    ASSERT_EQ(result1, kn::ErrorType::kSuccess);
    auto result2 = sinw::ExecCommand(kWorkerName1, "echo holamundo");
    ASSERT_EQ(result2, kn::ErrorType::kSuccess);
    auto result3 = sinw::StopInstance(kWorkerName1);
    ASSERT_EQ(result3, kn::ErrorType::kSuccess);
}
