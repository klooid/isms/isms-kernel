/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include <CommPayload.hpp>
#include <CommServerProtocol.hpp>
#include <ConnectionPayload.hpp>
#include <DBPayload.hpp>
#include <DoerPayload.hpp>
#include <EndpointType.hpp>
#include <IPayload.hpp>
#include <ProcessInfoPayload.hpp>
#include <ResPayload.hpp>
#include <ServiceInfoPayload.hpp>
#include <ServiceState.hpp>
#include <serialization.hpp>
#include <string>

auto payload = isms::kn::IPayload::Create();

TEST(SerializationTest, SerializeTest) {
  std::string stream_str;
  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Serialize(payload, stream_str));
}

TEST(SerializationTest, IPayloadDeserializeTest) {
  payload->set_origin_sid(0);
  payload->set_dest_sid(1);
  payload->set_pid(2);
  payload->set_project_id(3);
  payload->set_user("usertest");
  payload->set_password("passtest");
  payload->set_request_type(isms::kn::RequestType::kComm);

  auto payload_serialized = isms::kn::IPayload::Create();
  std::string stream_str;
  isms::kn::Serialize(payload, stream_str);

  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Deserialize(stream_str, payload_serialized));
  EXPECT_EQ(payload->get_origin_sid(), payload_serialized->get_origin_sid());
  EXPECT_EQ(payload->get_dest_sid(), payload_serialized->get_dest_sid());
  EXPECT_EQ(payload->get_pid(), payload_serialized->get_pid());
  EXPECT_EQ(payload->get_project_id(), payload_serialized->get_project_id());
  EXPECT_EQ(payload->get_user(), payload_serialized->get_user());
  EXPECT_EQ(payload->get_password(), payload_serialized->get_password());
  EXPECT_EQ(payload->get_request_type(),
            payload_serialized->get_request_type());
}

TEST(SerializationTest, ConnDeserializeTest) {
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::ConnectionPayload>(payload);
  test_payload->set_endpoint("test.ipc");
  test_payload->set_type_of_service(isms::kn::RequestType::kComm);

  auto payload_serialized =
      isms::kn::IPayload::CastPayload<isms::kn::ConnectionPayload>(
          isms::kn::IPayload::Create());
  std::string stream_str;
  isms::kn::Serialize(test_payload, stream_str);

  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Deserialize(stream_str, payload_serialized));
  EXPECT_EQ(test_payload->get_endpoint(), payload_serialized->get_endpoint());
  EXPECT_EQ(test_payload->get_type_of_service(),
            payload_serialized->get_type_of_service());
}

TEST(SerializationTest, CommDeserializeTest) {
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::CommPayload>(payload);
  test_payload->set_data("test data");
  test_payload->set_function_id(4);
  test_payload->set_function_name("POST");
  test_payload->set_function_type(isms::kn::EndpointType::kSub);
  test_payload->set_path_to_send("test/test/path");
  test_payload->set_protocol(isms::kn::CommServerProtocol::kMqtt);
  test_payload->set_result_data("test_result");

  auto payload_serialized =
      isms::kn::IPayload::CastPayload<isms::kn::CommPayload>(
          isms::kn::IPayload::Create());
  std::string stream_str;
  isms::kn::Serialize(test_payload, stream_str);

  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Deserialize(stream_str, payload_serialized));

  EXPECT_EQ(test_payload->get_function_id(),
            payload_serialized->get_function_id());
  EXPECT_EQ(test_payload->get_data(), payload_serialized->get_data());
  EXPECT_EQ(test_payload->get_function_name(),
            payload_serialized->get_function_name());
  EXPECT_EQ(test_payload->get_function_type(),
            payload_serialized->get_function_type());
  EXPECT_EQ(test_payload->get_path_to_send(),
            payload_serialized->get_path_to_send());
  EXPECT_EQ(test_payload->get_protocol(), payload_serialized->get_protocol());
  EXPECT_EQ(test_payload->get_result_data(),
            payload_serialized->get_result_data());
}

TEST(SerializationTest, DbDeserializeTest) {
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::DbPayload>(payload);
  test_payload->set_custom(false);
  test_payload->set_function_name("test func");
  test_payload->set_inputs("test_inputs");
  test_payload->set_outputs("test outputs");
  test_payload->set_query("test query");

  auto payload_serialized =
      isms::kn::IPayload::CastPayload<isms::kn::DbPayload>(
          isms::kn::IPayload::Create());
  std::string stream_str;
  isms::kn::Serialize(test_payload, stream_str);

  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Deserialize(stream_str, payload_serialized));

  EXPECT_EQ(test_payload->IsCustom(), payload_serialized->IsCustom());
  EXPECT_EQ(test_payload->get_function_name(),
            payload_serialized->get_function_name());
  EXPECT_EQ(test_payload->get_inputs(), payload_serialized->get_inputs());
  EXPECT_EQ(test_payload->get_outputs(), payload_serialized->get_outputs());
  EXPECT_EQ(test_payload->get_query(), payload_serialized->get_query());
}

TEST(SerializationTest, DoerDeserializeTest) {
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::DoerPayload>(payload);
  test_payload->set_doee_id(5);
  test_payload->set_doee_name("test name");
  test_payload->set_inputs("test inputs");
  test_payload->set_result("test result");
  test_payload->set_volume_path("src/test.test");

  auto payload_serialized =
      isms::kn::IPayload::CastPayload<isms::kn::DoerPayload>(
          isms::kn::IPayload::Create());
  std::string stream_str;
  isms::kn::Serialize(test_payload, stream_str);

  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Deserialize(stream_str, payload_serialized));

  EXPECT_EQ(test_payload->get_doee_id(), payload_serialized->get_doee_id());
  EXPECT_EQ(test_payload->get_doee_name(), payload_serialized->get_doee_name());
  EXPECT_EQ(test_payload->get_inputs(), payload_serialized->get_inputs());
  EXPECT_EQ(test_payload->get_result(), payload_serialized->get_result());
  EXPECT_EQ(test_payload->get_volume_path(),
            payload_serialized->get_volume_path());
}

TEST(SerializationTest, ProcessInfoDeserializeTest) {
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::ProcessInfoPayload>(payload);
  test_payload->set_current_service("test current");
  test_payload->set_current_time(1000);
  test_payload->set_origin_service("test origin");
  test_payload->set_origin_sid(1);
  test_payload->set_state(isms::kn::ProcessState::kReady);
  test_payload->set_process_id(6);
  test_payload->set_process_name("test name");
  test_payload->set_process_type("comm");
  test_payload->set_start_time("test time");

  auto payload_serialized =
      isms::kn::IPayload::CastPayload<isms::kn::ProcessInfoPayload>(
          isms::kn::IPayload::Create());
  std::string stream_str;
  isms::kn::Serialize(test_payload, stream_str);

  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Deserialize(stream_str, payload_serialized));

  EXPECT_EQ(test_payload->get_current_service(),
            payload_serialized->get_current_service());
  EXPECT_EQ(test_payload->get_current_time(),
            payload_serialized->get_current_time());
  EXPECT_EQ(test_payload->get_origin_service(),
            payload_serialized->get_origin_service());
  EXPECT_EQ(test_payload->get_origin_sid(),
            payload_serialized->get_origin_sid());
  EXPECT_EQ(test_payload->get_state(), payload_serialized->get_state());
  EXPECT_EQ(test_payload->get_process_id(),
            payload_serialized->get_process_id());
  EXPECT_EQ(test_payload->get_process_name(),
            payload_serialized->get_process_name());
  EXPECT_EQ(test_payload->get_process_type(),
            payload_serialized->get_process_type());
  EXPECT_EQ(test_payload->get_start_time(),
            payload_serialized->get_start_time());
}

TEST(SerializationTest, ResDeserializeTest) {
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::ResPayload>(payload);
  test_payload->set_comm_server_id(7);
  test_payload->set_device_id(8);
  test_payload->set_doee_id(9);
  test_payload->set_ip_to_check("test ip");
  test_payload->set_resource_type("test resource");

  auto payload_serialized =
      isms::kn::IPayload::CastPayload<isms::kn::ResPayload>(
          isms::kn::IPayload::Create());
  std::string stream_str;
  isms::kn::Serialize(test_payload, stream_str);

  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Deserialize(stream_str, payload_serialized));

  EXPECT_EQ(test_payload->get_comm_server_id(),
            payload_serialized->get_comm_server_id());
  EXPECT_EQ(test_payload->get_device_id(), payload_serialized->get_device_id());
  EXPECT_EQ(test_payload->get_doee_id(), payload_serialized->get_doee_id());
  EXPECT_EQ(test_payload->get_ip_to_check(),
            payload_serialized->get_ip_to_check());
  EXPECT_EQ(test_payload->get_resource_type(),
            payload_serialized->get_resource_type());
}

TEST(SerializationTest, ServiceInfoDeserializeTest) {
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::ServiceInfoPayload>(payload);
  test_payload->set_current_time("test time");
  test_payload->set_service_id(10);
  test_payload->set_service_name("test name");
  test_payload->set_service_type("test type");
  test_payload->set_start_time("test start time");
  test_payload->set_state(isms::kn::ServiceState::kRun);

  auto payload_serialized =
      isms::kn::IPayload::CastPayload<isms::kn::ServiceInfoPayload>(
          isms::kn::IPayload::Create());
  std::string stream_str;
  isms::kn::Serialize(test_payload, stream_str);

  ASSERT_EQ(isms::kn::ErrorType::kSuccess,
            isms::kn::Deserialize(stream_str, payload_serialized));

  EXPECT_EQ(test_payload->get_current_time(),
            payload_serialized->get_current_time());
  EXPECT_EQ(test_payload->get_service_id(),
            payload_serialized->get_service_id());
  EXPECT_EQ(test_payload->get_service_name(),
            payload_serialized->get_service_name());
  EXPECT_EQ(test_payload->get_service_type(),
            payload_serialized->get_service_type());
  EXPECT_EQ(test_payload->get_start_time(),
            payload_serialized->get_start_time());
  EXPECT_EQ(test_payload->get_state(), payload_serialized->get_state());
}
