/*
 * IoT Services Management System - 2021
 * See LICENSE for more information about licensing
 *
 * Author: Jose Ortega <ortega.josant@gmail.com>
 *         Luis G. Leon Vega <lleon95@gmail.com>
 *
 * Sponsor: Klooid Innovations 2021
 */

#include <gtest/gtest.h>

#include "MockScheduler.hpp"
#include <ConnectionPayload.hpp>
#include <IPayload.hpp>
#include <IServiceController.hpp>
#include <ErrorType.hpp>

const std::string kUser{"user"};
const std::string kPassword{"password"};

auto scheduler = std::make_shared<MockScheduler>();

TEST(ServicesTests, ConnectionTest) {
  auto service_controller = isms::kn::IServiceController::Build();
  service_controller->set_scheduler(scheduler);
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::ConnectionPayload>(
          isms::kn::IPayload::Create());
  test_payload->set_user(kUser);
  test_payload->set_password(kPassword);
  test_payload->set_type_of_service(isms::kn::RequestType::kComm);
  EXPECT_EQ(0, service_controller->AddService(test_payload));
  EXPECT_GT(0, service_controller->AddService(test_payload));
  test_payload->set_type_of_service(isms::kn::RequestType::kProc);
  EXPECT_EQ(1, service_controller->AddService(test_payload));
}

TEST(ServicesTests, GetServiceTest) {
  auto service_controller = isms::kn::IServiceController::Build();
  service_controller->set_scheduler(scheduler);
  auto test_payload =
      isms::kn::IPayload::CastPayload<isms::kn::ConnectionPayload>(
          isms::kn::IPayload::Create());
  test_payload->set_user(kUser);
  test_payload->set_password(kPassword);
  test_payload->set_type_of_service(isms::kn::RequestType::kComm);
  service_controller->AddService(test_payload);

  test_payload->set_type_of_service(isms::kn::RequestType::kDb);
  service_controller->AddService(test_payload);

  // std::cout << "Both are: " << s_1 << " " << s_2 << std::endl;
  std::shared_ptr<isms::kn::Service> service_test_1;
  std::shared_ptr<isms::kn::Service> service_test_2;
  std::shared_ptr<isms::kn::Service> service_test_3;

  EXPECT_EQ(0, service_controller->FindServiceId(isms::kn::RequestType::kComm));
  EXPECT_EQ(1, service_controller->FindServiceId(isms::kn::RequestType::kDb));

  ASSERT_GE(isms::kn::ErrorType::kSuccess,
            service_controller->ReturnServiceInfo(0, service_test_1));
  EXPECT_NE(nullptr, service_test_1);
  ASSERT_GE(isms::kn::ErrorType::kSuccess,
            service_controller->ReturnServiceInfo(1, service_test_2));
  EXPECT_NE(nullptr, service_test_2);
  ASSERT_EQ(isms::kn::ErrorType::kNotFound,
            service_controller->ReturnServiceInfo(2, service_test_3));
  ASSERT_EQ(nullptr, service_test_3);
}

TEST(ServicesTests, DeleteServiceTest) {
  auto service_controller = isms::kn::IServiceController::Build();
  service_controller->set_scheduler(scheduler);
  auto test_payload_1 =
      isms::kn::IPayload::CastPayload<isms::kn::ConnectionPayload>(
          isms::kn::IPayload::Create());
  test_payload_1->set_user(kUser);
  test_payload_1->set_password(kPassword);
  test_payload_1->set_type_of_service(isms::kn::RequestType::kComm);

  service_controller->AddService(test_payload_1);
  auto test_payload_2 =
      isms::kn::IPayload::CastPayload<isms::kn::ConnectionPayload>(
          isms::kn::IPayload::Create());
  test_payload_2->set_user(kUser);
  test_payload_2->set_password(kPassword);
  test_payload_2->set_type_of_service(isms::kn::RequestType::kDb);
  service_controller->AddService(test_payload_2);

  EXPECT_EQ(0, service_controller->DeleteService(test_payload_1));
  EXPECT_EQ(1, service_controller->DeleteService(test_payload_2));
}
