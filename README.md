# IoT Services Management System

## Description

The IoT Services Management System (ISMS) is an open-source project aims to provide an integrated platform to deploy and handle IoT applications. Its architecture is based in a microkernel-like design and the computational resource is a serverless-based approach.

## Infrastructure

Figure 1 shows an infrastructure overview of the ISMS. There are 7 main modules that are described below.

### Kernel
It is the manager of the system, is based in a microkernel design which provides an agnostic communication between modules and also, scheduling functionalities for all the processes.

### Doer
The Doer is the module responsible for the computational duties, it is based in the serverless approach. All the processes will be executed here by using the Workers. The Workers are isolated environments that are listening for requests to run an specific function, which is called Doee.

### Resadmin
This module handles the information for the projects. It manages the storage for the project and for the users, Doees, the users information, and any other relevant information.

### DBriver
The DBriver is the handler of the database entities.

### CommServer
This module manages the communication server entities. These server are based in the HTTP and the MQTT protocols so, when a project is created it can choose a server to create.

### ControlGate
This modules provide the interface for the users to manage the platform, create the projects and control them.

<img src="https://drive.google.com/uc?export=view&id=1IXlDJNDjUhZe15Gz8dv6hbohlQlHLEkN" width=45% height=45%>

<b>Fig.1 ISMS infrastructure overview.</b>

## Build the system

### Requirements

This project are developed using **Linux Ubuntu 20.04**, we recommend to not install it in another distribution as it is not supported yet, but you can try to install it in a Debian-based distribution by your own responsability.

1. **Basic requirements**

Be sure to install the following requirements:

```bash
sudo apt-get update
sudo apt-get install build-essential \
    zlib1g-dev libncurses5-dev libgdbm-dev \
    libnss3-dev libssl-dev libreadline-dev \
    libffi-dev curl libboost-serialization-dev \
    libseccomp-dev pkg-config squashfs-tools \
    cryptsetup
```

2. **Building system requirement**

Meson is the tool used to handle the building system of the project, it could be found by clicking this [link](https://mesonbuild.com/Quick-guide.html) or following the next steps:

```bash
sudo apt-get install python3 python3-pip python3-setuptools \
    python3-wheel ninja-build
pip3 install --user meson
sudo pip3 install meson
```

3. **libzmq requirement**
This instructions are **ONLY** for Linux Ubuntu 20.04, you can found more instructions [here](https://software.opensuse.org/download.html?project=network%3Amessaging%3Azeromq%3Arelease-stable&package=libzmq3-dev)

Steps:

```bash
echo 'deb http://download.opensuse.org/repositories/network:/messaging:/zeromq:/release-stable/xUbuntu_20.04/ /' | sudo tee /etc/apt/sources.list.d/network:messaging:zeromq:release-stable.list
curl -fsSL https://download.opensuse.org/repositories/network:messaging:zeromq:release-stable/xUbuntu_20.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/network_messaging_zeromq_release-stable.gpg > /dev/null
sudo apt update
sudo apt install libzmq3-dev
```

4. **Singularity requirement**

You can find the Singularity installation steps for other versions here [here](https://sylabs.io/guides/3.9/user-guide/quick_start.html#quick-installation-steps) but we recomend to follow the next steps:

+ Install Go:

    ```bash
    export VERSION=1.18 OS=linux ARCH=amd64 && \
        wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz && \
        sudo tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz && \
        rm go$VERSION.$OS-$ARCH.tar.gz
    ```

* Set the PATH var to point to Go

    ```bash
    echo 'export PATH=/usr/local/go/bin:$PATH' >> ~/.bashrc && \
        source ~/.bashrc
    ```

* Dowload singularity release:

    ```bash
    export VERSION=3.9.7 && \
        wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz && \
        tar -xzf singularity-ce-${VERSION}.tar.gz && \
        cd singularity-ce-${VERSION}
    ```

* Compiling Singularity

    ```bash
    ./mconfig && \
        make -C builddir && \
        sudo make -C builddir install
    ```
 
 5. **Pure-FTPd requirement**

* Download the MariaDB dependency, we recommend to use the version 10.6.7 but you can change it by setting the **VERSION** env variable:

    ```bash
    sudo apt update
    sudo apt upgrade -y
    sudo apt install software-properties-common -y

    export VERSION=10.6.7 && \
    curl -LsS -O https://downloads.mariadb.com/MariaDB/mariadb_repo_setup && \
    sudo bash mariadb_repo_setup --mariadb-server-version=${VERSION}
    

    
    sudo apt update
    sudo apt install mariadb-server mariadb-client libmariadb-dev libmysqlcppconn-dev
    wget https://dev.mysql.com/get/Downloads/Connector-C++/libmysqlcppconn8-2_8.0.28-1ubuntu20.04_amd64.deb && \
    sudo dpkg -i libmysqlcppconn8-2_8.0.28-1ubuntu20.04_amd64.deb
    
    sudo mariadb-secure-installation
    ```

* Check the MariaDB status and enable it
    ```bash
    systemctl status mariadb
    sudo systemctl enable mariadb
    ```

* Finally update and upgrade to avoid any requirement issue:
    ```bash
    sudo apt update
    sudo apt upgrade -y
    ```

* Download the Argon2 library, it can be found [here](https://github.com/P-H-C/phc-winner-argon2):

    ```bash
    export VERSION=20190702 && \
    wget https://github.com/P-H-C/phc-winner-argon2/archive/refs/tags/${VERSION}.tar.gz && \
    tar -xzvf ${VERSION}.tar.gz && \
    cd phc-winner-argon2-${VERSION}
    ```

* Install the argon2 library

    ```bash
    make PREFIX=/usr
    make test
    sudo make install PREFIX=/usr
    ```

For PureFTPd, you can found and change the release version by changing the **VERSION** variable, but we recommend to follow the next steps:

* Download Pure-FTPd

    ```bash
    export VERSION=1.0.50 && \
        wget https://download.pureftpd.org/pub/pure-ftpd/releases/pure-ftpd-${VERSION}.tar.gz && \
        tar -xzvf pure-ftpd-${VERSION}.tar.gz && \
        cd pure-ftpd-${VERSION}
    ```

* Install Pure-FTPd

    ```bash
    ./configure --with-mysql --with-everything
    make
    make check
    sudo make install -j 4
    ```

 6. **Set ISMS required env variables**

Change the variable **ISMS_PATH** value to your ISMS root project directory, example: ***/home/user/isms***

```bash
echo 'export ISMS_PATH=change/this/path/to/isms-project-root-dir' >> ~/.bashrc && \ 
    echo 'export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$ISMS_PATH/builddir/common-dependencies/pkg-config' >> ~/.bashrc && \
    source ~/.bashrc
```

## Compile ISMS

Go to the project root directory and follow the next steps:

1. Create the build directory:

    ```bash
    sudo meson builddir
    ```

2.  Compile & install the project:

    ```bash
    sudo meson install -C builddir
    ```

4. Run tests

    ```bash
    sudo meson test -C builddir
    ```

## Project Status

This is a work in progress project, the modules are currently under development.

## License
Found the license [here](LICENSE)
